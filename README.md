# Jenkins Docker環境整備

## Jenkinsイメージ作成

オンライン環境でのみ実施

```bash
docker build -t jenkins .
```

<details>
<summary>実行時のログ詳細</summary>

```bash
# docker build -t jenkins .
Sending build context to Docker daemon  2.545GB
Step 1/32: FROM jenkins/jenkins:centos
centos: Pulling from jenkins/jenkins
8a29a15cefae: Pull complete
fa6ec84f0597: Pull complete
4411f8ba9caa: Pull complete
de74dd58dbe7: Pull complete
96b3d6c6b76e: Pull complete
465e7f89e63b: Pull complete
16c326f0033f: Pull complete
bd4ecde7f6e2: Pull complete
dea0779be650: Pull complete
3dad4f1b16f7: Pull complete
026e1f3ba92b: Pull complete
5ac29174ef09: Pull complete
ad950560ef71: Pull complete
Digest: sha256:f09bb88a9c1c29bcc1eb2850d81f1fcb29818003a78dd1b0512a201a6d1ab96b
Status: Downloaded newer image for jenkins/jenkins:centos
 ---> bf1c9963816a
Step 2/32: LABEL y-ok <fairsky1985@gmail.com>
 ---> Running in 2e5fb0107843
Removing intermediate container 2e5fb0107843
 ---> 20eac6185661
Step 3/32: USER root
 ---> Running in d484c820b869
Removing intermediate container d484c820b869
 ---> 85a1c2f8f3d8
Step 4/32: RUN dnf clean all
 ---> Running in 7e9a3bec28dd
0 files removed
Removing intermediate container 7e9a3bec28dd
 ---> 673b871afb82
Step 5/32: RUN dnf -y groupinstall "Development Tools"
 ---> Running in a639e0aeb7f2
CentOS-8 - AppStream                            3.4 MB/s | 5.7 MB     00:01
CentOS-8 - Base                                 1.6 MB/s | 2.2 MB     00:01
CentOS-8 - Extras                                15 kB/s | 5.5 kB     00:00
Extra Packages for Enterprise Linux Modular 8 - 196 kB/s | 116 kB     00:00
Extra Packages for Enterprise Linux 8 - x86_64  4.1 MB/s | 6.4 MB     00:01
github_git-lfs                                  3.4 kB/s |  12 kB     00:03    
github_git-lfs-source                           858  B/s | 2.3 kB     00:02    
Dependencies resolved.
================================================================================
 Package                          Arch   Version                Repo       Size
================================================================================
Upgrading:
 git                              x86_64 2.18.2-2.el8_1         AppStream 186 k
 git-core                         x86_64 2.18.2-2.el8_1         AppStream 5.0 M
 git-core-doc                     noarch 2.18.2-2.el8_1         AppStream 2.3 M
 perl-Git                         noarch 2.18.2-2.el8_1         AppStream  77 k
Installing group/module packages:
 asciidoc                         noarch 8.6.10-0.5.20180627gitf7c2274.el8
                                                                AppStream 216 k
 autoconf                         noarch 2.69-27.el8            AppStream 710 k
 automake                         noarch 1.16.1-6.el8           AppStream 713 k
 bison                            x86_64 3.0.4-10.el8           AppStream 688 k
 byacc                            x86_64 1.9.20170709-4.el8     AppStream  91 k
 ctags                            x86_64 5.8-22.el8             AppStream 170 k
 diffstat                         x86_64 1.61-7.el8             AppStream  44 k
 flex                             x86_64 2.6.1-9.el8            AppStream 320 k
 gcc                              x86_64 8.3.1-4.5.el8          AppStream  23 M
 gcc-c++                          x86_64 8.3.1-4.5.el8          AppStream  12 M
 gdb                              x86_64 8.2-6.el8              AppStream 296 k
 intltool                         noarch 0.51.0-11.el8          AppStream  66 k
 jna                              x86_64 4.5.1-5.el8            AppStream 242 k
 libtool                          x86_64 2.4.6-25.el8           AppStream 709 k
 ltrace                           x86_64 0.7.91-27.el8          AppStream 160 k
 patchutils                       x86_64 0.3.4-10.el8           AppStream 116 k
 perl-Fedora-VSP                  noarch 0.001-9.el8            AppStream  24 k
 perl-generators                  noarch 1.10-9.el8             AppStream  18 k
 pesign                           x86_64 0.112-25.el8           AppStream 181 k
 redhat-rpm-config                noarch 120-1.el8              AppStream  83 k
 rpm-build                        x86_64 4.14.2-26.el8_1        AppStream 169 k
 source-highlight                 x86_64 3.1.8-16.el8           AppStream 661 k
 systemtap                        x86_64 4.1-6.el8              AppStream  18 k
 valgrind                         x86_64 1:3.15.0-9.el8         AppStream  12 M
 valgrind-devel                   x86_64 1:3.15.0-9.el8         AppStream  90 k
 glibc-devel                      x86_64 2.28-72.el8_1.1        BaseOS    1.0 M
 make                             x86_64 1:4.2.1-9.el8          BaseOS    498 k
 rpm-sign                         x86_64 4.14.2-26.el8_1        BaseOS     77 k
 strace                           x86_64 4.24-5.el8             BaseOS    972 k
Installing dependencies:
 adobe-mappings-cmap              noarch 20171205-3.el8         AppStream 2.1 M
 adobe-mappings-cmap-deprecated   noarch 20171205-3.el8         AppStream 119 k
 adobe-mappings-pdf               noarch 20180407-1.el8         AppStream 707 k
 annobin                          x86_64 8.78-1.el8             AppStream 196 k
 boost-atomic                     x86_64 1.66.0-6.el8           AppStream  13 k
 boost-chrono                     x86_64 1.66.0-6.el8           AppStream  23 k
 boost-date-time                  x86_64 1.66.0-6.el8           AppStream  29 k
 boost-filesystem                 x86_64 1.66.0-6.el8           AppStream  49 k
 boost-regex                      x86_64 1.66.0-6.el8           AppStream 286 k
 boost-system                     x86_64 1.66.0-6.el8           AppStream  18 k
 boost-thread                     x86_64 1.66.0-6.el8           AppStream  59 k
 boost-timer                      x86_64 1.66.0-6.el8           AppStream  20 k
 cpp                              x86_64 8.3.1-4.5.el8          AppStream  10 M
 docbook-dtds                     noarch 1.0-69.el8             AppStream 377 k
 docbook-style-xsl                noarch 1.79.2-7.el8           AppStream 1.6 M
 dwz                              x86_64 0.12-9.el8             AppStream 109 k
 dyninst                          x86_64 10.1.0-2.el8           AppStream 3.9 M
 efi-srpm-macros                  noarch 3-2.el8                AppStream  22 k
 gc                               x86_64 7.6.4-3.el8            AppStream 109 k
 gd                               x86_64 2.2.5-6.el8            AppStream 144 k
 gdb-headless                     x86_64 8.2-6.el8              AppStream 3.7 M
 ghc-srpm-macros                  noarch 1.4.2-7.el8            AppStream 9.3 k
 go-srpm-macros                   noarch 2-16.el8               AppStream  14 k
 google-droid-sans-fonts          noarch 20120715-13.el8        AppStream 2.5 M
 graphviz                         x86_64 2.40.1-39.el8          AppStream 1.7 M
 guile                            x86_64 5:2.0.14-7.el8         AppStream 3.5 M
 isl                              x86_64 0.16.1-6.el8           AppStream 841 k
 jbig2dec-libs                    x86_64 0.14-2.el8             AppStream  67 k
 lcms2                            x86_64 2.9-2.el8              AppStream 165 k
 libICE                           x86_64 1.0.9-15.el8           AppStream  74 k
 libSM                            x86_64 1.2.3-1.el8            AppStream  48 k
 libXaw                           x86_64 1.0.13-10.el8          AppStream 194 k
 libXmu                           x86_64 1.1.2-12.el8           AppStream  74 k
 libXpm                           x86_64 3.5.12-7.el8           AppStream  58 k
 libXt                            x86_64 1.1.5-12.el8           AppStream 186 k
 libXxf86misc                     x86_64 1.0.4-1.el8            AppStream  23 k
 libXxf86vm                       x86_64 1.1.4-9.el8            AppStream  19 k
 libatomic_ops                    x86_64 7.6.2-3.el8            AppStream  38 k
 libbabeltrace                    x86_64 1.5.4-2.el8            AppStream 201 k
 libgs                            x86_64 9.25-5.el8_1.1         AppStream 3.1 M
 libidn                           x86_64 1.34-5.el8             AppStream 239 k
 libijs                           x86_64 0.35-5.el8             AppStream  30 k
 libipt                           x86_64 1.6.1-8.el8            AppStream  50 k
 libmcpp                          x86_64 2.7.2-20.el8           AppStream  81 k
 libmpc                           x86_64 1.0.2-9.el8            AppStream  59 k
 libpaper                         x86_64 1.1.24-22.el8          AppStream  45 k
 librsvg2                         x86_64 2.42.7-3.el8           AppStream 570 k
 libstdc++-devel                  x86_64 8.3.1-4.5.el8          AppStream 2.0 M
 libwebp                          x86_64 1.0.0-1.el8            AppStream 273 k
 mcpp                             x86_64 2.7.2-20.el8           AppStream  31 k
 nspr                             x86_64 4.21.0-2.el8_0         AppStream 143 k
 nss                              x86_64 3.44.0-9.el8_1         AppStream 724 k
 nss-softokn                      x86_64 3.44.0-9.el8_1         AppStream 472 k
 nss-softokn-freebl               x86_64 3.44.0-9.el8_1         AppStream 273 k
 nss-sysinit                      x86_64 3.44.0-9.el8_1         AppStream  69 k
 nss-tools                        x86_64 3.44.0-9.el8_1         AppStream 572 k
 nss-util                         x86_64 3.44.0-9.el8_1         AppStream 135 k
 ocaml-srpm-macros                noarch 5-4.el8                AppStream 9.4 k
 openblas-srpm-macros             noarch 2-2.el8                AppStream 7.9 k
 openjpeg2                        x86_64 2.3.1-3.el8_1          AppStream 154 k
 perl-Thread-Queue                noarch 3.13-1.el8             AppStream  24 k
 perl-XML-Parser                  x86_64 2.44-11.el8            AppStream 226 k
 perl-srpm-macros                 noarch 1-25.el8               AppStream  11 k
 python-srpm-macros               noarch 3-37.el8               AppStream  14 k
 python3-rpm-macros               noarch 3-37.el8               AppStream  13 k
 qt5-srpm-macros                  noarch 5.11.1-2.el8           AppStream  11 k
 rust-srpm-macros                 noarch 5-2.el8                AppStream 9.2 k
 systemtap-client                 x86_64 4.1-6.el8              AppStream 3.6 M
 systemtap-devel                  x86_64 4.1-6.el8              AppStream 2.3 M
 systemtap-runtime                x86_64 4.1-6.el8              AppStream 481 k
 tbb                              x86_64 2018.2-9.el8           AppStream 160 k
 urw-base35-bookman-fonts         noarch 20170801-10.el8        AppStream 857 k
 urw-base35-c059-fonts            noarch 20170801-10.el8        AppStream 884 k
 urw-base35-d050000l-fonts        noarch 20170801-10.el8        AppStream  79 k
 urw-base35-fonts                 noarch 20170801-10.el8        AppStream  12 k
 urw-base35-fonts-common          noarch 20170801-10.el8        AppStream  23 k
 urw-base35-gothic-fonts          noarch 20170801-10.el8        AppStream 654 k
 urw-base35-nimbus-mono-ps-fonts  noarch 20170801-10.el8        AppStream 801 k
 urw-base35-nimbus-roman-fonts    noarch 20170801-10.el8        AppStream 865 k
 urw-base35-nimbus-sans-fonts     noarch 20170801-10.el8        AppStream 1.3 M
 urw-base35-p052-fonts            noarch 20170801-10.el8        AppStream 982 k
 urw-base35-standard-symbols-ps-fonts
                                  noarch 20170801-10.el8        AppStream  44 k
 urw-base35-z003-fonts            noarch 20170801-10.el8        AppStream 279 k
 vim-filesystem                   noarch 2:8.0.1763-13.el8      AppStream  48 k
 xorg-x11-fonts-ISO8859-1-100dpi  noarch 7.5-19.el8             AppStream 1.1 M
 xorg-x11-server-utils            x86_64 7.7-27.el8             AppStream 198 k
 bzip2                            x86_64 1.0.6-26.el8           BaseOS     60 k
 efivar-libs                      x86_64 36-1.el8               BaseOS     97 k
 elfutils                         x86_64 0.176-5.el8            BaseOS    348 k
 file                             x86_64 5.33-8.el8             BaseOS     76 k
 gettext                          x86_64 0.19.8.1-17.el8        BaseOS    1.1 M
 gettext-common-devel             noarch 0.19.8.1-17.el8        BaseOS    419 k
 gettext-devel                    x86_64 0.19.8.1-17.el8        BaseOS    331 k
 gettext-libs                     x86_64 0.19.8.1-17.el8        BaseOS    314 k
 glibc-headers                    x86_64 2.28-72.el8_1.1        BaseOS    469 k
 kernel-headers                   x86_64 4.18.0-147.8.1.el8_1   BaseOS    2.7 M
 libcroco                         x86_64 0.6.12-4.el8           BaseOS    113 k
 libgomp                          x86_64 8.3.1-4.5.el8          BaseOS    203 k
 libicu                           x86_64 60.3-2.el8_1           BaseOS    8.8 M
 libtool-ltdl                     x86_64 2.4.6-25.el8           BaseOS     58 k
 libxcrypt-devel                  x86_64 4.1.1-4.el8            BaseOS     25 k
 libxslt                          x86_64 1.1.32-3.el8           BaseOS    249 k
 m4                               x86_64 1.4.18-7.el8           BaseOS    223 k
 mokutil                          x86_64 1:0.3.0-9.el8          BaseOS     44 k
 patch                            x86_64 2.7.6-9.el8_0          BaseOS    138 k
 python3-dateutil                 noarch 1:2.6.1-6.el8          BaseOS    251 k
 python3-dnf-plugins-core         noarch 4.0.8-3.el8            BaseOS    193 k
 python3-six                      noarch 1.11.0-8.el8           BaseOS     38 k
 sgml-common                      noarch 0.6.3-50.el8           BaseOS     62 k
 xml-common                       noarch 0.6.3-50.el8           BaseOS     39 k
 zip                              x86_64 3.0-23.el8             BaseOS    270 k
Installing weak dependencies:
 gcc-gdb-plugin                   x86_64 8.3.1-4.5.el8          AppStream 117 k
 dnf-plugins-core                 noarch 4.0.8-3.el8            BaseOS     62 k
Installing Groups:
 Development Tools

Transaction Summary
================================================================================
Install  142 Packages
Upgrade    4 Packages

Total download size: 137 M
Downloading Packages:
(1/146): adobe-mappings-cmap-deprecated-2017120 1.2 MB/s | 119 kB     00:00
(2/146): annobin-8.78-1.el8.x86_64.rpm          1.5 MB/s | 196 kB     00:00
(3/146): adobe-mappings-pdf-20180407-1.el8.noar 2.1 MB/s | 707 kB     00:00
(4/146): asciidoc-8.6.10-0.5.20180627gitf7c2274 1.5 MB/s | 216 kB     00:00
(5/146): automake-1.16.1-6.el8.noarch.rpm       2.8 MB/s | 713 kB     00:00
(6/146): autoconf-2.69-27.el8.noarch.rpm        2.0 MB/s | 710 kB     00:00
(7/146): boost-atomic-1.66.0-6.el8.x86_64.rpm   957 kB/s |  13 kB     00:00
(8/146): boost-chrono-1.66.0-6.el8.x86_64.rpm   1.6 MB/s |  23 kB     00:00
(9/146): boost-date-time-1.66.0-6.el8.x86_64.rp 1.1 MB/s |  29 kB     00:00
(10/146): boost-filesystem-1.66.0-6.el8.x86_64. 1.8 MB/s |  49 kB     00:00
(11/146): bison-3.0.4-10.el8.x86_64.rpm         3.5 MB/s | 688 kB     00:00
(12/146): boost-system-1.66.0-6.el8.x86_64.rpm  1.2 MB/s |  18 kB     00:00
(13/146): boost-thread-1.66.0-6.el8.x86_64.rpm  1.5 MB/s |  59 kB     00:00
(14/146): boost-regex-1.66.0-6.el8.x86_64.rpm   2.5 MB/s | 286 kB     00:00
(15/146): boost-timer-1.66.0-6.el8.x86_64.rpm   537 kB/s |  20 kB     00:00
(16/146): byacc-1.9.20170709-4.el8.x86_64.rpm   2.3 MB/s |  91 kB     00:00
(17/146): ctags-5.8-22.el8.x86_64.rpm           2.2 MB/s | 170 kB     00:00
(18/146): diffstat-1.61-7.el8.x86_64.rpm        1.6 MB/s |  44 kB     00:00
(19/146): docbook-dtds-1.0-69.el8.noarch.rpm    1.7 MB/s | 377 kB     00:00
(20/146): adobe-mappings-cmap-20171205-3.el8.no 1.5 MB/s | 2.1 MB     00:01
(21/146): dwz-0.12-9.el8.x86_64.rpm             1.7 MB/s | 109 kB     00:00
(22/146): docbook-style-xsl-1.79.2-7.el8.noarch 2.7 MB/s | 1.6 MB     00:00
(23/146): efi-srpm-macros-3-2.el8.noarch.rpm    1.4 MB/s |  22 kB     00:00
(24/146): flex-2.6.1-9.el8.x86_64.rpm           1.4 MB/s | 320 kB     00:00
(25/146): gc-7.6.4-3.el8.x86_64.rpm             2.1 MB/s | 109 kB     00:00
(26/146): dyninst-10.1.0-2.el8.x86_64.rpm       2.9 MB/s | 3.9 MB     00:01
(27/146): cpp-8.3.1-4.5.el8.x86_64.rpm          2.7 MB/s |  10 MB     00:03
(28/146): gcc-gdb-plugin-8.3.1-4.5.el8.x86_64.r 1.8 MB/s | 117 kB     00:00
(29/146): gd-2.2.5-6.el8.x86_64.rpm             1.6 MB/s | 144 kB     00:00
(30/146): gdb-8.2-6.el8.x86_64.rpm              1.3 MB/s | 296 kB     00:00
(31/146): gdb-headless-8.2-6.el8.x86_64.rpm     2.8 MB/s | 3.7 MB     00:01
(32/146): ghc-srpm-macros-1.4.2-7.el8.noarch.rp 699 kB/s | 9.3 kB     00:00
(33/146): go-srpm-macros-2-16.el8.noarch.rpm    1.0 MB/s |  14 kB     00:00
(34/146): gcc-c++-8.3.1-4.5.el8.x86_64.rpm      2.8 MB/s |  12 MB     00:04
(35/146): google-droid-sans-fonts-20120715-13.e 2.5 MB/s | 2.5 MB     00:00
(36/146): graphviz-2.40.1-39.el8.x86_64.rpm     2.3 MB/s | 1.7 MB     00:00
(37/146): intltool-0.51.0-11.el8.noarch.rpm     2.3 MB/s |  66 kB     00:00
(38/146): isl-0.16.1-6.el8.x86_64.rpm           3.3 MB/s | 841 kB     00:00
(39/146): jbig2dec-libs-0.14-2.el8.x86_64.rpm   1.6 MB/s |  67 kB     00:00
(40/146): jna-4.5.1-5.el8.x86_64.rpm            1.9 MB/s | 242 kB     00:00
(41/146): lcms2-2.9-2.el8.x86_64.rpm            2.1 MB/s | 165 kB     00:00
(42/146): libICE-1.0.9-15.el8.x86_64.rpm        1.9 MB/s |  74 kB     00:00
(43/146): libSM-1.2.3-1.el8.x86_64.rpm          1.8 MB/s |  48 kB     00:00
(44/146): libXaw-1.0.13-10.el8.x86_64.rpm       2.9 MB/s | 194 kB     00:00
(45/146): libXmu-1.1.2-12.el8.x86_64.rpm        2.6 MB/s |  74 kB     00:00
(46/146): libXpm-3.5.12-7.el8.x86_64.rpm        2.1 MB/s |  58 kB     00:00
(47/146): libXt-1.1.5-12.el8.x86_64.rpm         2.3 MB/s | 186 kB     00:00
(48/146): libXxf86misc-1.0.4-1.el8.x86_64.rpm   1.6 MB/s |  23 kB     00:00
(49/146): libXxf86vm-1.1.4-9.el8.x86_64.rpm     1.4 MB/s |  19 kB     00:00
(50/146): libatomic_ops-7.6.2-3.el8.x86_64.rpm  1.4 MB/s |  38 kB     00:00
(51/146): guile-2.0.14-7.el8.x86_64.rpm         2.8 MB/s | 3.5 MB     00:01
(52/146): libbabeltrace-1.5.4-2.el8.x86_64.rpm  2.2 MB/s | 201 kB     00:00
(53/146): libidn-1.34-5.el8.x86_64.rpm          560 kB/s | 239 kB     00:00
(54/146): libijs-0.35-5.el8.x86_64.rpm          1.1 MB/s |  30 kB     00:00
(55/146): libipt-1.6.1-8.el8.x86_64.rpm         1.8 MB/s |  50 kB     00:00
(56/146): libmcpp-2.7.2-20.el8.x86_64.rpm       2.0 MB/s |  81 kB     00:00
(57/146): libmpc-1.0.2-9.el8.x86_64.rpm         2.1 MB/s |  59 kB     00:00
(58/146): libpaper-1.1.24-22.el8.x86_64.rpm     1.7 MB/s |  45 kB     00:00
(59/146): librsvg2-2.42.7-3.el8.x86_64.rpm      2.9 MB/s | 570 kB     00:00
(60/146): libgs-9.25-5.el8_1.1.x86_64.rpm       2.7 MB/s | 3.1 MB     00:01
(61/146): gcc-8.3.1-4.5.el8.x86_64.rpm          2.9 MB/s |  23 MB     00:08
(62/146): libstdc++-devel-8.3.1-4.5.el8.x86_64. 3.1 MB/s | 2.0 MB     00:00
(63/146): libtool-2.4.6-25.el8.x86_64.rpm       2.1 MB/s | 709 kB     00:00
(64/146): mcpp-2.7.2-20.el8.x86_64.rpm          2.2 MB/s |  31 kB     00:00
(65/146): nspr-4.21.0-2.el8_0.x86_64.rpm        2.2 MB/s | 143 kB     00:00
(66/146): ltrace-0.7.91-27.el8.x86_64.rpm       1.8 MB/s | 160 kB     00:00
(67/146): libwebp-1.0.0-1.el8.x86_64.rpm        2.1 MB/s | 273 kB     00:00
(68/146): nss-softokn-freebl-3.44.0-9.el8_1.x86 1.8 MB/s | 273 kB     00:00
(69/146): nss-sysinit-3.44.0-9.el8_1.x86_64.rpm 1.7 MB/s |  69 kB     00:00
(70/146): nss-softokn-3.44.0-9.el8_1.x86_64.rpm 1.9 MB/s | 472 kB     00:00
(71/146): nss-util-3.44.0-9.el8_1.x86_64.rpm    2.0 MB/s | 135 kB     00:00
(72/146): ocaml-srpm-macros-5-4.el8.noarch.rpm  705 kB/s | 9.4 kB     00:00
(73/146): openblas-srpm-macros-2-2.el8.noarch.r 584 kB/s | 7.9 kB     00:00
(74/146): nss-3.44.0-9.el8_1.x86_64.rpm         1.8 MB/s | 724 kB     00:00
(75/146): patchutils-0.3.4-10.el8.x86_64.rpm    2.2 MB/s | 116 kB     00:00
(76/146): nss-tools-3.44.0-9.el8_1.x86_64.rpm   2.6 MB/s | 572 kB     00:00
(77/146): perl-Fedora-VSP-0.001-9.el8.noarch.rp 1.6 MB/s |  24 kB     00:00
(78/146): perl-Thread-Queue-3.13-1.el8.noarch.r 1.7 MB/s |  24 kB     00:00
(79/146): openjpeg2-2.3.1-3.el8_1.x86_64.rpm    1.2 MB/s | 154 kB     00:00
(80/146): perl-generators-1.10-9.el8.noarch.rpm 1.3 MB/s |  18 kB     00:00
(81/146): perl-srpm-macros-1-25.el8.noarch.rpm  751 kB/s |  11 kB     00:00
(82/146): python-srpm-macros-3-37.el8.noarch.rp 1.0 MB/s |  14 kB     00:00
(83/146): python3-rpm-macros-3-37.el8.noarch.rp 958 kB/s |  13 kB     00:00
(84/146): qt5-srpm-macros-5.11.1-2.el8.noarch.r 784 kB/s |  11 kB     00:00
(85/146): perl-XML-Parser-2.44-11.el8.x86_64.rp 1.9 MB/s | 226 kB     00:00
(86/146): pesign-0.112-25.el8.x86_64.rpm        1.9 MB/s | 181 kB     00:00
(87/146): redhat-rpm-config-120-1.el8.noarch.rp 1.6 MB/s |  83 kB     00:00
(88/146): rpm-build-4.14.2-26.el8_1.x86_64.rpm  1.2 MB/s | 169 kB     00:00
(89/146): systemtap-4.1-6.el8.x86_64.rpm        693 kB/s |  18 kB     00:00
(90/146): source-highlight-3.1.8-16.el8.x86_64. 2.6 MB/s | 661 kB     00:00
(91/146): rust-srpm-macros-5-2.el8.noarch.rpm    36 kB/s | 9.2 kB     00:00
(92/146): systemtap-runtime-4.1-6.el8.x86_64.rp 1.0 MB/s | 481 kB     00:00
(93/146): tbb-2018.2-9.el8.x86_64.rpm           1.8 MB/s | 160 kB     00:00
(94/146): urw-base35-bookman-fonts-20170801-10. 1.9 MB/s | 857 kB     00:00
(95/146): systemtap-devel-4.1-6.el8.x86_64.rpm  2.1 MB/s | 2.3 MB     00:01
(96/146): urw-base35-d050000l-fonts-20170801-10 2.0 MB/s |  79 kB     00:00
(97/146): urw-base35-fonts-20170801-10.el8.noar 913 kB/s |  12 kB     00:00
(98/146): urw-base35-fonts-common-20170801-10.e 1.7 MB/s |  23 kB     00:00
(99/146): urw-base35-c059-fonts-20170801-10.el8 3.2 MB/s | 884 kB     00:00
(100/146): urw-base35-gothic-fonts-20170801-10. 2.2 MB/s | 654 kB     00:00
(101/146): urw-base35-nimbus-mono-ps-fonts-2017 2.2 MB/s | 801 kB     00:00
(102/146): urw-base35-nimbus-roman-fonts-201708 3.5 MB/s | 865 kB     00:00
(103/146): systemtap-client-4.1-6.el8.x86_64.rp 2.0 MB/s | 3.6 MB     00:01
(104/146): urw-base35-p052-fonts-20170801-10.el 4.3 MB/s | 982 kB     00:00
(105/146): urw-base35-standard-symbols-ps-fonts 178 kB/s |  44 kB     00:00
(106/146): urw-base35-z003-fonts-20170801-10.el 1.8 MB/s | 279 kB     00:00
(107/146): valgrind-devel-3.15.0-9.el8.x86_64.r 1.8 MB/s |  90 kB     00:00
(108/146): vim-filesystem-8.0.1763-13.el8.noarc 1.8 MB/s |  48 kB     00:00
(109/146): urw-base35-nimbus-sans-fonts-2017080 2.5 MB/s | 1.3 MB     00:00
(110/146): xorg-x11-server-utils-7.7-27.el8.x86 2.1 MB/s | 198 kB     00:00
(111/146): bzip2-1.0.6-26.el8.x86_64.rpm        2.2 MB/s |  60 kB     00:00
(112/146): dnf-plugins-core-4.0.8-3.el8.noarch. 1.6 MB/s |  62 kB     00:00
(113/146): efivar-libs-36-1.el8.x86_64.rpm      1.3 MB/s |  97 kB     00:00
(114/146): elfutils-0.176-5.el8.x86_64.rpm      2.1 MB/s | 348 kB     00:00
(115/146): file-5.33-8.el8.x86_64.rpm           1.8 MB/s |  76 kB     00:00
(116/146): xorg-x11-fonts-ISO8859-1-100dpi-7.5- 2.0 MB/s | 1.1 MB     00:00
(117/146): gettext-common-devel-0.19.8.1-17.el8 2.4 MB/s | 419 kB     00:00
(118/146): gettext-devel-0.19.8.1-17.el8.x86_64 3.1 MB/s | 331 kB     00:00
(119/146): gettext-0.19.8.1-17.el8.x86_64.rpm   2.6 MB/s | 1.1 MB     00:00
(120/146): gettext-libs-0.19.8.1-17.el8.x86_64. 3.4 MB/s | 314 kB     00:00
(121/146): glibc-headers-2.28-72.el8_1.1.x86_64 2.5 MB/s | 469 kB     00:00
(122/146): glibc-devel-2.28-72.el8_1.1.x86_64.r 3.2 MB/s | 1.0 MB     00:00
(123/146): libcroco-0.6.12-4.el8.x86_64.rpm     1.4 MB/s | 113 kB     00:00
(124/146): libgomp-8.3.1-4.5.el8.x86_64.rpm     2.0 MB/s | 203 kB     00:00
(125/146): kernel-headers-4.18.0-147.8.1.el8_1. 2.8 MB/s | 2.7 MB     00:00
(126/146): libtool-ltdl-2.4.6-25.el8.x86_64.rpm 1.5 MB/s |  58 kB     00:00
(127/146): libxcrypt-devel-4.1.1-4.el8.x86_64.r 397 kB/s |  25 kB     00:00
(128/146): libxslt-1.1.32-3.el8.x86_64.rpm      1.2 MB/s | 249 kB     00:00
(129/146): m4-1.4.18-7.el8.x86_64.rpm           1.9 MB/s | 223 kB     00:00
(130/146): make-4.2.1-9.el8.x86_64.rpm          2.8 MB/s | 498 kB     00:00
(131/146): mokutil-0.3.0-9.el8.x86_64.rpm       1.7 MB/s |  44 kB     00:00
(132/146): patch-2.7.6-9.el8_0.x86_64.rpm       2.1 MB/s | 138 kB     00:00
(133/146): python3-dateutil-2.6.1-6.el8.noarch. 2.2 MB/s | 251 kB     00:00
(134/146): python3-dnf-plugins-core-4.0.8-3.el8 1.4 MB/s | 193 kB     00:00
(135/146): python3-six-1.11.0-8.el8.noarch.rpm  1.4 MB/s |  38 kB     00:00
(136/146): rpm-sign-4.14.2-26.el8_1.x86_64.rpm  1.9 MB/s |  77 kB     00:00
(137/146): sgml-common-0.6.3-50.el8.noarch.rpm  1.6 MB/s |  62 kB     00:00
(138/146): strace-4.24-5.el8.x86_64.rpm         2.4 MB/s | 972 kB     00:00
(139/146): xml-common-0.6.3-50.el8.noarch.rpm   2.5 MB/s |  39 kB     00:00
(140/146): libicu-60.3-2.el8_1.x86_64.rpm       3.9 MB/s | 8.8 MB     00:02
(141/146): zip-3.0-23.el8.x86_64.rpm            2.1 MB/s | 270 kB     00:00
(142/146): git-2.18.2-2.el8_1.x86_64.rpm        2.4 MB/s | 186 kB     00:00
(143/146): valgrind-3.15.0-9.el8.x86_64.rpm     3.0 MB/s |  12 MB     00:03
(144/146): perl-Git-2.18.2-2.el8_1.noarch.rpm   1.9 MB/s |  77 kB     00:00
(145/146): git-core-doc-2.18.2-2.el8_1.noarch.r 3.0 MB/s | 2.3 MB     00:00
(146/146): git-core-2.18.2-2.el8_1.x86_64.rpm   3.4 MB/s | 5.0 MB     00:01
--------------------------------------------------------------------------------
Total                                           7.1 MB/s | 137 MB     00:19
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : urw-base35-fonts-common-20170801-10.el8.noarch       1/150
  Installing       : nspr-4.21.0-2.el8_0.x86_64                           2/150
  Running scriptlet: nspr-4.21.0-2.el8_0.x86_64                           2/150
  Installing       : nss-util-3.44.0-9.el8_1.x86_64                       3/150
  Installing       : boost-system-1.66.0-6.el8.x86_64                     4/150
  Running scriptlet: boost-system-1.66.0-6.el8.x86_64                     4/150
  Installing       : libgomp-8.3.1-4.5.el8.x86_64                         5/150
  Running scriptlet: libgomp-8.3.1-4.5.el8.x86_64                         5/150
  Installing       : libICE-1.0.9-15.el8.x86_64                           6/150
  Installing       : m4-1.4.18-7.el8.x86_64                               7/150
  Running scriptlet: m4-1.4.18-7.el8.x86_64                               7/150
  Installing       : libcroco-0.6.12-4.el8.x86_64                         8/150
  Running scriptlet: libcroco-0.6.12-4.el8.x86_64                         8/150
  Installing       : libmpc-1.0.2-9.el8.x86_64                            9/150
  Running scriptlet: libmpc-1.0.2-9.el8.x86_64                            9/150
  Installing       : libXpm-3.5.12-7.el8.x86_64                          10/150
  Installing       : gettext-libs-0.19.8.1-17.el8.x86_64                 11/150
  Installing       : autoconf-2.69-27.el8.noarch                         12/150
  Running scriptlet: autoconf-2.69-27.el8.noarch                         12/150
  Installing       : libSM-1.2.3-1.el8.x86_64                            13/150
  Installing       : libXt-1.1.5-12.el8.x86_64                           14/150
  Installing       : libXmu-1.1.2-12.el8.x86_64                          15/150
  Installing       : boost-chrono-1.66.0-6.el8.x86_64                    16/150
  Running scriptlet: boost-chrono-1.66.0-6.el8.x86_64                    16/150
  Upgrading        : git-core-2.18.2-2.el8_1.x86_64                      17/150
  Installing       : zip-3.0-23.el8.x86_64                               18/150
  Running scriptlet: xml-common-0.6.3-50.el8.noarch                      19/150
  Installing       : xml-common-0.6.3-50.el8.noarch                      19/150
  Installing       : patch-2.7.6-9.el8_0.x86_64                          20/150
  Installing       : libtool-ltdl-2.4.6-25.el8.x86_64                    21/150
  Running scriptlet: libtool-ltdl-2.4.6-25.el8.x86_64                    21/150
  Installing       : file-5.33-8.el8.x86_64                              22/150
  Installing       : efivar-libs-36-1.el8.x86_64                         23/150
  Running scriptlet: efivar-libs-36-1.el8.x86_64                         23/150
  Installing       : adobe-mappings-cmap-20171205-3.el8.noarch           24/150
  Installing       : adobe-mappings-cmap-deprecated-20171205-3.el8.no    25/150
  Installing       : mokutil-1:0.3.0-9.el8.x86_64                        26/150
  Upgrading        : git-core-doc-2.18.2-2.el8_1.noarch                  27/150
  Upgrading        : perl-Git-2.18.2-2.el8_1.noarch                      28/150
  Upgrading        : git-2.18.2-2.el8_1.x86_64                           29/150
  Installing       : boost-timer-1.66.0-6.el8.x86_64                     30/150
  Running scriptlet: boost-timer-1.66.0-6.el8.x86_64                     30/150
  Installing       : libXaw-1.0.13-10.el8.x86_64                         31/150
  Installing       : gettext-0.19.8.1-17.el8.x86_64                      32/150
  Running scriptlet: gettext-0.19.8.1-17.el8.x86_64                      32/150
  Installing       : cpp-8.3.1-4.5.el8.x86_64                            33/150
  Running scriptlet: cpp-8.3.1-4.5.el8.x86_64                            33/150
  Installing       : librsvg2-2.42.7-3.el8.x86_64                        34/150
  Installing       : boost-filesystem-1.66.0-6.el8.x86_64                35/150
  Running scriptlet: boost-filesystem-1.66.0-6.el8.x86_64                35/150
  Installing       : boost-thread-1.66.0-6.el8.x86_64                    36/150
  Running scriptlet: boost-thread-1.66.0-6.el8.x86_64                    36/150
  Installing       : nss-softokn-freebl-3.44.0-9.el8_1.x86_64            37/150
  Installing       : nss-softokn-3.44.0-9.el8_1.x86_64                   38/150
  Installing       : nss-3.44.0-9.el8_1.x86_64                           39/150
  Running scriptlet: nss-3.44.0-9.el8_1.x86_64                           39/150
  Installing       : nss-sysinit-3.44.0-9.el8_1.x86_64                   40/150
  Installing       : nss-tools-3.44.0-9.el8_1.x86_64                     41/150
  Installing       : sgml-common-0.6.3-50.el8.noarch                     42/150
  Installing       : docbook-dtds-1.0-69.el8.noarch                      43/150
  Running scriptlet: docbook-dtds-1.0-69.el8.noarch                      43/150
  Installing       : docbook-style-xsl-1.79.2-7.el8.noarch               44/150
  Running scriptlet: docbook-style-xsl-1.79.2-7.el8.noarch               44/150
  Installing       : python3-six-1.11.0-8.el8.noarch                     45/150
  Installing       : python3-dateutil-1:2.6.1-6.el8.noarch               46/150
  Installing       : python3-dnf-plugins-core-4.0.8-3.el8.noarch         47/150
  Installing       : dnf-plugins-core-4.0.8-3.el8.noarch                 48/150
  Installing       : make-1:4.2.1-9.el8.x86_64                           49/150
  Running scriptlet: make-1:4.2.1-9.el8.x86_64                           49/150
  Installing       : libxslt-1.1.32-3.el8.x86_64                         50/150
  Installing       : libicu-60.3-2.el8_1.x86_64                          51/150
  Running scriptlet: libicu-60.3-2.el8_1.x86_64                          51/150
  Installing       : boost-regex-1.66.0-6.el8.x86_64                     52/150
  Running scriptlet: boost-regex-1.66.0-6.el8.x86_64                     52/150
  Installing       : kernel-headers-4.18.0-147.8.1.el8_1.x86_64          53/150
  Running scriptlet: glibc-headers-2.28-72.el8_1.1.x86_64                54/150
  Installing       : glibc-headers-2.28-72.el8_1.1.x86_64                54/150
  Installing       : libxcrypt-devel-4.1.1-4.el8.x86_64                  55/150
  Installing       : glibc-devel-2.28-72.el8_1.1.x86_64                  56/150
  Running scriptlet: glibc-devel-2.28-72.el8_1.1.x86_64                  56/150
  Installing       : gettext-common-devel-0.19.8.1-17.el8.noarch         57/150
  Installing       : gettext-devel-0.19.8.1-17.el8.x86_64                58/150
  Running scriptlet: gettext-devel-0.19.8.1-17.el8.x86_64                58/150
  Installing       : elfutils-0.176-5.el8.x86_64                         59/150
  Installing       : bzip2-1.0.6-26.el8.x86_64                           60/150
  Installing       : xorg-x11-fonts-ISO8859-1-100dpi-7.5-19.el8.noarc    61/150
  Running scriptlet: xorg-x11-fonts-ISO8859-1-100dpi-7.5-19.el8.noarc    61/150
  Installing       : vim-filesystem-2:8.0.1763-13.el8.noarch             62/150
  Installing       : valgrind-1:3.15.0-9.el8.x86_64                      63/150
  Installing       : tbb-2018.2-9.el8.x86_64                             64/150
  Running scriptlet: tbb-2018.2-9.el8.x86_64                             64/150
  Installing       : rust-srpm-macros-5-2.el8.noarch                     65/150
  Installing       : qt5-srpm-macros-5.11.1-2.el8.noarch                 66/150
  Installing       : python3-rpm-macros-3-37.el8.noarch                  67/150
  Installing       : python-srpm-macros-3-37.el8.noarch                  68/150
  Installing       : perl-srpm-macros-1-25.el8.noarch                    69/150
  Installing       : perl-XML-Parser-2.44-11.el8.x86_64                  70/150
  Installing       : perl-Thread-Queue-3.13-1.el8.noarch                 71/150
  Installing       : automake-1.16.1-6.el8.noarch                        72/150
  Installing       : perl-Fedora-VSP-0.001-9.el8.noarch                  73/150
  Installing       : openjpeg2-2.3.1-3.el8_1.x86_64                      74/150
  Installing       : openblas-srpm-macros-2-2.el8.noarch                 75/150
  Installing       : ocaml-srpm-macros-5-4.el8.noarch                    76/150
  Installing       : libwebp-1.0.0-1.el8.x86_64                          77/150
  Installing       : gd-2.2.5-6.el8.x86_64                               78/150
  Running scriptlet: gd-2.2.5-6.el8.x86_64                               78/150
  Installing       : libstdc++-devel-8.3.1-4.5.el8.x86_64                79/150
  Installing       : libpaper-1.1.24-22.el8.x86_64                       80/150
  Installing       : libmcpp-2.7.2-20.el8.x86_64                         81/150
  Running scriptlet: libmcpp-2.7.2-20.el8.x86_64                         81/150
  Installing       : mcpp-2.7.2-20.el8.x86_64                            82/150
  Installing       : libipt-1.6.1-8.el8.x86_64                           83/150
  Installing       : libijs-0.35-5.el8.x86_64                            84/150
  Installing       : libidn-1.34-5.el8.x86_64                            85/150
  Running scriptlet: libidn-1.34-5.el8.x86_64                            85/150
  Installing       : libbabeltrace-1.5.4-2.el8.x86_64                    86/150
  Running scriptlet: libbabeltrace-1.5.4-2.el8.x86_64                    86/150
  Installing       : libatomic_ops-7.6.2-3.el8.x86_64                    87/150
  Installing       : gc-7.6.4-3.el8.x86_64                               88/150
  Installing       : guile-5:2.0.14-7.el8.x86_64                         89/150
  Running scriptlet: guile-5:2.0.14-7.el8.x86_64                         89/150
  Installing       : gdb-headless-8.2-6.el8.x86_64                       90/150
  Installing       : libXxf86vm-1.1.4-9.el8.x86_64                       91/150
  Installing       : libXxf86misc-1.0.4-1.el8.x86_64                     92/150
  Installing       : xorg-x11-server-utils-7.7-27.el8.x86_64             93/150
  Installing       : urw-base35-bookman-fonts-20170801-10.el8.noarch     94/150
  Running scriptlet: urw-base35-bookman-fonts-20170801-10.el8.noarch     94/150
  Installing       : urw-base35-c059-fonts-20170801-10.el8.noarch        95/150
  Running scriptlet: urw-base35-c059-fonts-20170801-10.el8.noarch        95/150
  Installing       : urw-base35-d050000l-fonts-20170801-10.el8.noarch    96/150
  Running scriptlet: urw-base35-d050000l-fonts-20170801-10.el8.noarch    96/150
  Installing       : urw-base35-gothic-fonts-20170801-10.el8.noarch      97/150
  Running scriptlet: urw-base35-gothic-fonts-20170801-10.el8.noarch      97/150
  Installing       : urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.    98/150
  Running scriptlet: urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.    98/150
  Installing       : urw-base35-nimbus-roman-fonts-20170801-10.el8.no    99/150
  Running scriptlet: urw-base35-nimbus-roman-fonts-20170801-10.el8.no    99/150
  Installing       : urw-base35-nimbus-sans-fonts-20170801-10.el8.noa   100/150
  Running scriptlet: urw-base35-nimbus-sans-fonts-20170801-10.el8.noa   100/150
  Installing       : urw-base35-p052-fonts-20170801-10.el8.noarch       101/150
  Running scriptlet: urw-base35-p052-fonts-20170801-10.el8.noarch       101/150
  Installing       : urw-base35-standard-symbols-ps-fonts-20170801-10   102/150
  Running scriptlet: urw-base35-standard-symbols-ps-fonts-20170801-10   102/150
  Installing       : urw-base35-z003-fonts-20170801-10.el8.noarch       103/150
  Running scriptlet: urw-base35-z003-fonts-20170801-10.el8.noarch       103/150
  Installing       : urw-base35-fonts-20170801-10.el8.noarch            104/150
  Installing       : lcms2-2.9-2.el8.x86_64                             105/150
  Running scriptlet: lcms2-2.9-2.el8.x86_64                             105/150
  Installing       : jbig2dec-libs-0.14-2.el8.x86_64                    106/150
  Running scriptlet: jbig2dec-libs-0.14-2.el8.x86_64                    106/150
  Installing       : isl-0.16.1-6.el8.x86_64                            107/150
  Running scriptlet: isl-0.16.1-6.el8.x86_64                            107/150
  Installing       : gcc-8.3.1-4.5.el8.x86_64                           108/150
  Running scriptlet: gcc-8.3.1-4.5.el8.x86_64                           108/150
  Installing       : annobin-8.78-1.el8.x86_64                          109/150
  Installing       : gcc-gdb-plugin-8.3.1-4.5.el8.x86_64                110/150
  Running scriptlet: gcc-gdb-plugin-8.3.1-4.5.el8.x86_64                110/150
  Installing       : systemtap-devel-4.1-6.el8.x86_64                   111/150
  Installing       : google-droid-sans-fonts-20120715-13.el8.noarch     112/150
  Installing       : go-srpm-macros-2-16.el8.noarch                     113/150
  Installing       : ghc-srpm-macros-1.4.2-7.el8.noarch                 114/150
  Installing       : efi-srpm-macros-3-2.el8.noarch                     115/150
  Installing       : dwz-0.12-9.el8.x86_64                              116/150
  Installing       : redhat-rpm-config-120-1.el8.noarch                 117/150
  Installing       : ctags-5.8-22.el8.x86_64                            118/150
  Installing       : source-highlight-3.1.8-16.el8.x86_64               119/150
  Running scriptlet: source-highlight-3.1.8-16.el8.x86_64               119/150
  Installing       : boost-date-time-1.66.0-6.el8.x86_64                120/150
  Running scriptlet: boost-date-time-1.66.0-6.el8.x86_64                120/150
  Installing       : boost-atomic-1.66.0-6.el8.x86_64                   121/150
  Running scriptlet: boost-atomic-1.66.0-6.el8.x86_64                   121/150
  Installing       : dyninst-10.1.0-2.el8.x86_64                        122/150
  Running scriptlet: dyninst-10.1.0-2.el8.x86_64                        122/150
  Running scriptlet: systemtap-runtime-4.1-6.el8.x86_64                 123/150
  Installing       : systemtap-runtime-4.1-6.el8.x86_64                 123/150
  Installing       : systemtap-client-4.1-6.el8.x86_64                  124/150
  Installing       : adobe-mappings-pdf-20180407-1.el8.noarch           125/150
  Installing       : libgs-9.25-5.el8_1.1.x86_64                        126/150
  Installing       : graphviz-2.40.1-39.el8.x86_64                      127/150
  Running scriptlet: graphviz-2.40.1-39.el8.x86_64                      127/150
  Installing       : asciidoc-8.6.10-0.5.20180627gitf7c2274.el8.noarc   128/150
  Installing       : systemtap-4.1-6.el8.x86_64                         129/150
  Running scriptlet: systemtap-4.1-6.el8.x86_64                         129/150
  Installing       : rpm-build-4.14.2-26.el8_1.x86_64                   130/150
  Installing       : gdb-8.2-6.el8.x86_64                               131/150
  Installing       : gcc-c++-8.3.1-4.5.el8.x86_64                       132/150
  Installing       : libtool-2.4.6-25.el8.x86_64                        133/150
  Running scriptlet: libtool-2.4.6-25.el8.x86_64                        133/150
  Installing       : perl-generators-1.10-9.el8.noarch                  134/150
  Installing       : intltool-0.51.0-11.el8.noarch                      135/150
  Installing       : valgrind-devel-1:3.15.0-9.el8.x86_64               136/150
  Running scriptlet: pesign-0.112-25.el8.x86_64                         137/150
  Installing       : pesign-0.112-25.el8.x86_64                         137/150
  Running scriptlet: pesign-0.112-25.el8.x86_64                         137/150
  Installing       : bison-3.0.4-10.el8.x86_64                          138/150
  Running scriptlet: bison-3.0.4-10.el8.x86_64                          138/150
  Installing       : flex-2.6.1-9.el8.x86_64                            139/150
  Running scriptlet: flex-2.6.1-9.el8.x86_64                            139/150
  Installing       : strace-4.24-5.el8.x86_64                           140/150
  Installing       : rpm-sign-4.14.2-26.el8_1.x86_64                    141/150
  Installing       : patchutils-0.3.4-10.el8.x86_64                     142/150
  Installing       : ltrace-0.7.91-27.el8.x86_64                        143/150
  Installing       : jna-4.5.1-5.el8.x86_64                             144/150
  Installing       : diffstat-1.61-7.el8.x86_64                         145/150
  Installing       : byacc-1.9.20170709-4.el8.x86_64                    146/150
  Cleanup          : git-2.18.2-1.el8_1.x86_64                          147/150
  Cleanup          : git-core-doc-2.18.2-1.el8_1.noarch                 148/150
  Cleanup          : perl-Git-2.18.2-1.el8_1.noarch                     149/150
  Cleanup          : git-core-2.18.2-1.el8_1.x86_64                     150/150
  Running scriptlet: guile-5:2.0.14-7.el8.x86_64                        150/150
  Running scriptlet: urw-base35-bookman-fonts-20170801-10.el8.noarch    150/150
  Running scriptlet: urw-base35-c059-fonts-20170801-10.el8.noarch       150/150
  Running scriptlet: urw-base35-d050000l-fonts-20170801-10.el8.noarch   150/150
  Running scriptlet: urw-base35-gothic-fonts-20170801-10.el8.noarch     150/150
  Running scriptlet: urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.   150/150
  Running scriptlet: urw-base35-nimbus-roman-fonts-20170801-10.el8.no   150/150
  Running scriptlet: urw-base35-nimbus-sans-fonts-20170801-10.el8.noa   150/150
  Running scriptlet: urw-base35-p052-fonts-20170801-10.el8.noarch       150/150
  Running scriptlet: urw-base35-standard-symbols-ps-fonts-20170801-10   150/150
  Running scriptlet: urw-base35-z003-fonts-20170801-10.el8.noarch       150/150
  Running scriptlet: git-core-2.18.2-1.el8_1.x86_64                     150/150
  Verifying        : adobe-mappings-cmap-20171205-3.el8.noarch            1/150
  Verifying        : adobe-mappings-cmap-deprecated-20171205-3.el8.no     2/150
  Verifying        : adobe-mappings-pdf-20180407-1.el8.noarch             3/150
  Verifying        : annobin-8.78-1.el8.x86_64                            4/150
  Verifying        : asciidoc-8.6.10-0.5.20180627gitf7c2274.el8.noarc     5/150
  Verifying        : autoconf-2.69-27.el8.noarch                          6/150
  Verifying        : automake-1.16.1-6.el8.noarch                         7/150
  Verifying        : bison-3.0.4-10.el8.x86_64                            8/150
  Verifying        : boost-atomic-1.66.0-6.el8.x86_64                     9/150
  Verifying        : boost-chrono-1.66.0-6.el8.x86_64                    10/150
  Verifying        : boost-date-time-1.66.0-6.el8.x86_64                 11/150
  Verifying        : boost-filesystem-1.66.0-6.el8.x86_64                12/150
  Verifying        : boost-regex-1.66.0-6.el8.x86_64                     13/150
  Verifying        : boost-system-1.66.0-6.el8.x86_64                    14/150
  Verifying        : boost-thread-1.66.0-6.el8.x86_64                    15/150
  Verifying        : boost-timer-1.66.0-6.el8.x86_64                     16/150
  Verifying        : byacc-1.9.20170709-4.el8.x86_64                     17/150
  Verifying        : cpp-8.3.1-4.5.el8.x86_64                            18/150
  Verifying        : ctags-5.8-22.el8.x86_64                             19/150
  Verifying        : diffstat-1.61-7.el8.x86_64                          20/150
  Verifying        : docbook-dtds-1.0-69.el8.noarch                      21/150
  Verifying        : docbook-style-xsl-1.79.2-7.el8.noarch               22/150
  Verifying        : dwz-0.12-9.el8.x86_64                               23/150
  Verifying        : dyninst-10.1.0-2.el8.x86_64                         24/150
  Verifying        : efi-srpm-macros-3-2.el8.noarch                      25/150
  Verifying        : flex-2.6.1-9.el8.x86_64                             26/150
  Verifying        : gc-7.6.4-3.el8.x86_64                               27/150
  Verifying        : gcc-8.3.1-4.5.el8.x86_64                            28/150
  Verifying        : gcc-c++-8.3.1-4.5.el8.x86_64                        29/150
  Verifying        : gcc-gdb-plugin-8.3.1-4.5.el8.x86_64                 30/150
  Verifying        : gd-2.2.5-6.el8.x86_64                               31/150
  Verifying        : gdb-8.2-6.el8.x86_64                                32/150
  Verifying        : gdb-headless-8.2-6.el8.x86_64                       33/150
  Verifying        : ghc-srpm-macros-1.4.2-7.el8.noarch                  34/150
  Verifying        : go-srpm-macros-2-16.el8.noarch                      35/150
  Verifying        : google-droid-sans-fonts-20120715-13.el8.noarch      36/150
  Verifying        : graphviz-2.40.1-39.el8.x86_64                       37/150
  Verifying        : guile-5:2.0.14-7.el8.x86_64                         38/150
  Verifying        : intltool-0.51.0-11.el8.noarch                       39/150
  Verifying        : isl-0.16.1-6.el8.x86_64                             40/150
  Verifying        : jbig2dec-libs-0.14-2.el8.x86_64                     41/150
  Verifying        : jna-4.5.1-5.el8.x86_64                              42/150
  Verifying        : lcms2-2.9-2.el8.x86_64                              43/150
  Verifying        : libICE-1.0.9-15.el8.x86_64                          44/150
  Verifying        : libSM-1.2.3-1.el8.x86_64                            45/150
  Verifying        : libXaw-1.0.13-10.el8.x86_64                         46/150
  Verifying        : libXmu-1.1.2-12.el8.x86_64                          47/150
  Verifying        : libXpm-3.5.12-7.el8.x86_64                          48/150
  Verifying        : libXt-1.1.5-12.el8.x86_64                           49/150
  Verifying        : libXxf86misc-1.0.4-1.el8.x86_64                     50/150
  Verifying        : libXxf86vm-1.1.4-9.el8.x86_64                       51/150
  Verifying        : libatomic_ops-7.6.2-3.el8.x86_64                    52/150
  Verifying        : libbabeltrace-1.5.4-2.el8.x86_64                    53/150
  Verifying        : libgs-9.25-5.el8_1.1.x86_64                         54/150
  Verifying        : libidn-1.34-5.el8.x86_64                            55/150
  Verifying        : libijs-0.35-5.el8.x86_64                            56/150
  Verifying        : libipt-1.6.1-8.el8.x86_64                           57/150
  Verifying        : libmcpp-2.7.2-20.el8.x86_64                         58/150
  Verifying        : libmpc-1.0.2-9.el8.x86_64                           59/150
  Verifying        : libpaper-1.1.24-22.el8.x86_64                       60/150
  Verifying        : librsvg2-2.42.7-3.el8.x86_64                        61/150
  Verifying        : libstdc++-devel-8.3.1-4.5.el8.x86_64                62/150
  Verifying        : libtool-2.4.6-25.el8.x86_64                         63/150
  Verifying        : libwebp-1.0.0-1.el8.x86_64                          64/150
  Verifying        : ltrace-0.7.91-27.el8.x86_64                         65/150
  Verifying        : mcpp-2.7.2-20.el8.x86_64                            66/150
  Verifying        : nspr-4.21.0-2.el8_0.x86_64                          67/150
  Verifying        : nss-3.44.0-9.el8_1.x86_64                           68/150
  Verifying        : nss-softokn-3.44.0-9.el8_1.x86_64                   69/150
  Verifying        : nss-softokn-freebl-3.44.0-9.el8_1.x86_64            70/150
  Verifying        : nss-sysinit-3.44.0-9.el8_1.x86_64                   71/150
  Verifying        : nss-tools-3.44.0-9.el8_1.x86_64                     72/150
  Verifying        : nss-util-3.44.0-9.el8_1.x86_64                      73/150
  Verifying        : ocaml-srpm-macros-5-4.el8.noarch                    74/150
  Verifying        : openblas-srpm-macros-2-2.el8.noarch                 75/150
  Verifying        : openjpeg2-2.3.1-3.el8_1.x86_64                      76/150
  Verifying        : patchutils-0.3.4-10.el8.x86_64                      77/150
  Verifying        : perl-Fedora-VSP-0.001-9.el8.noarch                  78/150
  Verifying        : perl-Thread-Queue-3.13-1.el8.noarch                 79/150
  Verifying        : perl-XML-Parser-2.44-11.el8.x86_64                  80/150
  Verifying        : perl-generators-1.10-9.el8.noarch                   81/150
  Verifying        : perl-srpm-macros-1-25.el8.noarch                    82/150
  Verifying        : pesign-0.112-25.el8.x86_64                          83/150
  Verifying        : python-srpm-macros-3-37.el8.noarch                  84/150
  Verifying        : python3-rpm-macros-3-37.el8.noarch                  85/150
  Verifying        : qt5-srpm-macros-5.11.1-2.el8.noarch                 86/150
  Verifying        : redhat-rpm-config-120-1.el8.noarch                  87/150
  Verifying        : rpm-build-4.14.2-26.el8_1.x86_64                    88/150
  Verifying        : rust-srpm-macros-5-2.el8.noarch                     89/150
  Verifying        : source-highlight-3.1.8-16.el8.x86_64                90/150
  Verifying        : systemtap-4.1-6.el8.x86_64                          91/150
  Verifying        : systemtap-client-4.1-6.el8.x86_64                   92/150
  Verifying        : systemtap-devel-4.1-6.el8.x86_64                    93/150
  Verifying        : systemtap-runtime-4.1-6.el8.x86_64                  94/150
  Verifying        : tbb-2018.2-9.el8.x86_64                             95/150
  Verifying        : urw-base35-bookman-fonts-20170801-10.el8.noarch     96/150
  Verifying        : urw-base35-c059-fonts-20170801-10.el8.noarch        97/150
  Verifying        : urw-base35-d050000l-fonts-20170801-10.el8.noarch    98/150
  Verifying        : urw-base35-fonts-20170801-10.el8.noarch             99/150
  Verifying        : urw-base35-fonts-common-20170801-10.el8.noarch     100/150
  Verifying        : urw-base35-gothic-fonts-20170801-10.el8.noarch     101/150
  Verifying        : urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.   102/150
  Verifying        : urw-base35-nimbus-roman-fonts-20170801-10.el8.no   103/150
  Verifying        : urw-base35-nimbus-sans-fonts-20170801-10.el8.noa   104/150
  Verifying        : urw-base35-p052-fonts-20170801-10.el8.noarch       105/150
  Verifying        : urw-base35-standard-symbols-ps-fonts-20170801-10   106/150
  Verifying        : urw-base35-z003-fonts-20170801-10.el8.noarch       107/150
  Verifying        : valgrind-1:3.15.0-9.el8.x86_64                     108/150
  Verifying        : valgrind-devel-1:3.15.0-9.el8.x86_64               109/150
  Verifying        : vim-filesystem-2:8.0.1763-13.el8.noarch            110/150
  Verifying        : xorg-x11-fonts-ISO8859-1-100dpi-7.5-19.el8.noarc   111/150
  Verifying        : xorg-x11-server-utils-7.7-27.el8.x86_64            112/150
  Verifying        : bzip2-1.0.6-26.el8.x86_64                          113/150
  Verifying        : dnf-plugins-core-4.0.8-3.el8.noarch                114/150
  Verifying        : efivar-libs-36-1.el8.x86_64                        115/150
  Verifying        : elfutils-0.176-5.el8.x86_64                        116/150
  Verifying        : file-5.33-8.el8.x86_64                             117/150
  Verifying        : gettext-0.19.8.1-17.el8.x86_64                     118/150
  Verifying        : gettext-common-devel-0.19.8.1-17.el8.noarch        119/150
  Verifying        : gettext-devel-0.19.8.1-17.el8.x86_64               120/150
  Verifying        : gettext-libs-0.19.8.1-17.el8.x86_64                121/150
  Verifying        : glibc-devel-2.28-72.el8_1.1.x86_64                 122/150
  Verifying        : glibc-headers-2.28-72.el8_1.1.x86_64               123/150
  Verifying        : kernel-headers-4.18.0-147.8.1.el8_1.x86_64         124/150
  Verifying        : libcroco-0.6.12-4.el8.x86_64                       125/150
  Verifying        : libgomp-8.3.1-4.5.el8.x86_64                       126/150
  Verifying        : libicu-60.3-2.el8_1.x86_64                         127/150
  Verifying        : libtool-ltdl-2.4.6-25.el8.x86_64                   128/150
  Verifying        : libxcrypt-devel-4.1.1-4.el8.x86_64                 129/150
  Verifying        : libxslt-1.1.32-3.el8.x86_64                        130/150
  Verifying        : m4-1.4.18-7.el8.x86_64                             131/150
  Verifying        : make-1:4.2.1-9.el8.x86_64                          132/150
  Verifying        : mokutil-1:0.3.0-9.el8.x86_64                       133/150
  Verifying        : patch-2.7.6-9.el8_0.x86_64                         134/150
  Verifying        : python3-dateutil-1:2.6.1-6.el8.noarch              135/150
  Verifying        : python3-dnf-plugins-core-4.0.8-3.el8.noarch        136/150
  Verifying        : python3-six-1.11.0-8.el8.noarch                    137/150
  Verifying        : rpm-sign-4.14.2-26.el8_1.x86_64                    138/150
  Verifying        : sgml-common-0.6.3-50.el8.noarch                    139/150
  Verifying        : strace-4.24-5.el8.x86_64                           140/150
  Verifying        : xml-common-0.6.3-50.el8.noarch                     141/150
  Verifying        : zip-3.0-23.el8.x86_64                              142/150
  Verifying        : git-2.18.2-2.el8_1.x86_64                          143/150
  Verifying        : git-2.18.2-1.el8_1.x86_64                          144/150
  Verifying        : git-core-2.18.2-2.el8_1.x86_64                     145/150
  Verifying        : git-core-2.18.2-1.el8_1.x86_64                     146/150
  Verifying        : git-core-doc-2.18.2-2.el8_1.noarch                 147/150
  Verifying        : git-core-doc-2.18.2-1.el8_1.noarch                 148/150
  Verifying        : perl-Git-2.18.2-2.el8_1.noarch                     149/150
  Verifying        : perl-Git-2.18.2-1.el8_1.noarch                     150/150

Upgraded:
  git-2.18.2-2.el8_1.x86_64                git-core-2.18.2-2.el8_1.x86_64
  git-core-doc-2.18.2-2.el8_1.noarch       perl-Git-2.18.2-2.el8_1.noarch      

Installed:
  asciidoc-8.6.10-0.5.20180627gitf7c2274.el8.noarch
  autoconf-2.69-27.el8.noarch                    
  automake-1.16.1-6.el8.noarch                   
  bison-3.0.4-10.el8.x86_64
  byacc-1.9.20170709-4.el8.x86_64   
  ctags-5.8-22.el8.x86_64  
  diffstat-1.61-7.el8.x86_64        
  flex-2.6.1-9.el8.x86_64  
  gcc-8.3.1-4.5.el8.x86_64 
  gcc-c++-8.3.1-4.5.el8.x86_64      
  gdb-8.2-6.el8.x86_64     
  intltool-0.51.0-11.el8.noarch                  
  jna-4.5.1-5.el8.x86_64   
  libtool-2.4.6-25.el8.x86_64       
  ltrace-0.7.91-27.el8.x86_64       
  patchutils-0.3.4-10.el8.x86_64    
  perl-Fedora-VSP-0.001-9.el8.noarch             
  perl-generators-1.10-9.el8.noarch              
  pesign-0.112-25.el8.x86_64        
  redhat-rpm-config-120-1.el8.noarch             
  rpm-build-4.14.2-26.el8_1.x86_64  
  source-highlight-3.1.8-16.el8.x86_64
  systemtap-4.1-6.el8.x86_64        
  valgrind-1:3.15.0-9.el8.x86_64    
  valgrind-devel-1:3.15.0-9.el8.x86_64
  glibc-devel-2.28-72.el8_1.1.x86_64
  make-1:4.2.1-9.el8.x86_64
  rpm-sign-4.14.2-26.el8_1.x86_64   
  strace-4.24-5.el8.x86_64 
  gcc-gdb-plugin-8.3.1-4.5.el8.x86_64 
  dnf-plugins-core-4.0.8-3.el8.noarch            
  adobe-mappings-cmap-20171205-3.el8.noarch      
  adobe-mappings-cmap-deprecated-20171205-3.el8.noarch                          
  adobe-mappings-pdf-20180407-1.el8.noarch       
  annobin-8.78-1.el8.x86_64
  boost-atomic-1.66.0-6.el8.x86_64  
  boost-chrono-1.66.0-6.el8.x86_64  
  boost-date-time-1.66.0-6.el8.x86_64 
  boost-filesystem-1.66.0-6.el8.x86_64
  boost-regex-1.66.0-6.el8.x86_64   
  boost-system-1.66.0-6.el8.x86_64  
  boost-thread-1.66.0-6.el8.x86_64  
  boost-timer-1.66.0-6.el8.x86_64   
  cpp-8.3.1-4.5.el8.x86_64 
  docbook-dtds-1.0-69.el8.noarch                 
  docbook-style-xsl-1.79.2-7.el8.noarch          
  dwz-0.12-9.el8.x86_64    
  dyninst-10.1.0-2.el8.x86_64       
  efi-srpm-macros-3-2.el8.noarch                 
  gc-7.6.4-3.el8.x86_64    
  gd-2.2.5-6.el8.x86_64    
  gdb-headless-8.2-6.el8.x86_64     
  ghc-srpm-macros-1.4.2-7.el8.noarch             
  go-srpm-macros-2-16.el8.noarch                 
  google-droid-sans-fonts-20120715-13.el8.noarch 
  graphviz-2.40.1-39.el8.x86_64     
  guile-5:2.0.14-7.el8.x86_64       
  isl-0.16.1-6.el8.x86_64  
  jbig2dec-libs-0.14-2.el8.x86_64   
  lcms2-2.9-2.el8.x86_64   
  libICE-1.0.9-15.el8.x86_64        
  libSM-1.2.3-1.el8.x86_64 
  libXaw-1.0.13-10.el8.x86_64       
  libXmu-1.1.2-12.el8.x86_64        
  libXpm-3.5.12-7.el8.x86_64        
  libXt-1.1.5-12.el8.x86_64
  libXxf86misc-1.0.4-1.el8.x86_64   
  libXxf86vm-1.1.4-9.el8.x86_64     
  libatomic_ops-7.6.2-3.el8.x86_64  
  libbabeltrace-1.5.4-2.el8.x86_64  
  libgs-9.25-5.el8_1.1.x86_64       
  libidn-1.34-5.el8.x86_64 
  libijs-0.35-5.el8.x86_64 
  libipt-1.6.1-8.el8.x86_64
  libmcpp-2.7.2-20.el8.x86_64       
  libmpc-1.0.2-9.el8.x86_64
  libpaper-1.1.24-22.el8.x86_64     
  librsvg2-2.42.7-3.el8.x86_64      
  libstdc++-devel-8.3.1-4.5.el8.x86_64
  libwebp-1.0.0-1.el8.x86_64        
  mcpp-2.7.2-20.el8.x86_64 
  nspr-4.21.0-2.el8_0.x86_64        
  nss-3.44.0-9.el8_1.x86_64
  nss-softokn-3.44.0-9.el8_1.x86_64 
  nss-softokn-freebl-3.44.0-9.el8_1.x86_64                                      
  nss-sysinit-3.44.0-9.el8_1.x86_64 
  nss-tools-3.44.0-9.el8_1.x86_64   
  nss-util-3.44.0-9.el8_1.x86_64    
  ocaml-srpm-macros-5-4.el8.noarch               
  openblas-srpm-macros-2-2.el8.noarch            
  openjpeg2-2.3.1-3.el8_1.x86_64    
  perl-Thread-Queue-3.13-1.el8.noarch            
  perl-XML-Parser-2.44-11.el8.x86_64
  perl-srpm-macros-1-25.el8.noarch               
  python-srpm-macros-3-37.el8.noarch             
  python3-rpm-macros-3-37.el8.noarch             
  qt5-srpm-macros-5.11.1-2.el8.noarch            
  rust-srpm-macros-5-2.el8.noarch                
  systemtap-client-4.1-6.el8.x86_64 
  systemtap-devel-4.1-6.el8.x86_64  
  systemtap-runtime-4.1-6.el8.x86_64
  tbb-2018.2-9.el8.x86_64  
  urw-base35-bookman-fonts-20170801-10.el8.noarch
  urw-base35-c059-fonts-20170801-10.el8.noarch   
  urw-base35-d050000l-fonts-20170801-10.el8.noarch 
  urw-base35-fonts-20170801-10.el8.noarch        
  urw-base35-fonts-common-20170801-10.el8.noarch 
  urw-base35-gothic-fonts-20170801-10.el8.noarch 
  urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.noarch                        
  urw-base35-nimbus-roman-fonts-20170801-10.el8.noarch                          
  urw-base35-nimbus-sans-fonts-20170801-10.el8.noarch                           
  urw-base35-p052-fonts-20170801-10.el8.noarch   
  urw-base35-standard-symbols-ps-fonts-20170801-10.el8.noarch                   
  urw-base35-z003-fonts-20170801-10.el8.noarch   
  vim-filesystem-2:8.0.1763-13.el8.noarch        
  xorg-x11-fonts-ISO8859-1-100dpi-7.5-19.el8.noarch
  xorg-x11-server-utils-7.7-27.el8.x86_64                                       
  bzip2-1.0.6-26.el8.x86_64
  efivar-libs-36-1.el8.x86_64       
  elfutils-0.176-5.el8.x86_64       
  file-5.33-8.el8.x86_64   
  gettext-0.19.8.1-17.el8.x86_64    
  gettext-common-devel-0.19.8.1-17.el8.noarch    
  gettext-devel-0.19.8.1-17.el8.x86_64
  gettext-libs-0.19.8.1-17.el8.x86_64 
  glibc-headers-2.28-72.el8_1.1.x86_64
  kernel-headers-4.18.0-147.8.1.el8_1.x86_64                                    
  libcroco-0.6.12-4.el8.x86_64      
  libgomp-8.3.1-4.5.el8.x86_64      
  libicu-60.3-2.el8_1.x86_64        
  libtool-ltdl-2.4.6-25.el8.x86_64  
  libxcrypt-devel-4.1.1-4.el8.x86_64
  libxslt-1.1.32-3.el8.x86_64       
  m4-1.4.18-7.el8.x86_64   
  mokutil-1:0.3.0-9.el8.x86_64      
  patch-2.7.6-9.el8_0.x86_64        
  python3-dateutil-1:2.6.1-6.el8.noarch          
  python3-dnf-plugins-core-4.0.8-3.el8.noarch    
  python3-six-1.11.0-8.el8.noarch                
  sgml-common-0.6.3-50.el8.noarch                
  xml-common-0.6.3-50.el8.noarch                 
  zip-3.0-23.el8.x86_64    

Complete!
Removing intermediate container a639e0aeb7f2
 ---> dbdbea6e7f2c
Step 6/32: RUN dnf -y install langpacks-ja
 ---> Running in 4921451cc1c5
Last metadata expiration check: 0:00:42 ago on Sun Apr 26 05:19:46 2020.
Dependencies resolved.
================================================================================
 Package                Arch        Version                Repository      Size
================================================================================
Installing:
 langpacks-ja           noarch      1.0-12.el8             AppStream      9.6 k
Installing weak dependencies:
 glibc-langpack-ja      x86_64      2.28-72.el8_1.1        BaseOS         323 k

Transaction Summary
================================================================================
Install  2 Packages

Total download size: 333 k
Installed size: 2.1 M
Downloading Packages:
(1/2): langpacks-ja-1.0-12.el8.noarch.rpm       267 kB/s | 9.6 kB     00:00
(2/2): glibc-langpack-ja-2.28-72.el8_1.1.x86_64 3.3 MB/s | 323 kB     00:00
--------------------------------------------------------------------------------
Total                                           374 kB/s | 333 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : glibc-langpack-ja-2.28-72.el8_1.1.x86_64               1/2
  Installing       : langpacks-ja-1.0-12.el8.noarch                         2/2
  Running scriptlet: langpacks-ja-1.0-12.el8.noarch                         2/2
  Verifying        : langpacks-ja-1.0-12.el8.noarch                         1/2
  Verifying        : glibc-langpack-ja-2.28-72.el8_1.1.x86_64               2/2

Installed:
  langpacks-ja-1.0-12.el8.noarch    glibc-langpack-ja-2.28-72.el8_1.1.x86_64

Complete!
Removing intermediate container 4921451cc1c5
 ---> 559eac06783e
Step 7/32: RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
 ---> Running in ef3645ef2449
Removing intermediate container ef3645ef2449
 ---> 2f33f8adf660
Step 8/32: ENV LANG="ja_JP.UTF-8"
 ---> Running in ea1ea27ae26e
Removing intermediate container ea1ea27ae26e
 ---> d8adc66bd9e7
Step 9/32: ENV LANGUAGE="ja_JP:ja"
 ---> Running in f843f076d3ad
Removing intermediate container f843f076d3ad
 ---> 1996be47771d
Step 10/32: ENV LC_ALL="ja_JP.UTF-8"
 ---> Running in cdef39620b22
Removing intermediate container cdef39620b22
 ---> 171c6fbc0810
Step 11/32: ADD asset /asset
 ---> eacd7a1adab8
Step 12/32: RUN dnf -y remove java-1.8.0-openjdk
 ---> Running in ab6b163265a1
Dependencies resolved.
================================================================================
 Package                   Arch    Version                    Repository   Size
================================================================================
Removing:
 java-1.8.0-openjdk        x86_64  1:1.8.0.242.b08-0.el8_1    @AppStream  1.0 M
Removing dependent packages:
 java-1.8.0-openjdk-devel  x86_64  1:1.8.0.242.b08-0.el8_1    @AppStream   41 M
Removing unused dependencies:
 alsa-lib                  x86_64  1.1.9-4.el8                @AppStream  1.3 M
 giflib                    x86_64  5.1.4-3.el8                @AppStream  103 k
 libXtst                   x86_64  1.2.3-7.el8                @AppStream   34 k
 ttmkfdir                  x86_64  3.0.9-54.el8               @AppStream  140 k
 xorg-x11-fonts-Type1      noarch  7.5-19.el8                 @AppStream  863 k

Transaction Summary
================================================================================
Remove  7 Packages

Freed space: 44 M
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Erasing          : java-1.8.0-openjdk-devel-1:1.8.0.242.b08-0.el8_1.x86   1/7
  Running scriptlet: java-1.8.0-openjdk-devel-1:1.8.0.242.b08-0.el8_1.x86   1/7
  Erasing          : java-1.8.0-openjdk-1:1.8.0.242.b08-0.el8_1.x86_64      2/7
  Running scriptlet: java-1.8.0-openjdk-1:1.8.0.242.b08-0.el8_1.x86_64      2/7
  Erasing          : xorg-x11-fonts-Type1-7.5-19.el8.noarch                 3/7
  Running scriptlet: xorg-x11-fonts-Type1-7.5-19.el8.noarch                 3/7
  Erasing          : ttmkfdir-3.0.9-54.el8.x86_64                           4/7
  Erasing          : libXtst-1.2.3-7.el8.x86_64                             5/7
  Erasing          : alsa-lib-1.1.9-4.el8.x86_64                            6/7
  Running scriptlet: alsa-lib-1.1.9-4.el8.x86_64                            6/7
  Erasing          : giflib-5.1.4-3.el8.x86_64                              7/7
  Running scriptlet: giflib-5.1.4-3.el8.x86_64                              7/7
  Verifying        : alsa-lib-1.1.9-4.el8.x86_64                            1/7
  Verifying        : giflib-5.1.4-3.el8.x86_64                              2/7
  Verifying        : java-1.8.0-openjdk-1:1.8.0.242.b08-0.el8_1.x86_64      3/7
  Verifying        : java-1.8.0-openjdk-devel-1:1.8.0.242.b08-0.el8_1.x86   4/7
  Verifying        : libXtst-1.2.3-7.el8.x86_64                             5/7
  Verifying        : ttmkfdir-3.0.9-54.el8.x86_64                           6/7
  Verifying        : xorg-x11-fonts-Type1-7.5-19.el8.noarch                 7/7

Removed:
  java-1.8.0-openjdk-1:1.8.0.242.b08-0.el8_1.x86_64
  java-1.8.0-openjdk-devel-1:1.8.0.242.b08-0.el8_1.x86_64
  alsa-lib-1.1.9-4.el8.x86_64
  giflib-5.1.4-3.el8.x86_64
  libXtst-1.2.3-7.el8.x86_64
  ttmkfdir-3.0.9-54.el8.x86_64
  xorg-x11-fonts-Type1-7.5-19.el8.noarch

Complete!
Removing intermediate container ab6b163265a1
 ---> b559044e04a3
Step 13/32: RUN rm -rf /usr/lib/jvm/*
 ---> Running in 28099945cece
Removing intermediate container 28099945cece
 ---> b272741e404b
Step 14/32: RUN rpm -Uvh --force /asset/jdk-8u101-linux-x64.rpm
 ---> Running in cbf2ff46f1c6
Verifying...                          ########################################
Preparing...                          ########################################
Updating / installing...
jdk1.8.0_101-2000:1.8.0_101-fcs       ########################################
Unpacking JAR files...
	tools.jar...
	plugin.jar...
	javaws.jar...
	deploy.jar...
	rt.jar...
	jsse.jar...
	charsets.jar...
	localedata.jar...
Removing intermediate container cbf2ff46f1c6
 ---> 0a0646ca7f2a
Step 15/32: ENV JAVA_HOME=/usr/java/jdk1.8.0_101
 ---> Running in 652a3ec36ae2
Removing intermediate container 652a3ec36ae2
 ---> c690af7e3673
Step 16/32: ENV PATH $PATH:$JAVA_HOME/bin
 ---> Running in f7e1d8e2ba33
Removing intermediate container f7e1d8e2ba33
 ---> 8bf9df9f08ae
Step 17/32: RUN tar xvzf /asset/apache-maven-3.6.3-bin.tar.gz -C /opt/
 ---> Running in 86da7f471645
apache-maven-3.6.3/README.txt
apache-maven-3.6.3/LICENSE
apache-maven-3.6.3/NOTICE
apache-maven-3.6.3/lib/
apache-maven-3.6.3/lib/cdi-api.license
apache-maven-3.6.3/lib/commons-cli.license
apache-maven-3.6.3/lib/commons-io.license
apache-maven-3.6.3/lib/commons-lang3.license
apache-maven-3.6.3/lib/guava.license
apache-maven-3.6.3/lib/guice.license
apache-maven-3.6.3/lib/jansi.license
apache-maven-3.6.3/lib/javax.inject.license
apache-maven-3.6.3/lib/jcl-over-slf4j.license
apache-maven-3.6.3/lib/jsoup.license
apache-maven-3.6.3/lib/jsr250-api.license
apache-maven-3.6.3/lib/org.eclipse.sisu.inject.license
apache-maven-3.6.3/lib/org.eclipse.sisu.plexus.license
apache-maven-3.6.3/lib/plexus-cipher.license
apache-maven-3.6.3/lib/plexus-component-annotations.license
apache-maven-3.6.3/lib/plexus-interpolation.license
apache-maven-3.6.3/lib/plexus-sec-dispatcher.license
apache-maven-3.6.3/lib/plexus-utils.license
apache-maven-3.6.3/lib/slf4j-api.license
apache-maven-3.6.3/boot/
apache-maven-3.6.3/boot/plexus-classworlds.license
apache-maven-3.6.3/lib/jansi-native/
apache-maven-3.6.3/lib/jansi-native/freebsd32/
apache-maven-3.6.3/lib/jansi-native/freebsd64/
apache-maven-3.6.3/lib/jansi-native/linux32/
apache-maven-3.6.3/lib/jansi-native/linux64/
apache-maven-3.6.3/lib/jansi-native/osx/
apache-maven-3.6.3/lib/jansi-native/windows32/
apache-maven-3.6.3/lib/jansi-native/windows64/
apache-maven-3.6.3/lib/jansi-native/freebsd32/libjansi.so
apache-maven-3.6.3/lib/jansi-native/freebsd64/libjansi.so
apache-maven-3.6.3/lib/jansi-native/linux32/libjansi.so
apache-maven-3.6.3/lib/jansi-native/linux64/libjansi.so
apache-maven-3.6.3/lib/jansi-native/osx/libjansi.jnilib
apache-maven-3.6.3/lib/jansi-native/windows32/jansi.dll
apache-maven-3.6.3/lib/jansi-native/windows64/jansi.dll
apache-maven-3.6.3/bin/m2.conf
apache-maven-3.6.3/bin/mvn.cmd
apache-maven-3.6.3/bin/mvnDebug.cmd
apache-maven-3.6.3/bin/mvn
apache-maven-3.6.3/bin/mvnDebug
apache-maven-3.6.3/bin/mvnyjp
apache-maven-3.6.3/conf/
apache-maven-3.6.3/conf/logging/
apache-maven-3.6.3/conf/logging/simplelogger.properties
apache-maven-3.6.3/conf/settings.xml
apache-maven-3.6.3/conf/toolchains.xml
apache-maven-3.6.3/lib/ext/
apache-maven-3.6.3/lib/jansi-native/
apache-maven-3.6.3/lib/ext/README.txt
apache-maven-3.6.3/lib/jansi-native/README.txt
apache-maven-3.6.3/boot/plexus-classworlds-2.6.0.jar
apache-maven-3.6.3/lib/maven-embedder-3.6.3.jar
apache-maven-3.6.3/lib/maven-settings-3.6.3.jar
apache-maven-3.6.3/lib/maven-settings-builder-3.6.3.jar
apache-maven-3.6.3/lib/maven-plugin-api-3.6.3.jar
apache-maven-3.6.3/lib/maven-model-3.6.3.jar
apache-maven-3.6.3/lib/maven-model-builder-3.6.3.jar
apache-maven-3.6.3/lib/maven-builder-support-3.6.3.jar
apache-maven-3.6.3/lib/maven-resolver-api-1.4.1.jar
apache-maven-3.6.3/lib/maven-resolver-util-1.4.1.jar
apache-maven-3.6.3/lib/maven-shared-utils-3.2.1.jar
apache-maven-3.6.3/lib/commons-io-2.5.jar
apache-maven-3.6.3/lib/guice-4.2.1-no_aop.jar
apache-maven-3.6.3/lib/guava-25.1-android.jar
apache-maven-3.6.3/lib/javax.inject-1.jar
apache-maven-3.6.3/lib/jsr250-api-1.0.jar
apache-maven-3.6.3/lib/plexus-utils-3.2.1.jar
apache-maven-3.6.3/lib/plexus-sec-dispatcher-1.4.jar
apache-maven-3.6.3/lib/plexus-cipher-1.7.jar
apache-maven-3.6.3/lib/slf4j-api-1.7.29.jar
apache-maven-3.6.3/lib/commons-lang3-3.8.1.jar
apache-maven-3.6.3/lib/maven-core-3.6.3.jar
apache-maven-3.6.3/lib/maven-repository-metadata-3.6.3.jar
apache-maven-3.6.3/lib/maven-artifact-3.6.3.jar
apache-maven-3.6.3/lib/maven-resolver-provider-3.6.3.jar
apache-maven-3.6.3/lib/maven-resolver-impl-1.4.1.jar
apache-maven-3.6.3/lib/maven-resolver-spi-1.4.1.jar
apache-maven-3.6.3/lib/org.eclipse.sisu.inject-0.3.4.jar
apache-maven-3.6.3/lib/plexus-component-annotations-2.1.0.jar
apache-maven-3.6.3/lib/maven-compat-3.6.3.jar
apache-maven-3.6.3/lib/plexus-interpolation-1.25.jar
apache-maven-3.6.3/lib/wagon-provider-api-3.3.4.jar
apache-maven-3.6.3/lib/org.eclipse.sisu.plexus-0.3.4.jar
apache-maven-3.6.3/lib/cdi-api-1.0.jar
apache-maven-3.6.3/lib/commons-cli-1.4.jar
apache-maven-3.6.3/lib/wagon-http-3.3.4-shaded.jar
apache-maven-3.6.3/lib/jsoup-1.12.1.jar
apache-maven-3.6.3/lib/jcl-over-slf4j-1.7.29.jar
apache-maven-3.6.3/lib/wagon-file-3.3.4.jar
apache-maven-3.6.3/lib/maven-resolver-connector-basic-1.4.1.jar
apache-maven-3.6.3/lib/maven-resolver-transport-wagon-1.4.1.jar
apache-maven-3.6.3/lib/maven-slf4j-provider-3.6.3.jar
apache-maven-3.6.3/lib/jansi-1.17.1.jar
Removing intermediate container 86da7f471645
 ---> 329fb2091a1e
Step 18/32: RUN ln -s /opt/apache-maven-3.6.3 /opt/apache-maven
 ---> Running in 40139439bb5f
Removing intermediate container 40139439bb5f
 ---> 85fc2ef50af7
Step 19/32: RUN echo 'export PATH=$PATH:/opt/apache-maven/bin' | tee -a /etc/profile
 ---> Running in b91751bb82d0
export PATH=$PATH:/opt/apache-maven/bin
Removing intermediate container b91751bb82d0
 ---> 2792025961b7
Step 20/32: COPY /asset/plugin.txt /usr/share/jenkins/ref/plugins.txt
 ---> b9dd81f3c8e6
Step 21/32: RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
 ---> Running in c7349777c633
Creating initial locks...
Analyzing war /usr/share/jenkins/jenkins.war...
Registering preinstalled plugins...
Downloading plugins...
Downloading plugin: ace-editor from https://updates.jenkins.io/download/plugins/ace-editor/1.1/ace-editor.hpi
Downloading plugin: blueocean-commons from https://updates.jenkins.io/download/plugins/blueocean-commons/1.23.0/blueocean-commons.hpi
Downloading plugin: blueocean-bitbucket-pipeline from https://updates.jenkins.io/download/plugins/blueocean-bitbucket-pipeline/1.23.0/blueocean-bitbucket-pipeline.hpi
Downloading plugin: antisamy-markup-formatter from https://updates.jenkins.io/download/plugins/antisamy-markup-formatter/2.0/antisamy-markup-formatter.hpi
Downloading plugin: authentication-tokens from https://updates.jenkins.io/download/plugins/authentication-tokens/1.3/authentication-tokens.hpi
Downloading plugin: apache-httpcomponents-client-4-api from https://updates.jenkins.io/download/plugins/apache-httpcomponents-client-4-api/4.5.10-2.0/apache-httpcomponents-client-4-api.hpi
Downloading plugin: cloudbees-bitbucket-branch-source from https://updates.jenkins.io/download/plugins/cloudbees-bitbucket-branch-source/2.7.0/cloudbees-bitbucket-branch-source.hpi
Downloading plugin: blueocean-display-url from https://updates.jenkins.io/download/plugins/blueocean-display-url/2.3.1/blueocean-display-url.hpi
Downloading plugin: durable-task from https://updates.jenkins.io/download/plugins/durable-task/1.34/durable-task.hpi
Downloading plugin: docker-workflow from https://updates.jenkins.io/download/plugins/docker-workflow/1.23/docker-workflow.hpi
Downloading plugin: cloudbees-folder from https://updates.jenkins.io/download/plugins/cloudbees-folder/6.12/cloudbees-folder.hpi
Downloading plugin: build-timeout from https://updates.jenkins.io/download/plugins/build-timeout/1.19.1/build-timeout.hpi
Downloading plugin: analysis-model-api from https://updates.jenkins.io/download/plugins/analysis-model-api/8.0.1/analysis-model-api.hpi
Downloading plugin: data-tables-api from https://updates.jenkins.io/download/plugins/data-tables-api/1.10.20-14/data-tables-api.hpi
Downloading plugin: docker-commons from https://updates.jenkins.io/download/plugins/docker-commons/1.16/docker-commons.hpi
Downloading plugin: blueocean-autofavorite from https://updates.jenkins.io/download/plugins/blueocean-autofavorite/1.2.4/blueocean-autofavorite.hpi
Downloading plugin: blueocean-events from https://updates.jenkins.io/download/plugins/blueocean-events/1.23.0/blueocean-events.hpi
Downloading plugin: docker-compose-build-step from https://updates.jenkins.io/download/plugins/docker-compose-build-step/1.0/docker-compose-build-step.hpi
Downloading plugin: blueocean from https://updates.jenkins.io/download/plugins/blueocean/1.23.0/blueocean.hpi
Downloading plugin: blueocean-dashboard from https://updates.jenkins.io/download/plugins/blueocean-dashboard/1.23.0/blueocean-dashboard.hpi
Downloading plugin: docker-plugin from https://updates.jenkins.io/download/plugins/docker-plugin/1.2.0/docker-plugin.hpi
Downloading plugin: dashboard-view from https://updates.jenkins.io/download/plugins/dashboard-view/2.12/dashboard-view.hpi
Downloading plugin: credentials-binding from https://updates.jenkins.io/download/plugins/credentials-binding/1.22/credentials-binding.hpi
Downloading plugin: coverity from https://updates.jenkins.io/download/plugins/coverity/1.11.4/coverity.hpi
Downloading plugin: command-launcher from https://updates.jenkins.io/download/plugins/command-launcher/1.4/command-launcher.hpi
Downloading plugin: config-file-provider from https://updates.jenkins.io/download/plugins/config-file-provider/3.6.3/config-file-provider.hpi
Downloading plugin: blueocean-core-js from https://updates.jenkins.io/download/plugins/blueocean-core-js/1.23.0/blueocean-core-js.hpi
Downloading plugin: credentials from https://updates.jenkins.io/download/plugins/credentials/2.3.7/credentials.hpi
Downloading plugin: blueocean-config from https://updates.jenkins.io/download/plugins/blueocean-config/1.23.0/blueocean-config.hpi
Downloading plugin: ansible from https://updates.jenkins.io/download/plugins/ansible/1.0/ansible.hpi
Downloading plugin: ant from https://updates.jenkins.io/download/plugins/ant/1.11/ant.hpi
Downloading plugin: ansible-tower from https://updates.jenkins.io/download/plugins/ansible-tower/0.14.1/ansible-tower.hpi
Downloading plugin: blueocean-git-pipeline from https://updates.jenkins.io/download/plugins/blueocean-git-pipeline/1.23.0/blueocean-git-pipeline.hpi
Downloading plugin: blueocean-i18n from https://updates.jenkins.io/download/plugins/blueocean-i18n/1.23.0/blueocean-i18n.hpi
Downloading plugin: blueocean-github-pipeline from https://updates.jenkins.io/download/plugins/blueocean-github-pipeline/1.23.0/blueocean-github-pipeline.hpi
Downloading plugin: blueocean-pipeline-scm-api from https://updates.jenkins.io/download/plugins/blueocean-pipeline-scm-api/1.23.0/blueocean-pipeline-scm-api.hpi
Downloading plugin: blueocean-rest from https://updates.jenkins.io/download/plugins/blueocean-rest/1.23.0/blueocean-rest.hpi
Downloading plugin: branch-api from https://updates.jenkins.io/download/plugins/branch-api/2.5.6/branch-api.hpi
Downloading plugin: blueocean-personalization from https://updates.jenkins.io/download/plugins/blueocean-personalization/1.23.0/blueocean-personalization.hpi
Downloading plugin: blueocean-pipeline-editor from https://updates.jenkins.io/download/plugins/blueocean-pipeline-editor/1.23.0/blueocean-pipeline-editor.hpi
Downloading plugin: docker-slaves from https://updates.jenkins.io/download/plugins/docker-slaves/1.0.7/docker-slaves.hpi
Downloading plugin: blueocean-pipeline-api-impl from https://updates.jenkins.io/download/plugins/blueocean-pipeline-api-impl/1.23.0/blueocean-pipeline-api-impl.hpi
Downloading plugin: blueocean-rest-impl from https://updates.jenkins.io/download/plugins/blueocean-rest-impl/1.23.0/blueocean-rest-impl.hpi
Downloading plugin: blueocean-web from https://updates.jenkins.io/download/plugins/blueocean-web/1.23.0/blueocean-web.hpi
Downloading plugin: blueocean-jira from https://updates.jenkins.io/download/plugins/blueocean-jira/1.23.0/blueocean-jira.hpi
Downloading plugin: docker-java-api from https://updates.jenkins.io/download/plugins/docker-java-api/3.1.5.2/docker-java-api.hpi
Downloading plugin: bootstrap4-api from https://updates.jenkins.io/download/plugins/bootstrap4-api/4.4.1-12/bootstrap4-api.hpi
Downloading plugin: docker-build-step from https://updates.jenkins.io/download/plugins/docker-build-step/2.4/docker-build-step.hpi
Downloading plugin: bouncycastle-api from https://updates.jenkins.io/download/plugins/bouncycastle-api/2.18/bouncycastle-api.hpi
Downloading plugin: blueocean-executor-info from https://updates.jenkins.io/download/plugins/blueocean-executor-info/1.23.0/blueocean-executor-info.hpi
Downloading plugin: build-environment from https://updates.jenkins.io/download/plugins/build-environment/1.7/build-environment.hpi
Downloading plugin: display-url-api from https://updates.jenkins.io/download/plugins/display-url-api/2.3.2/display-url-api.hpi
Downloading plugin: jackson2-api from https://updates.jenkins.io/download/plugins/jackson2-api/2.10.3/jackson2-api.hpi
Downloading plugin: git-server from https://updates.jenkins.io/download/plugins/git-server/1.9/git-server.hpi
Downloading plugin: github-api from https://updates.jenkins.io/download/plugins/github-api/1.111/github-api.hpi
Downloading plugin: github-branch-source from https://updates.jenkins.io/download/plugins/github-branch-source/2.7.1/github-branch-source.hpi
Downloading plugin: git-parameter from https://updates.jenkins.io/download/plugins/git-parameter/0.9.12/git-parameter.hpi
Downloading plugin: gitlab-plugin from https://updates.jenkins.io/download/plugins/gitlab-plugin/1.5.13/gitlab-plugin.hpi
Downloading plugin: gitlab-branch-source from https://updates.jenkins.io/download/plugins/gitlab-branch-source/1.4.6/gitlab-branch-source.hpi
Downloading plugin: gitlab-oauth from https://updates.jenkins.io/download/plugins/gitlab-oauth/1.5/gitlab-oauth.hpi
Downloading plugin: blueocean-jwt from https://updates.jenkins.io/download/plugins/blueocean-jwt/1.23.0/blueocean-jwt.hpi
Downloading plugin: image-tag-parameter from https://updates.jenkins.io/download/plugins/image-tag-parameter/1.2/image-tag-parameter.hpi
Downloading plugin: echarts-api from https://updates.jenkins.io/download/plugins/echarts-api/4.6.0-9/echarts-api.hpi
Downloading plugin: email-ext from https://updates.jenkins.io/download/plugins/email-ext/2.69/email-ext.hpi
Downloading plugin: font-awesome-api from https://updates.jenkins.io/download/plugins/font-awesome-api/5.13.0-1/font-awesome-api.hpi
Downloading plugin: docker-swarm from https://updates.jenkins.io/download/plugins/docker-swarm/1.9/docker-swarm.hpi
Downloading plugin: generic-webhook-trigger from https://updates.jenkins.io/download/plugins/generic-webhook-trigger/1.67/generic-webhook-trigger.hpi
Downloading plugin: forensics-api from https://updates.jenkins.io/download/plugins/forensics-api/0.7.0/forensics-api.hpi
Downloading plugin: favorite from https://updates.jenkins.io/download/plugins/favorite/2.3.2/favorite.hpi
Downloading plugin: git from https://updates.jenkins.io/download/plugins/git/4.2.2/git.hpi
Downloading plugin: git-changelog from https://updates.jenkins.io/download/plugins/git-changelog/2.23/git-changelog.hpi
Downloading plugin: git-client from https://updates.jenkins.io/download/plugins/git-client/3.2.1/git-client.hpi
Downloading plugin: jdk-tool from https://updates.jenkins.io/download/plugins/jdk-tool/1.4/jdk-tool.hpi
Downloading plugin: github from https://updates.jenkins.io/download/plugins/github/1.29.5/github.hpi
Downloading plugin: htmlpublisher from https://updates.jenkins.io/download/plugins/htmlpublisher/1.22/htmlpublisher.hpi
Downloading plugin: google-oauth-plugin from https://updates.jenkins.io/download/plugins/google-oauth-plugin/1.0.0/google-oauth-plugin.hpi
Downloading plugin: gradle from https://updates.jenkins.io/download/plugins/gradle/1.36/gradle.hpi
Downloading plugin: h2-api from https://updates.jenkins.io/download/plugins/h2-api/1.4.199/h2-api.hpi
Downloading plugin: handy-uri-templates-2-api from https://updates.jenkins.io/download/plugins/handy-uri-templates-2-api/2.1.8-1.0/handy-uri-templates-2-api.hpi
Downloading plugin: jsch from https://updates.jenkins.io/download/plugins/jsch/0.1.55.2/jsch.hpi
Downloading plugin: mercurial from https://updates.jenkins.io/download/plugins/mercurial/2.10/mercurial.hpi
Downloading plugin: gitlab-api from https://updates.jenkins.io/download/plugins/gitlab-api/1.0.6/gitlab-api.hpi
Downloading plugin: kubernetes from https://updates.jenkins.io/download/plugins/kubernetes/1.25.3/kubernetes.hpi
Downloading plugin: jquery from https://updates.jenkins.io/download/plugins/jquery/1.12.4-1/jquery.hpi
Downloading plugin: ldap from https://updates.jenkins.io/download/plugins/ldap/1.24/ldap.hpi
Downloading plugin: matrix-auth from https://updates.jenkins.io/download/plugins/matrix-auth/2.5/matrix-auth.hpi
Downloading plugin: jira from https://updates.jenkins.io/download/plugins/jira/3.0.15/jira.hpi
Downloading plugin: javadoc from https://updates.jenkins.io/download/plugins/javadoc/1.5/javadoc.hpi
Downloading plugin: handlebars from https://updates.jenkins.io/download/plugins/handlebars/1.1.1/handlebars.hpi
Downloading plugin: jenkins-design-language from https://updates.jenkins.io/download/plugins/jenkins-design-language/1.23.0/jenkins-design-language.hpi
Downloading plugin: list-git-branches-parameter from https://updates.jenkins.io/download/plugins/list-git-branches-parameter/0.0.8/list-git-branches-parameter.hpi
Downloading plugin: plugin-util-api from https://updates.jenkins.io/download/plugins/plugin-util-api/1.2.0/plugin-util-api.hpi
Downloading plugin: plain-credentials from https://updates.jenkins.io/download/plugins/plain-credentials/1.7/plain-credentials.hpi
Downloading plugin: popper-api from https://updates.jenkins.io/download/plugins/popper-api/1.16.0-6/popper-api.hpi
Downloading plugin: kubernetes-credentials from https://updates.jenkins.io/download/plugins/kubernetes-credentials/0.6.2/kubernetes-credentials.hpi
Downloading plugin: jquery-detached from https://updates.jenkins.io/download/plugins/jquery-detached/1.2.1/jquery-detached.hpi
Downloading plugin: structs from https://updates.jenkins.io/download/plugins/structs/1.20/structs.hpi
Downloading plugin: matrix-project from https://updates.jenkins.io/download/plugins/matrix-project/1.14/matrix-project.hpi
Downloading plugin: pipeline-stage-step from https://updates.jenkins.io/download/plugins/pipeline-stage-step/2.3/pipeline-stage-step.hpi
Downloading plugin: kubernetes-client-api from https://updates.jenkins.io/download/plugins/kubernetes-client-api/4.9.1-1/kubernetes-client-api.hpi
Downloading plugin: junit from https://updates.jenkins.io/download/plugins/junit/1.28/junit.hpi
Downloading plugin: jquery3-api from https://updates.jenkins.io/download/plugins/jquery3-api/3.4.1-10/jquery3-api.hpi
Downloading plugin: lockable-resources from https://updates.jenkins.io/download/plugins/lockable-resources/2.7/lockable-resources.hpi
Downloading plugin: mapdb-api from https://updates.jenkins.io/download/plugins/mapdb-api/1.0.9.0/mapdb-api.hpi
Downloading plugin: maven-plugin from https://updates.jenkins.io/download/plugins/maven-plugin/3.6/maven-plugin.hpi
Downloading plugin: mailer from https://updates.jenkins.io/download/plugins/mailer/1.32/mailer.hpi
Downloading plugin: ssh-agent from https://updates.jenkins.io/download/plugins/ssh-agent/1.19/ssh-agent.hpi
Downloading plugin: pipeline-rest-api from https://updates.jenkins.io/download/plugins/pipeline-rest-api/2.13/pipeline-rest-api.hpi
Downloading plugin: token-macro from https://updates.jenkins.io/download/plugins/token-macro/2.12/token-macro.hpi
Downloading plugin: scm-api from https://updates.jenkins.io/download/plugins/scm-api/2.6.3/scm-api.hpi
Downloading plugin: sse-gateway from https://updates.jenkins.io/download/plugins/sse-gateway/1.23/sse-gateway.hpi
Downloading plugin: resource-disposer from https://updates.jenkins.io/download/plugins/resource-disposer/0.14/resource-disposer.hpi
Downloading plugin: rebuild from https://updates.jenkins.io/download/plugins/rebuild/1.31/rebuild.hpi
Downloading plugin: pam-auth from https://updates.jenkins.io/download/plugins/pam-auth/1.6/pam-auth.hpi
Downloading plugin: script-security from https://updates.jenkins.io/download/plugins/script-security/1.71/script-security.hpi
Downloading plugin: pipeline-input-step from https://updates.jenkins.io/download/plugins/pipeline-input-step/2.11/pipeline-input-step.hpi
Downloading plugin: pipeline-stage-view from https://updates.jenkins.io/download/plugins/pipeline-stage-view/2.13/pipeline-stage-view.hpi
Downloading plugin: pipeline-maven from https://updates.jenkins.io/download/plugins/pipeline-maven/3.8.2/pipeline-maven.hpi
Downloading plugin: pipeline-milestone-step from https://updates.jenkins.io/download/plugins/pipeline-milestone-step/1.3.1/pipeline-milestone-step.hpi
Downloading plugin: pubsub-light from https://updates.jenkins.io/download/plugins/pubsub-light/1.13/pubsub-light.hpi
Downloading plugin: trilead-api from https://updates.jenkins.io/download/plugins/trilead-api/1.0.6/trilead-api.hpi
Downloading plugin: workflow-cps from https://updates.jenkins.io/download/plugins/workflow-cps/2.80/workflow-cps.hpi
Downloading plugin: workflow-cps-global-lib from https://updates.jenkins.io/download/plugins/workflow-cps-global-lib/2.16/workflow-cps-global-lib.hpi
Downloading plugin: timestamper from https://updates.jenkins.io/download/plugins/timestamper/1.11.3/timestamper.hpi
Downloading plugin: ssh-slaves from https://updates.jenkins.io/download/plugins/ssh-slaves/1.31.2/ssh-slaves.hpi
Downloading plugin: view-job-filters from https://updates.jenkins.io/download/plugins/view-job-filters/2.2/view-job-filters.hpi
Downloading plugin: variant from https://updates.jenkins.io/download/plugins/variant/1.3/variant.hpi
Downloading plugin: pipeline-model-extensions from https://updates.jenkins.io/download/plugins/pipeline-model-extensions/1.6.0/pipeline-model-extensions.hpi
Downloading plugin: workflow-api from https://updates.jenkins.io/download/plugins/workflow-api/2.40/workflow-api.hpi
Downloading plugin: pipeline-stage-tags-metadata from https://updates.jenkins.io/download/plugins/pipeline-stage-tags-metadata/1.6.0/pipeline-stage-tags-metadata.hpi
Downloading plugin: pipeline-model-api from https://updates.jenkins.io/download/plugins/pipeline-model-api/1.6.0/pipeline-model-api.hpi
Downloading plugin: pipeline-model-declarative-agent from https://updates.jenkins.io/download/plugins/pipeline-model-declarative-agent/1.1.1/pipeline-model-declarative-agent.hpi
Downloading plugin: pipeline-graph-analysis from https://updates.jenkins.io/download/plugins/pipeline-graph-analysis/1.10/pipeline-graph-analysis.hpi
Downloading plugin: ssh-credentials from https://updates.jenkins.io/download/plugins/ssh-credentials/1.18.1/ssh-credentials.hpi
Downloading plugin: subversion from https://updates.jenkins.io/download/plugins/subversion/2.13.1/subversion.hpi
Downloading plugin: pipeline-model-definition from https://updates.jenkins.io/download/plugins/pipeline-model-definition/1.6.0/pipeline-model-definition.hpi
Downloading plugin: violation-comments-to-gitlab from https://updates.jenkins.io/download/plugins/violation-comments-to-gitlab/2.39/violation-comments-to-gitlab.hpi
Downloading plugin: workflow-support from https://updates.jenkins.io/download/plugins/workflow-support/3.4/workflow-support.hpi
Downloading plugin: workflow-job from https://updates.jenkins.io/download/plugins/workflow-job/2.38/workflow-job.hpi
Downloading plugin: workflow-durable-task-step from https://updates.jenkins.io/download/plugins/workflow-durable-task-step/2.35/workflow-durable-task-step.hpi
Downloading plugin: workflow-basic-steps from https://updates.jenkins.io/download/plugins/workflow-basic-steps/2.20/workflow-basic-steps.hpi
Downloading plugin: warnings-ng from https://updates.jenkins.io/download/plugins/warnings-ng/8.1.0/warnings-ng.hpi
Downloading plugin: workflow-aggregator from https://updates.jenkins.io/download/plugins/workflow-aggregator/2.6/workflow-aggregator.hpi
Downloading plugin: ws-cleanup from https://updates.jenkins.io/download/plugins/ws-cleanup/0.38/ws-cleanup.hpi
Downloading plugin: oauth-credentials from https://updates.jenkins.io/download/plugins/oauth-credentials/0.4/oauth-credentials.hpi
Downloading plugin: pipeline-build-step from https://updates.jenkins.io/download/plugins/pipeline-build-step/2.12/pipeline-build-step.hpi
Downloading plugin: pipeline-github-lib from https://updates.jenkins.io/download/plugins/pipeline-github-lib/1.0/pipeline-github-lib.hpi
Downloading plugin: synopsys-coverity from https://updates.jenkins.io/download/plugins/synopsys-coverity/2.3.1/synopsys-coverity.hpi
Downloading plugin: next-executions from https://updates.jenkins.io/download/plugins/next-executions/1.0.15/next-executions.hpi
Downloading plugin: momentjs from https://updates.jenkins.io/download/plugins/momentjs/1.1.1/momentjs.hpi
Downloading plugin: workflow-step-api from https://updates.jenkins.io/download/plugins/workflow-step-api/2.22/workflow-step-api.hpi
Downloading plugin: workflow-scm-step from https://updates.jenkins.io/download/plugins/workflow-scm-step/2.11/workflow-scm-step.hpi
Downloading plugin: workflow-multibranch from https://updates.jenkins.io/download/plugins/workflow-multibranch/2.21/workflow-multibranch.hpi
 > blueocean-i18n depends on blueocean-rest:1.23.0
 > blueocean-display-url depends on blueocean-rest:1.16.0,workflow-job:2.32,workflow-multibranch:2.21,display-url-api:2.3.2,scm-api:2.4.1,script-security:1.58,structs:1.19
 > blueocean depends on blueocean-bitbucket-pipeline:1.23.0,blueocean-commons:1.23.0,blueocean-config:1.23.0,blueocean-core-js:1.23.0,blueocean-dashboard:1.23.0,blueocean-events:1.23.0,blueocean-git-pipeline:1.23.0,blueocean-github-pipeline:1.23.0,blueocean-i18n:1.23.0,blueocean-jira:1.23.0,blueocean-jwt:1.23.0,blueocean-personalization:1.23.0,blueocean-pipeline-api-impl:1.23.0,blueocean-pipeline-editor:1.23.0,blueocean-rest-impl:1.23.0,blueocean-rest:1.23.0,blueocean-web:1.23.0,jenkins-design-language:1.23.0,blueocean-autofavorite:1.2.3,blueocean-display-url:2.2.0,pipeline-build-step:2.7,pipeline-milestone-step:1.3.1
 > cloudbees-bitbucket-branch-source depends on apache-httpcomponents-client-4-api:4.5.10-1.0,authentication-tokens:1.3,display-url-api:2.3.2,git:3.12.1,handy-uri-templates-2-api:2.1.6-1.0,jackson2-api:2.10.0,mercurial:2.8,scm-api:2.6.3,structs:1.20
 > authentication-tokens depends on credentials:1.22
 > docker-commons depends on authentication-tokens:1.3,credentials-binding:1.18,credentials:2.1.18,pipeline-model-extensions:1.3.8;resolution:=optional
 > blueocean-config depends on blueocean-commons:1.23.0,blueocean-jwt:1.23.0,blueocean-rest:1.23.0,blueocean-web:1.23.0
 > blueocean-rest depends on blueocean-commons:1.23.0
 > git-server depends on git-client:2.7.6
Skipping optional dependency pipeline-model-extensions
 > cloudbees-folder depends on credentials:2.2.0;resolution:=optional
 > blueocean-commons depends on jackson2-api:2.10.2
Skipping optional dependency credentials
 > favorite depends on mailer:1.18,matrix-project:1.7.1,token-macro:2.0
 > htmlpublisher depends on workflow-step-api:2.20,matrix-project:1.0
 > gradle depends on structs:1.20,workflow-api:2.37,workflow-cps:2.74,workflow-job:2.35,workflow-basic-steps:2.16.1,workflow-durable-task-step:2.28,workflow-step-api:2.20
 > dashboard-view depends on junit:1.21,matrix-project:1.14
 > branch-api depends on cloudbees-folder:6.11,scm-api:2.4.1,structs:1.18
 > matrix-auth depends on configuration-as-code:1.12;resolution:=optional,cloudbees-folder:6.1.0;resolution:=optional
 > pipeline-stage-step depends on workflow-api:2.15,workflow-step-api:2.11,scm-api:2.0.7,structs:1.6
 > bootstrap4-api depends on font-awesome-api:5.13.0-1,jquery3-api:3.4.1-10,popper-api:1.16.0-6
 > blueocean-github-pipeline depends on blueocean-pipeline-api-impl:1.23.0,apache-httpcomponents-client-4-api:4.5.10-1.0,github-api:1.111,github-branch-source:2.6.0,jackson2-api:2.10.2,pubsub-light:1.13
 > github-branch-source depends on github:1.29.3,github-api:1.110,jackson2-api:2.10.2
Skipping optional dependency configuration-as-code
 > plain-credentials depends on credentials:2.2.0
Skipping optional dependency cloudbees-folder
 > mailer depends on display-url-api:2.3.1
 > blueocean-jwt depends on blueocean-commons:1.23.0,mailer:1.20
 > kubernetes-credentials depends on apache-httpcomponents-client-4-api:4.5.5-3.0,authentication-tokens:1.3,credentials:2.2.0,docker-commons:1.14,google-oauth-plugin:0.8,jackson2-api:2.9.9,kubernetes-client-api:1.0.0,plain-credentials:1.5
 > ssh-slaves depends on credentials:2.3.0,ssh-credentials:1.18,trilead-api:1.0.5
 > jsch depends on ssh-credentials:1.14,trilead-api:1.0.5
 > ldap depends on mailer:1.20
 > blueocean-pipeline-scm-api depends on blueocean-rest:1.23.0,workflow-multibranch:2.20,branch-api:2.0.20,credentials:2.2.1,pubsub-light:1.13
 > gitlab-branch-source depends on gitlab-api:1.0.6,apache-httpcomponents-client-4-api:4.5.10-2.0,credentials:2.3.0,git:3.12.1,handy-uri-templates-2-api:2.1.6-1.0,plain-credentials:1.5,scm-api:2.6.3
 > mercurial depends on credentials:2.3.5,jsch:0.1.54.1,matrix-project:1.14,multiple-scms:0.4-beta-1;resolution:=optional,scm-api:2.6.3,ssh-credentials:1.17.3
 > pipeline-model-declarative-agent depends on pipeline-model-extensions:1.1.1
 > rebuild depends on matrix-project:1.12
 > pipeline-stage-tags-metadata depends on workflow-api:2.36
 > ssh-credentials depends on credentials:2.2.0,trilead-api:1.0.5
 > blueocean-rest-impl depends on blueocean-jwt:1.23.0,blueocean-rest:1.23.0,blueocean-web:1.23.0,workflow-cps:2.76,cloudbees-folder:6.9,credentials:2.2.1,display-url-api:2.3.0,junit:1.23,mailer:1.20,favorite:2.3.1
 > pipeline-input-step depends on workflow-api:2.35,workflow-step-api:2.20,workflow-support:3.3,credentials:2.3.0
 > workflow-durable-task-step depends on workflow-api:2.33,workflow-step-api:2.20,workflow-support:3.3,durable-task:1.33,scm-api:2.2.6,script-security:1.58,structs:1.18
 > pipeline-github-lib depends on workflow-cps-global-lib:2.5,git:3.0.2
 > workflow-api depends on workflow-step-api:2.16,scm-api:2.2.6,structs:1.17
 > docker-workflow depends on workflow-basic-steps:2.18,workflow-cps:2.76,workflow-durable-task-step:2.31,workflow-step-api:2.22,cloudbees-folder:6.9,docker-commons:1.14,pipeline-model-extensions:1.5.1
 > lockable-resources depends on workflow-support:2.12,mailer:1.13,matrix-project:1.4,script-security:1.33
 > pipeline-rest-api depends on workflow-api:2.24,workflow-job:2.12,workflow-step-api:2.14,workflow-support:2.14,jackson2-api:2.9.9.1,pipeline-graph-analysis:1.4,pipeline-input-step:2.8,pipeline-stage-step:2.3
 > pipeline-build-step depends on workflow-api:2.33,workflow-step-api:2.22,workflow-support:3.1,script-security:1.50
 > pipeline-model-extensions depends on workflow-cps:2.76,workflow-job:2.33,credentials-binding:1.13,credentials:2.1.16,pipeline-stage-step:2.3,pipeline-model-api:1.6.0
 > blueocean-git-pipeline depends on blueocean-pipeline-api-impl:1.23.0,blueocean-pipeline-scm-api:1.23.0,git:3.9.3
 > ansible depends on workflow-step-api:1.10;resolution:=optional,credentials:1.16.1,job-dsl:1.36;resolution:=optional,plain-credentials:1.4,ssh-credentials:1.10
Skipping optional dependency workflow-step-api
 > git-parameter depends on rebuild:1.25;resolution:=optional,git:3.10.0,jquery:1.11.2-1,junit:1.3
 > kubernetes depends on workflow-api:2.40,workflow-cps:2.80;resolution:=optional,workflow-step-api:2.22,authentication-tokens:1.3,cloudbees-folder:6.12,credentials:2.3.5,durable-task:1.34,jackson2-api:2.10.2,kubernetes-client-api:4.8.0-1,plain-credentials:1.6,structs:1.20,variant:1.3,kubernetes-credentials:0.6.2,pipeline-model-extensions:1.6.0;resolution:=optional
 > sse-gateway depends on pubsub-light:1.13
 > forensics-api depends on bootstrap4-api:4.4.1-10,data-tables-api:1.10.20-13,echarts-api:4.6.0-7,font-awesome-api:5.12.0-7,jquery3-api:3.4.1-10,plugin-util-api:1.0.1,workflow-api:2.37,workflow-cps:2.76,workflow-job:2.36
 > blueocean-personalization depends on blueocean-dashboard:1.23.0
 > junit depends on workflow-api:2.34,workflow-step-api:2.19,script-security:1.56,structs:1.17
 > token-macro depends on workflow-step-api:2.14,structs:1.14
 > blueocean-core-js depends on jenkins-design-language:1.23.0
Skipping optional dependency rebuild
 > workflow-cps depends on workflow-api:2.36,workflow-scm-step:2.4,workflow-step-api:2.21,workflow-support:3.3,scm-api:2.2.6,script-security:1.63,structs:1.20,support-core:2.43;resolution:=optional,ace-editor:1.0.1,jquery-detached:1.2.1
 > generic-webhook-trigger depends on credentials:2.1.16,plain-credentials:1.2,structs:1.7
Skipping optional dependency multiple-scms
Skipping optional dependency job-dsl
 > git depends on configuration-as-code:1.35;resolution:=optional,workflow-scm-step:2.9,workflow-step-api:2.20,credentials:2.3.0,git-client:3.0.0,mailer:1.23,matrix-project:1.14;resolution:=optional,parameterized-trigger:2.33;resolution:=optional,promoted-builds:3.2;resolution:=optional,scm-api:2.6.3,script-security:1.66,ssh-credentials:1.17.3,structs:1.20,token-macro:2.10;resolution:=optional
 > timestamper depends on workflow-api:2.39,workflow-step-api:2.19
 > blueocean-bitbucket-pipeline depends on blueocean-pipeline-api-impl:1.23.0,apache-httpcomponents-client-4-api:4.5.10-1.0,cloudbees-bitbucket-branch-source:2.4.4,pubsub-light:1.13
 > blueocean-executor-info depends on blueocean-pipeline-api-impl:1.23.0,blueocean-rest:1.23.0,blueocean-web:1.23.0,workflow-scm-step:2.9,workflow-step-api:2.22,script-security:1.63,structs:1.20
 > blueocean-web depends on blueocean-core-js:1.23.0,blueocean-rest:1.23.0,jenkins-design-language:1.23.0,variant:1.1
Skipping optional dependency workflow-cps
 > github-api depends on jackson2-api:2.10.2
Skipping optional dependency configuration-as-code
 > docker-slaves depends on workflow-support:2.2;resolution:=optional,docker-commons:1.3.1,durable-task:1.12;resolution:=optional,workflow-step-api:2.3;resolution:=optional
 > pipeline-model-definition depends on workflow-api:2.36,workflow-basic-steps:2.18,workflow-cps-global-lib:2.9,workflow-cps:2.76,workflow-durable-task-step:2.31,workflow-multibranch:2.16,workflow-scm-step:2.9,workflow-support:3.3,credentials-binding:1.13,credentials:2.1.16,docker-workflow:1.22,git-client:2.7.0,mailer:1.20,pipeline-input-step:2.8,pipeline-stage-step:2.3,scm-api:2.4.1,pipeline-model-api:1.6.0,pipeline-model-declarative-agent:1.1.1,pipeline-model-extensions:1.6.0,pipeline-stage-tags-metadata:1.6.0
 > pipeline-maven depends on h2-api:1.4.199,mysql-api:8.0.16;resolution:=optional,postgresql-api:42.2.5;resolution:=optional,maven-plugin:3.1.2;resolution:=optional,workflow-api:2.34,workflow-job:2.29,workflow-step-api:2.16,branch-api:2.0.20.1,cloudbees-folder:6.6,config-file-provider:3.5,credentials:2.1.19,htmlpublisher:1.16;resolution:=optional,jacoco:3.0.3;resolution:=optional,jgiven:0.15.1;resolution:=optional,junit-attachments:1.4.2;resolution:=optional,junit:1.26.1;resolution:=optional,matrix-project:1.14;resolution:=optional,maven-invoker-plugin:2.4;resolution:=optional,pipeline-build-step:2.7;resolution:=optional,script-security:1.54.1,structs:1.17,findbugs:4.70;resolution:=optional,tasks:4.51;resolution:=optional
Skipping optional dependency workflow-support
 > pipeline-milestone-step depends on workflow-api:2.11,workflow-step-api:2.9
 > gitlab-oauth depends on git:3.6.4,mailer:1.20
Skipping optional dependency mysql-api
 > image-tag-parameter depends on credentials:2.3.0
Skipping optional dependency durable-task
Skipping optional dependency workflow-step-api
Skipping optional dependency postgresql-api
 > blueocean-jira depends on blueocean-rest:1.23.0,apache-httpcomponents-client-4-api:4.5.10-1.0,jackson2-api:2.10.2,jira:3.0.14
Skipping optional dependency maven-plugin
 > github depends on credentials:2.1.13,display-url-api:2.0,git:3.4.0,github-api:1.90,plain-credentials:1.1,scm-api:2.2.0,structs:1.17,token-macro:1.12.1
 > list-git-branches-parameter depends on rebuild:1.25;resolution:=optional,git:3.9.3,jquery:1.11.2-1
Skipping optional dependency rebuild
Skipping optional dependency matrix-project
Skipping optional dependency parameterized-trigger
Skipping optional dependency promoted-builds
 > ssh-agent depends on workflow-step-api:2.16,bouncycastle-api:2.16.3,credentials:2.1.17,ssh-credentials:1.14
 > google-oauth-plugin depends on credentials:2.2.0,oauth-credentials:0.3
Skipping optional dependency support-core
Skipping optional dependency token-macro
Skipping optional dependency pipeline-model-extensions
Skipping optional dependency htmlpublisher
Skipping optional dependency jacoco
Skipping optional dependency jgiven
Skipping optional dependency junit-attachments
 > view-job-filters depends on workflow-job:2.36;resolution:=optional,cvs:2.15;resolution:=optional,email-ext:2.69;resolution:=optional,git:4.0.0;resolution:=optional,m2-extra-steps:1.1.8;resolution:=optional,mailer:1.29;resolution:=optional,matrix-auth:2.5;resolution:=optional,matrix-project:1.14;resolution:=optional,subversion:2.13.1;resolution:=optional
Skipping optional dependency workflow-job
Skipping optional dependency junit
Skipping optional dependency cvs
Skipping optional dependency matrix-project
Skipping optional dependency email-ext
Skipping optional dependency git
Skipping optional dependency maven-invoker-plugin
Skipping optional dependency m2-extra-steps
Skipping optional dependency pipeline-build-step
Skipping optional dependency mailer
Skipping optional dependency matrix-auth
Skipping optional dependency matrix-project
Skipping optional dependency subversion
Skipping optional dependency findbugs
Skipping optional dependency tasks
 > docker-java-api depends on jackson2-api:2.6.4
 > jira depends on workflow-job:2.0,workflow-step-api:2.0;resolution:=optional,apache-httpcomponents-client-4-api:4.5.10-1.0,branch-api:2.0.0,credentials:2.2.1,jackson2-api:2.10.2,mailer:1.20,matrix-project:1.6,p4:1.3.8;resolution:=optional,script-security:1.16,structs:1.7
Skipping optional dependency workflow-step-api
 > violation-comments-to-gitlab depends on credentials:2.1.4,plain-credentials:1.2,structs:1.10
Skipping optional dependency p4
 > git-changelog depends on workflow-step-api:2.14,antisamy-markup-formatter:1.5,credentials:2.1.4,git:2.3.5,plain-credentials:1.2,script-security:1.46,token-macro:1.10
 > gitlab-plugin depends on matrix-project:1.10,plain-credentials:1.1;resolution:=optional,git-client:1.19.0,workflow-step-api:2.16,workflow-job:1.15,credentials:2.1.0,git:2.4.1,display-url-api:1.1.1
Skipping optional dependency plain-credentials
 > kubernetes-client-api depends on bouncycastle-api:2.17,jackson2-api:2.10.3
 > git-client depends on configuration-as-code:1.35;resolution:=optional,apache-httpcomponents-client-4-api:4.5.10-2.0,credentials:2.3.0,jsch:0.1.55.1,script-security:1.66,ssh-credentials:1.17.3,structs:1.20
 > blueocean-autofavorite depends on workflow-job:2.32,branch-api:2.0.11,git-client:2.5.0,git:3.6.0,favorite:2.3.1
 > credentials-binding depends on workflow-step-api:2.22,credentials:2.3.5,plain-credentials:1.6,ssh-credentials:1.17.3,structs:1.20
Skipping optional dependency configuration-as-code
 > blueocean-pipeline-api-impl depends on blueocean-pipeline-scm-api:1.23.0,blueocean-rest-impl:1.23.0,workflow-api:2.36,workflow-cps:2.76,workflow-durable-task-step:2.31,workflow-job:2.33,workflow-multibranch:2.20,workflow-step-api:2.22,workflow-support:3.3,branch-api:2.0.20,credentials:2.2.1,git:3.9.3,github-branch-source:2.6.0,htmlpublisher:1.14,pipeline-build-step:2.7,pipeline-graph-analysis:1.10,pipeline-input-step:2.8,pipeline-stage-step:2.3,plain-credentials:1.4,scm-api:2.6.3,pipeline-model-definition:1.6.0,pipeline-stage-tags-metadata:1.6.0
 > build-timeout depends on naginator:1.16;resolution:=optional,token-macro:1.5.1
Skipping optional dependency naginator
 > command-launcher depends on script-security:1.68
 > config-file-provider depends on cloudbees-folder:5.12;resolution:=optional,credentials:2.1.4,script-security:1.56,ssh-credentials:1.12,structs:1.14,token-macro:2.0
Skipping optional dependency cloudbees-folder
 > docker-swarm depends on docker-commons:1.9,docker-java-api:3.0.14,durable-task:1.16
 > docker-plugin depends on workflow-api:2.23.1;resolution:=optional,workflow-cps:2.41;resolution:=optional,workflow-durable-task-step:2.17;resolution:=optional,workflow-job:2.12.2;resolution:=optional,workflow-step-api:2.14;resolution:=optional,workflow-support:2.16;resolution:=optional,apache-httpcomponents-client-4-api:4.5.3-2.0,bouncycastle-api:2.16.2,docker-commons:1.9,docker-java-api:3.1.5,durable-task:1.16,script-security:1.36;resolution:=optional,ssh-slaves:1.22,token-macro:2.3
Skipping optional dependency workflow-api
Skipping optional dependency workflow-cps
Skipping optional dependency workflow-durable-task-step
Skipping optional dependency workflow-job
Skipping optional dependency workflow-step-api
Skipping optional dependency workflow-support
 > blueocean-pipeline-editor depends on blueocean-commons:1.23.0,blueocean-dashboard:1.23.0,blueocean-pipeline-api-impl:1.23.0,blueocean-rest:1.23.0,docker-workflow:1.23,pipeline-model-definition:1.6.0
Skipping optional dependency script-security
 > ansible-tower depends on workflow-step-api:2.20;resolution:=optional,credentials:2.3.0,envinject:2.1.3;resolution:=optional,plain-credentials:1.4;resolution:=optional,script-security:1.67
Skipping optional dependency workflow-step-api
 > workflow-aggregator depends on lockable-resources:2.3,pipeline-stage-view:2.10,workflow-api:2.29,workflow-basic-steps:2.11,workflow-cps-global-lib:2.11,workflow-cps:2.56,workflow-durable-task-step:2.22,workflow-job:2.25,workflow-multibranch:2.20,workflow-scm-step:2.6,workflow-step-api:2.16,workflow-support:2.20,cloudbees-folder:6.6,credentials:2.1.18,git-client:2.7.3,jackson2-api:2.8.11.3,pipeline-build-step:2.7,pipeline-input-step:2.8,pipeline-milestone-step:1.3.1,pipeline-stage-step:2.3,scm-api:2.2.8,structs:1.15,pipeline-model-definition:1.3.2
Skipping optional dependency envinject
 > credentials depends on configuration-as-code:1.35;resolution:=optional,structs:1.20
Skipping optional dependency plain-credentials
Skipping optional dependency configuration-as-code
 > workflow-job depends on workflow-api:2.36,workflow-step-api:2.20,workflow-support:3.3
 > pipeline-stage-view depends on pipeline-rest-api:2.13,workflow-api:2.24,workflow-job:2.12,handlebars:1.1.1,jquery-detached:1.2.1,momentjs:1.1
 > oauth-credentials depends on credentials:2.3.0
 > font-awesome-api depends on plugin-util-api:1.0.2
 > workflow-multibranch depends on workflow-api:2.27,workflow-cps:2.53,workflow-job:2.21,workflow-scm-step:2.4,workflow-step-api:2.13,workflow-support:2.17,branch-api:2.0.21,cloudbees-folder:6.1.2,scm-api:2.2.7,script-security:1.42,structs:1.17
 > matrix-project depends on junit:1.20,script-security:1.54
 > workflow-basic-steps depends on workflow-api:2.40,workflow-durable-task-step:2.31,workflow-step-api:2.22,apache-httpcomponents-client-4-api:4.5.10-2.0,mailer:1.32,structs:1.20
 > pipeline-graph-analysis depends on workflow-api:2.34,workflow-cps:2.65,workflow-job:2.32,workflow-step-api:2.19,workflow-support:3.2,pipeline-input-step:2.8,pipeline-stage-step:2.3,scm-api:2.2.7,structs:1.17
 > scm-api depends on structs:1.9
 > workflow-step-api depends on structs:1.20
 > workflow-scm-step depends on workflow-step-api:2.9
 > data-tables-api depends on bootstrap4-api:4.4.1-10,font-awesome-api:5.12.0-7,jquery3-api:3.4.1-10,plugin-util-api:1.0.2
 > email-ext depends on workflow-job:2.12;resolution:=optional,workflow-step-api:2.19;resolution:=optional,config-file-provider:2.15.6;resolution:=optional,junit:1.2,mailer:1.13,matrix-project:1.4,script-security:1.54,structs:1.17,token-macro:2.3,analysis-core:1.54;resolution:=optional
Skipping optional dependency workflow-job
Skipping optional dependency workflow-step-api
Skipping optional dependency config-file-provider
 > blueocean-dashboard depends on blueocean-web:1.23.0
 > docker-build-step depends on docker-commons:1.15,matrix-project:1.14,credentials:2.1.18
 > workflow-cps-global-lib depends on workflow-api:2.33,workflow-cps:2.71,workflow-scm-step:2.7,workflow-step-api:2.20,workflow-support:3.3,cloudbees-folder:6.1.2,credentials:2.1.18,git-server:1.7,scm-api:2.6.3,script-security:1.70,structs:1.19
 > pipeline-model-api depends on workflow-step-api:2.22,jackson2-api:2.9.8,structs:1.20
 > maven-plugin depends on apache-httpcomponents-client-4-api:4.5.5-2.1,javadoc:1.0,jsch:0.1.54.2,junit:1.6,mailer:1.7,token-macro:2.0;resolution:=optional
 > blueocean-events depends on blueocean-pipeline-api-impl:1.23.0,pubsub-light:1.13,sse-gateway:1.21
Skipping optional dependency analysis-core
Skipping optional dependency token-macro
 > coverity depends on credentials:1.18
 > echarts-api depends on jquery3-api:3.4.1-10,plugin-util-api:1.2.0
 > subversion depends on workflow-scm-step:2.6,credentials:2.1.16,mapdb-api:1.0.9.0,scm-api:2.6.3,ssh-credentials:1.12,structs:1.9
 > ws-cleanup depends on workflow-durable-task-step:2.4,matrix-project:1.7.1,resource-disposer:0.3,script-security:1.54,structs:1.19
 > analysis-model-api depends on plugin-util-api:1.0.2
 > warnings-ng depends on analysis-model-api:8.0.1,bootstrap4-api:4.4.1-10,data-tables-api:1.10.20-14,echarts-api:4.6.0-8,font-awesome-api:5.12.0-7,forensics-api:0.7.0,jquery3-api:3.4.1-10,plugin-util-api:1.0.2,workflow-api:2.37,workflow-cps:2.76,workflow-job:2.36,workflow-step-api:2.20,antisamy-markup-formatter:1.5,apache-httpcomponents-client-4-api:4.5.10-2.0,credentials:2.3.0,dashboard-view:2.9.4;resolution:=optional,matrix-project:1.14;resolution:=optional,structs:1.20,token-macro:2.10;resolution:=optional
Skipping optional dependency dashboard-view
Skipping optional dependency matrix-project
Skipping optional dependency token-macro
 > synopsys-coverity depends on credentials:2.1.10,plain-credentials:1.0,job-dsl:1.67;resolution:=optional,workflow-job:2.9;resolution:=optional,workflow-cps:2.23;resolution:=optional,workflow-step-api:2.10;resolution:=optional
Skipping optional dependency job-dsl
Skipping optional dependency workflow-job
Skipping optional dependency workflow-cps
Skipping optional dependency workflow-step-api
 > ant depends on structs:1.17
 > workflow-support depends on workflow-api:2.36,workflow-step-api:2.20,scm-api:2.2.6,script-security:1.39

WAR bundled plugins:


Installed plugins:
ace-editor:1.1
analysis-model-api:8.0.1
ansible-tower:0.14.1
ansible:1.0
ant:1.11
antisamy-markup-formatter:2.0
apache-httpcomponents-client-4-api:4.5.10-2.0
authentication-tokens:1.3
blueocean-autofavorite:1.2.4
blueocean-bitbucket-pipeline:1.23.0
blueocean-commons:1.23.0
blueocean-config:1.23.0
blueocean-core-js:1.23.0
blueocean-dashboard:1.23.0
blueocean-display-url:2.3.1
blueocean-events:1.23.0
blueocean-executor-info:1.23.0
blueocean-git-pipeline:1.23.0
blueocean-github-pipeline:1.23.0
blueocean-i18n:1.23.0
blueocean-jira:1.23.0
blueocean-jwt:1.23.0
blueocean-personalization:1.23.0
blueocean-pipeline-api-impl:1.23.0
blueocean-pipeline-editor:1.23.0
blueocean-pipeline-scm-api:1.23.0
blueocean-rest-impl:1.23.0
blueocean-rest:1.23.0
blueocean-web:1.23.0
blueocean:1.23.0
bootstrap4-api:4.4.1-12
bouncycastle-api:2.18
branch-api:2.5.6
build-environment:1.7
build-timeout:1.19.1
cloudbees-bitbucket-branch-source:2.7.0
cloudbees-folder:6.12
command-launcher:1.4
config-file-provider:3.6.3
coverity:1.11.4
credentials-binding:1.22
credentials:2.3.7
dashboard-view:2.12
data-tables-api:1.10.20-14
display-url-api:2.3.2
docker-build-step:2.4
docker-commons:1.16
docker-compose-build-step:1.0
docker-java-api:3.1.5.2
docker-plugin:1.2.0
docker-slaves:1.0.7
docker-swarm:1.9
docker-workflow:1.23
durable-task:1.34
echarts-api:4.6.0-9
email-ext:2.69
favorite:2.3.2
font-awesome-api:5.13.0-1
forensics-api:0.7.0
generic-webhook-trigger:1.67
git-changelog:2.23
git-client:3.2.1
git-parameter:0.9.12
git-server:1.9
git:4.2.2
github-api:1.111
github-branch-source:2.7.1
github:1.29.5
gitlab-api:1.0.6
gitlab-branch-source:1.4.6
gitlab-oauth:1.5
gitlab-plugin:1.5.13
google-oauth-plugin:1.0.0
gradle:1.36
h2-api:1.4.199
handlebars:1.1.1
handy-uri-templates-2-api:2.1.8-1.0
htmlpublisher:1.22
image-tag-parameter:1.2
jackson2-api:2.10.3
javadoc:1.5
jdk-tool:1.4
jenkins-design-language:1.23.0
jira:3.0.15
jquery-detached:1.2.1
jquery:1.12.4-1
jquery3-api:3.4.1-10
jsch:0.1.55.2
junit:1.28
kubernetes-client-api:4.9.1-1
kubernetes-credentials:0.6.2
kubernetes:1.25.3
ldap:1.24
list-git-branches-parameter:0.0.8
lockable-resources:2.7
mailer:1.32
mapdb-api:1.0.9.0
matrix-auth:2.5
matrix-project:1.14
maven-plugin:3.6
mercurial:2.10
momentjs:1.1.1
next-executions:1.0.15
oauth-credentials:0.4
pam-auth:1.6
pipeline-build-step:2.12
pipeline-github-lib:1.0
pipeline-graph-analysis:1.10
pipeline-input-step:2.11
pipeline-maven:3.8.2
pipeline-milestone-step:1.3.1
pipeline-model-api:1.6.0
pipeline-model-declarative-agent:1.1.1
pipeline-model-definition:1.6.0
pipeline-model-extensions:1.6.0
pipeline-rest-api:2.13
pipeline-stage-step:2.3
pipeline-stage-tags-metadata:1.6.0
pipeline-stage-view:2.13
plain-credentials:1.7
plugin-util-api:1.2.0
popper-api:1.16.0-6
pubsub-light:1.13
rebuild:1.31
resource-disposer:0.14
scm-api:2.6.3
script-security:1.71
sse-gateway:1.23
ssh-agent:1.19
ssh-credentials:1.18.1
ssh-slaves:1.31.2
structs:1.20
subversion:2.13.1
synopsys-coverity:2.3.1
timestamper:1.11.3
token-macro:2.12
trilead-api:1.0.6
variant:1.3
view-job-filters:2.2
violation-comments-to-gitlab:2.39
warnings-ng:8.1.0
workflow-aggregator:2.6
workflow-api:2.40
workflow-basic-steps:2.20
workflow-cps-global-lib:2.16
workflow-cps:2.80
workflow-durable-task-step:2.35
workflow-job:2.38
workflow-multibranch:2.21
workflow-scm-step:2.11
workflow-step-api:2.22
workflow-support:3.4
ws-cleanup:0.38
Cleaning up locks
Removing intermediate container c7349777c633
 ---> a778170a18b3
Step 22/32: RUN dnf -y remove git
 ---> Running in 1b96ff7ea903
Dependencies resolved.
================================================================================
 Package               Arch        Version                Repository       Size
================================================================================
Removing:
 git                   x86_64      2.18.2-2.el8_1         @AppStream      395 k
Removing unused dependencies:
 git-core-doc          noarch      2.18.2-2.el8_1         @AppStream       11 M
 libsecret             x86_64      0.18.6-1.el8           @BaseOS         508 k
 perl-Error            noarch      1:0.17025-2.el8        @AppStream       70 k
 perl-Git              noarch      2.18.2-2.el8_1         @AppStream       63 k
 perl-TermReadKey      x86_64      2.37-7.el8             @AppStream       65 k

Transaction Summary
================================================================================
Remove  6 Packages

Freed space: 12 M
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Erasing          : git-2.18.2-2.el8_1.x86_64                              1/6
  Erasing          : perl-Git-2.18.2-2.el8_1.noarch                         2/6
  Erasing          : perl-Error-1:0.17025-2.el8.noarch                      3/6
  Erasing          : git-core-doc-2.18.2-2.el8_1.noarch                     4/6
  Erasing          : libsecret-0.18.6-1.el8.x86_64                          5/6
  Erasing          : perl-TermReadKey-2.37-7.el8.x86_64                     6/6
  Running scriptlet: perl-TermReadKey-2.37-7.el8.x86_64                     6/6
  Verifying        : git-2.18.2-2.el8_1.x86_64                              1/6
  Verifying        : git-core-doc-2.18.2-2.el8_1.noarch                     2/6
  Verifying        : libsecret-0.18.6-1.el8.x86_64                          3/6
  Verifying        : perl-Error-1:0.17025-2.el8.noarch                      4/6
  Verifying        : perl-Git-2.18.2-2.el8_1.noarch                         5/6
  Verifying        : perl-TermReadKey-2.37-7.el8.x86_64                     6/6

Removed:
  git-2.18.2-2.el8_1.x86_64            git-core-doc-2.18.2-2.el8_1.noarch
  libsecret-0.18.6-1.el8.x86_64        perl-Error-1:0.17025-2.el8.noarch
  perl-Git-2.18.2-2.el8_1.noarch       perl-TermReadKey-2.37-7.el8.x86_64

Complete!
Removing intermediate container 1b96ff7ea903
 ---> bf8c4260541c
Step 23/32 : RUN dnf -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-ExtUtils-MakeMaker
 ---> Running in 98ec37e75f0c
Last metadata expiration check: 0:01:18 ago on 2020年04月26日 14時19分46秒.
Package gettext-devel-0.19.8.1-17.el8.x86_64 is already installed.
Dependencies resolved.
================================================================================
 Package                       Arch     Version               Repository   Size
================================================================================
Installing:
 perl-ExtUtils-MakeMaker       noarch   1:7.34-1.el8          AppStream   300 k
 expat-devel                   x86_64   2.2.5-3.el8           BaseOS       55 k
 libcurl-devel                 x86_64   7.61.1-11.el8         BaseOS      831 k
 openssl-devel                 x86_64   1:1.1.1c-2.el8_1.1    BaseOS      2.3 M
 zlib-devel                    x86_64   1.2.11-10.el8         BaseOS       56 k
Installing dependencies:
 perl-CPAN-Meta-Requirements   noarch   2.140-396.el8         AppStream    37 k
 perl-CPAN-Meta-YAML           noarch   0.018-397.el8         AppStream    34 k
 perl-ExtUtils-Command         noarch   1:7.34-1.el8          AppStream    19 k
 perl-ExtUtils-Install         noarch   2.14-4.el8            AppStream    46 k
 perl-ExtUtils-Manifest        noarch   1.70-395.el8          AppStream    37 k
 perl-ExtUtils-ParseXS         noarch   1:3.35-2.el8          AppStream    83 k
 perl-JSON-PP                  noarch   1:2.97.001-3.el8      AppStream    68 k
 perl-Test-Harness             noarch   1:3.42-1.el8          AppStream   279 k
 perl-devel                    x86_64   4:5.26.3-416.el8      AppStream   599 k
 perl-version                  x86_64   6:0.99.24-1.el8       AppStream    67 k
 systemtap-sdt-devel           x86_64   4.1-6.el8             AppStream    80 k
 keyutils-libs-devel           x86_64   1.5.10-6.el8          BaseOS       48 k
 krb5-devel                    x86_64   1.17-9.el8            BaseOS      548 k
 libcom_err-devel              x86_64   1.44.6-3.el8          BaseOS       38 k
 libkadm5                      x86_64   1.17-9.el8            BaseOS      184 k
 libselinux-devel              x86_64   2.9-2.1.el8           BaseOS      199 k
 libsepol-devel                x86_64   2.9-1.el8             BaseOS       86 k
 libverto-devel                x86_64   0.3.0-5.el8           BaseOS       18 k
 pcre2-devel                   x86_64   10.32-1.el8           BaseOS      605 k
 pcre2-utf16                   x86_64   10.32-1.el8           BaseOS      228 k
 pcre2-utf32                   x86_64   10.32-1.el8           BaseOS      220 k
 perl-Math-BigInt              noarch   1:1.9998.11-7.el8     BaseOS      196 k
 perl-Math-Complex             noarch   1.59-416.el8          BaseOS      108 k
 python3-pyparsing             noarch   2.1.10-7.el8          BaseOS      142 k
Installing weak dependencies:
 perl-CPAN-Meta                noarch   2.150010-396.el8      AppStream   191 k
 perl-Encode-Locale            noarch   1.05-9.el8            AppStream    21 k
 perl-Time-HiRes               x86_64   1.9758-1.el8          AppStream    61 k

Transaction Summary
================================================================================
Install  32 Packages

Total download size: 7.6 M
Installed size: 17 M
Downloading Packages:
(1/32): perl-CPAN-Meta-Requirements-2.140-396.e 596 kB/s |  37 kB     00:00
(2/32): perl-Encode-Locale-1.05-9.el8.noarch.rp 1.5 MB/s |  21 kB     00:00
(3/32): perl-ExtUtils-Command-7.34-1.el8.noarch 1.3 MB/s |  19 kB     00:00
(4/32): perl-CPAN-Meta-2.150010-396.el8.noarch. 955 kB/s | 191 kB     00:00
(5/32): perl-ExtUtils-Install-2.14-4.el8.noarch 202 kB/s |  46 kB     00:00
(6/32): perl-CPAN-Meta-YAML-0.018-397.el8.noarc  94 kB/s |  34 kB     00:00
(7/32): perl-ExtUtils-MakeMaker-7.34-1.el8.noar 1.8 MB/s | 300 kB     00:00
(8/32): perl-ExtUtils-Manifest-1.70-395.el8.noa 365 kB/s |  37 kB     00:00
(9/32): perl-JSON-PP-2.97.001-3.el8.noarch.rpm  755 kB/s |  68 kB     00:00
(10/32): perl-ExtUtils-ParseXS-3.35-2.el8.noarc 550 kB/s |  83 kB     00:00
(11/32): perl-Time-HiRes-1.9758-1.el8.x86_64.rp 941 kB/s |  61 kB     00:00
(12/32): perl-version-0.99.24-1.el8.x86_64.rpm  1.3 MB/s |  67 kB     00:00
(13/32): systemtap-sdt-devel-4.1-6.el8.x86_64.r 1.5 MB/s |  80 kB     00:00
(14/32): expat-devel-2.2.5-3.el8.x86_64.rpm     1.4 MB/s |  55 kB     00:00
(15/32): perl-Test-Harness-3.42-1.el8.noarch.rp 982 kB/s | 279 kB     00:00
(16/32): perl-devel-5.26.3-416.el8.x86_64.rpm   1.8 MB/s | 599 kB     00:00
(17/32): libcom_err-devel-1.44.6-3.el8.x86_64.r 1.4 MB/s |  38 kB     00:00
(18/32): keyutils-libs-devel-1.5.10-6.el8.x86_6 196 kB/s |  48 kB     00:00
(19/32): krb5-devel-1.17-9.el8.x86_64.rpm       2.2 MB/s | 548 kB     00:00
(20/32): libselinux-devel-2.9-2.1.el8.x86_64.rp 2.2 MB/s | 199 kB     00:00
(21/32): libsepol-devel-2.9-1.el8.x86_64.rpm    1.6 MB/s |  86 kB     00:00
(22/32): libverto-devel-0.3.0-5.el8.x86_64.rpm  1.3 MB/s |  18 kB     00:00
(23/32): libcurl-devel-7.61.1-11.el8.x86_64.rpm 3.1 MB/s | 831 kB     00:00
(24/32): libkadm5-1.17-9.el8.x86_64.rpm         734 kB/s | 184 kB     00:00
(25/32): pcre2-utf16-10.32-1.el8.x86_64.rpm     1.6 MB/s | 228 kB     00:00
(26/32): pcre2-devel-10.32-1.el8.x86_64.rpm     2.6 MB/s | 605 kB     00:00
(27/32): pcre2-utf32-10.32-1.el8.x86_64.rpm     2.1 MB/s | 220 kB     00:00
(28/32): perl-Math-BigInt-1.9998.11-7.el8.noarc 2.1 MB/s | 196 kB     00:00
(29/32): perl-Math-Complex-1.59-416.el8.noarch. 1.6 MB/s | 108 kB     00:00
(30/32): zlib-devel-1.2.11-10.el8.x86_64.rpm    1.4 MB/s |  56 kB     00:00
(31/32): python3-pyparsing-2.1.10-7.el8.noarch. 1.8 MB/s | 142 kB     00:00
(32/32): openssl-devel-1.1.1c-2.el8_1.1.x86_64. 2.7 MB/s | 2.3 MB     00:00
--------------------------------------------------------------------------------
Total                                           2.1 MB/s | 7.6 MB     00:03
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                        1/1
  Installing       : perl-version-6:0.99.24-1.el8.x86_64                   1/32
  Installing       : perl-CPAN-Meta-Requirements-2.140-396.el8.noarch      2/32
  Installing       : perl-Time-HiRes-1.9758-1.el8.x86_64                   3/32
  Installing       : perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch             4/32
  Installing       : perl-Test-Harness-1:3.42-1.el8.noarch                 5/32
  Installing       : zlib-devel-1.2.11-10.el8.x86_64                       6/32
  Installing       : python3-pyparsing-2.1.10-7.el8.noarch                 7/32
  Installing       : systemtap-sdt-devel-4.1-6.el8.x86_64                  8/32
  Installing       : perl-Math-Complex-1.59-416.el8.noarch                 9/32
  Installing       : perl-Math-BigInt-1:1.9998.11-7.el8.noarch            10/32
  Installing       : perl-JSON-PP-1:2.97.001-3.el8.noarch                 11/32
  Installing       : pcre2-utf32-10.32-1.el8.x86_64                       12/32
  Installing       : pcre2-utf16-10.32-1.el8.x86_64                       13/32
  Installing       : pcre2-devel-10.32-1.el8.x86_64                       14/32
  Installing       : libverto-devel-0.3.0-5.el8.x86_64                    15/32
  Installing       : libsepol-devel-2.9-1.el8.x86_64                      16/32
  Installing       : libselinux-devel-2.9-2.1.el8.x86_64                  17/32
  Installing       : libkadm5-1.17-9.el8.x86_64                           18/32
  Installing       : libcom_err-devel-1.44.6-3.el8.x86_64                 19/32
  Installing       : keyutils-libs-devel-1.5.10-6.el8.x86_64              20/32
  Installing       : krb5-devel-1.17-9.el8.x86_64                         21/32
  Installing       : perl-ExtUtils-Manifest-1.70-395.el8.noarch           22/32
  Installing       : perl-ExtUtils-Command-1:7.34-1.el8.noarch            23/32
  Installing       : perl-Encode-Locale-1.05-9.el8.noarch                 24/32
  Installing       : perl-CPAN-Meta-YAML-0.018-397.el8.noarch             25/32
  Installing       : perl-CPAN-Meta-2.150010-396.el8.noarch               26/32
  Installing       : perl-devel-4:5.26.3-416.el8.x86_64                   27/32
  Installing       : perl-ExtUtils-Install-2.14-4.el8.noarch              28/32
  Installing       : perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch          29/32
  Installing       : openssl-devel-1:1.1.1c-2.el8_1.1.x86_64              30/32
  Installing       : libcurl-devel-7.61.1-11.el8.x86_64                   31/32
  Installing       : expat-devel-2.2.5-3.el8.x86_64                       32/32
  Running scriptlet: expat-devel-2.2.5-3.el8.x86_64                       32/32
  Verifying        : perl-CPAN-Meta-2.150010-396.el8.noarch                1/32
  Verifying        : perl-CPAN-Meta-Requirements-2.140-396.el8.noarch      2/32
  Verifying        : perl-CPAN-Meta-YAML-0.018-397.el8.noarch              3/32
  Verifying        : perl-Encode-Locale-1.05-9.el8.noarch                  4/32
  Verifying        : perl-ExtUtils-Command-1:7.34-1.el8.noarch             5/32
  Verifying        : perl-ExtUtils-Install-2.14-4.el8.noarch               6/32
  Verifying        : perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch           7/32
  Verifying        : perl-ExtUtils-Manifest-1.70-395.el8.noarch            8/32
  Verifying        : perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch             9/32
  Verifying        : perl-JSON-PP-1:2.97.001-3.el8.noarch                 10/32
  Verifying        : perl-Test-Harness-1:3.42-1.el8.noarch                11/32
  Verifying        : perl-Time-HiRes-1.9758-1.el8.x86_64                  12/32
  Verifying        : perl-devel-4:5.26.3-416.el8.x86_64                   13/32
  Verifying        : perl-version-6:0.99.24-1.el8.x86_64                  14/32
  Verifying        : systemtap-sdt-devel-4.1-6.el8.x86_64                 15/32
  Verifying        : expat-devel-2.2.5-3.el8.x86_64                       16/32
  Verifying        : keyutils-libs-devel-1.5.10-6.el8.x86_64              17/32
  Verifying        : krb5-devel-1.17-9.el8.x86_64                         18/32
  Verifying        : libcom_err-devel-1.44.6-3.el8.x86_64                 19/32
  Verifying        : libcurl-devel-7.61.1-11.el8.x86_64                   20/32
  Verifying        : libkadm5-1.17-9.el8.x86_64                           21/32
  Verifying        : libselinux-devel-2.9-2.1.el8.x86_64                  22/32
  Verifying        : libsepol-devel-2.9-1.el8.x86_64                      23/32
  Verifying        : libverto-devel-0.3.0-5.el8.x86_64                    24/32
  Verifying        : openssl-devel-1:1.1.1c-2.el8_1.1.x86_64              25/32
  Verifying        : pcre2-devel-10.32-1.el8.x86_64                       26/32
  Verifying        : pcre2-utf16-10.32-1.el8.x86_64                       27/32
  Verifying        : pcre2-utf32-10.32-1.el8.x86_64                       28/32
  Verifying        : perl-Math-BigInt-1:1.9998.11-7.el8.noarch            29/32
  Verifying        : perl-Math-Complex-1.59-416.el8.noarch                30/32
  Verifying        : python3-pyparsing-2.1.10-7.el8.noarch                31/32
  Verifying        : zlib-devel-1.2.11-10.el8.x86_64                      32/32

Installed:
  perl-ExtUtils-MakeMaker-1:7.34-1.el8.noarch
  expat-devel-2.2.5-3.el8.x86_64
  libcurl-devel-7.61.1-11.el8.x86_64
  openssl-devel-1:1.1.1c-2.el8_1.1.x86_64
  zlib-devel-1.2.11-10.el8.x86_64
  perl-CPAN-Meta-2.150010-396.el8.noarch
  perl-Encode-Locale-1.05-9.el8.noarch
  perl-Time-HiRes-1.9758-1.el8.x86_64
  perl-CPAN-Meta-Requirements-2.140-396.el8.noarch
  perl-CPAN-Meta-YAML-0.018-397.el8.noarch
  perl-ExtUtils-Command-1:7.34-1.el8.noarch
  perl-ExtUtils-Install-2.14-4.el8.noarch
  perl-ExtUtils-Manifest-1.70-395.el8.noarch
  perl-ExtUtils-ParseXS-1:3.35-2.el8.noarch
  perl-JSON-PP-1:2.97.001-3.el8.noarch
  perl-Test-Harness-1:3.42-1.el8.noarch
  perl-devel-4:5.26.3-416.el8.x86_64
  perl-version-6:0.99.24-1.el8.x86_64 
  systemtap-sdt-devel-4.1-6.el8.x86_
  keyutils-libs-devel-1.5.10-6.el8.x86_64
  krb5-devel-1.17-9.el8.x86_64      
  libcom_err-devel-1.44.6-3.el8.x86_64
  libkadm5-1.17-9.el8.x86_64        
  libselinux-devel-2.9-2.1.el8.x86_64 
  libsepol-devel-2.9-1.el8.x86_64   
  libverto-devel-0.3.0-5.el8.x86_64 
  pcre2-devel-10.32-1.el8.x86_64    
  pcre2-utf16-10.32-1.el8.x86_64    
  pcre2-utf32-10.32-1.el8.x86_64    
  perl-Math-BigInt-1:1.9998.11-7.el8.noarch      
  perl-Math-Complex-1.59-416.el8.noarch          
  python3-pyparsing-2.1.10-7.el8.noarch          

Complete!
Removing intermediate container 98ec37e75f0c
 ---> 515596fa51cf
Step 24/32 : ADD /asset/git-2.26.2.tar.gz /usr/src/
 ---> 30c1b2bf1dbe
Step 25/32 : WORKDIR /usr/src/git-2.26.2
 ---> Running in b44f3f5b8ea1
Removing intermediate container b44f3f5b8ea1
 ---> 7497a73be0b1
Step 26/32 : RUN make configure
 ---> Running in 41918a14d5d0
GIT_VERSION = 2.26.2
    GEN configure
Removing intermediate container 41918a14d5d0
 ---> 5370516fb05a
Step 27/32 : RUN ./configure --prefix=/usr/local
 ---> Running in 23db1dd67c18
configure: Setting lib to 'lib' (the default)
configure: Will try -pthread then -lpthread to enable POSIX Threads.
configure: CHECKS for site configuration
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables...
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether gcc accepts -g... yes
checking for gcc option to accept ISO C89... none needed
checking how to run the C preprocessor... gcc -E
checking for grep that handles long lines and -e... /usr/bin/grep
checking for egrep... /usr/bin/grep -E
checking for ANSI C header files... yes
checking for sys/types.h... yes
checking for sys/stat.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for memory.h... yes
checking for strings.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for unistd.h... yes
checking for size_t... yes
checking for working alloca.h... yes
checking for alloca... yes
configure: CHECKS for programs
checking whether we are using the GNU C compiler... (cached) yes
checking whether gcc accepts -g... (cached) yes
checking for gcc option to accept ISO C89... (cached) none needed
checking for inline... inline
checking if linker supports -R... no
checking if linker supports -Wl,-rpath,... yes
checking for gar... no
checking for ar... ar
checking for gtar... gtar
checking for gnudiff... no
checking for gdiff... no
checking for diff... diff
checking for asciidoc... asciidoc
checking for asciidoc version... asciidoc 8.6.10
Using 'grep -a' for sane_grep
configure: CHECKS for libraries
checking for SHA1_Init in -lcrypto... yes
checking for curl_global_init in -lcurl... yes
checking for curl-config... curl-config
configure: Setting CURL_LDFLAGS to '-lcurl'
checking for XML_ParserCreate in -lexpat... yes
checking for iconv in -lc... yes
checking for deflateBound in -lz... yes
checking for socket in -lc... yes
checking for inet_ntop... yes
checking for inet_pton... yes
checking for hstrerror... yes
checking for basename in -lc... yes
checking if libc contains libintl... yes
checking libintl.h usability... yes
checking libintl.h presence... yes
checking for libintl.h... yes
configure: CHECKS for header files
checking sys/select.h usability... yes
checking sys/select.h presence... yes
checking for sys/select.h... yes
checking poll.h usability... yes
checking poll.h presence... yes
checking for poll.h... yes
checking sys/poll.h usability... yes
checking sys/poll.h presence... yes
checking for sys/poll.h... yes
checking for inttypes.h... (cached) yes
checking for old iconv()... no
checking whether iconv omits bom for utf-16 and utf-32... no
configure: CHECKS for typedefs, structures, and compiler characteristics
checking for socklen_t... yes
checking for struct itimerval... yes
checking for struct stat.st_mtimespec.tv_nsec... no
checking for struct stat.st_mtim.tv_nsec... yes
checking for struct dirent.d_type... yes
checking for struct passwd.pw_gecos... yes
checking for struct sockaddr_storage... yes
checking for struct addrinfo... yes
checking for getaddrinfo... yes
checking for library containing getaddrinfo... none required
checking whether the platform regex supports REG_STARTEND... yes
checking whether system succeeds to read fopen'ed directory... yes
checking whether snprintf() and/or vsnprintf() return bogus value... no
checking whether the platform uses typical file type bits... yes
configure: CHECKS for library functions
checking libgen.h usability... yes
checking libgen.h presence... yes
checking for libgen.h... yes
checking paths.h usability... yes
checking paths.h presence... yes
checking for paths.h... yes
checking libcharset.h usability... no
checking libcharset.h presence... no
checking for libcharset.h... no
checking for strings.h... (cached) yes
checking for locale_charset in -liconv... no
checking for locale_charset in -lcharset... no
checking for clock_gettime... yes
checking for library containing clock_gettime... none required
checking for CLOCK_MONOTONIC... yes
checking for setitimer... yes
checking for library containing setitimer... none required
checking for strcasestr... yes
checking for library containing strcasestr... none required
checking for memmem... yes
checking for library containing memmem... none required
checking for strlcpy... no
checking for uintmax_t... yes
checking for strtoumax... yes
checking for library containing strtoumax... none required
checking for setenv... yes
checking for library containing setenv... none required
checking for unsetenv... yes
checking for library containing unsetenv... none required
checking for mkdtemp... yes
checking for library containing mkdtemp... none required
checking for initgroups... yes
checking for library containing initgroups... none required
checking for getdelim... yes
checking for library containing getdelim... none required
checking for BSD sysctl... no
checking for POSIX Threads with ''... no
checking for POSIX Threads with '-mt'... no
checking for POSIX Threads with '-pthread'... yes
configure: creating ./config.status
config.status: creating config.mak.autogen
config.status: executing config.mak.autogen commands
Removing intermediate container 23db1dd67c18
 ---> c93a5701b1e9
Step 28/32 : RUN make all
 ---> Running in 9dd068247bc4
    * new build flags
    CC fuzz-commit-graph.o
    CC fuzz-pack-headers.o
    CC fuzz-pack-idx.o
    CC credential-store.o
    * new link flags
    CC common-main.o
    CC abspath.o
    CC add-interactive.o
    CC add-patch.o
    CC advice.o
    CC alias.o
    CC alloc.o
    CC apply.o
    CC archive.o
    CC archive-tar.o
    CC archive-zip.o
    CC argv-array.o
    * new prefix flags
    CC attr.o
    CC base85.o
    CC bisect.o
    CC blame.o
    CC blob.o
    CC branch.o
    CC bulk-checkin.o
    CC bundle.o
    CC cache-tree.o
    CC chdir-notify.o
    CC checkout.o
    CC color.o
    CC column.o
    CC combine-diff.o
    CC commit.o
    CC commit-graph.o
    CC commit-reach.o
    CC compat/obstack.o
    CC compat/terminal.o
    CC config.o
    CC connect.o
    CC connected.o
    CC convert.o
    CC copy.o
    CC credential.o
    CC csum-file.o
    CC ctype.o
    CC date.o
    CC decorate.o
    CC delta-islands.o
    CC diffcore-break.o
    CC diffcore-delta.o
    CC diffcore-order.o
    CC diffcore-pickaxe.o
    CC diffcore-rename.o
    CC diff-delta.o
    CC diff-lib.o
    CC diff-no-index.o
    CC diff.o
    CC dir.o
    CC dir-iterator.o
    CC editor.o
    CC entry.o
    CC environment.o
    CC ewah/bitmap.o
    CC ewah/ewah_bitmap.o
    CC ewah/ewah_io.o
    CC ewah/ewah_rlw.o
    CC exec-cmd.o
    CC fetch-negotiator.o
    CC fetch-pack.o
    CC fsck.o
    CC fsmonitor.o
    CC gettext.o
    CC gpg-interface.o
    CC graph.o
    CC grep.o
    CC hashmap.o
    CC linear-assignment.o
    GEN command-list.h
    CC help.o
    CC hex.o
    CC ident.o
    CC interdiff.o
    CC json-writer.o
    CC kwset.o
    CC levenshtein.o
    CC line-log.o
    CC line-range.o
    CC list-objects.o
    CC list-objects-filter.o
    CC list-objects-filter-options.o
    CC ll-merge.o
    CC lockfile.o
    CC log-tree.o
    CC ls-refs.o
    CC mailinfo.o
    CC mailmap.o
    CC match-trees.o
    CC mem-pool.o
    CC merge.o
    CC merge-blobs.o
    CC merge-recursive.o
    CC mergesort.o
    CC midx.o
    CC name-hash.o
    CC negotiator/default.o
    CC negotiator/skipping.o
    CC notes.o
    CC notes-cache.o
    CC notes-merge.o
    CC notes-utils.o
    CC object.o
    CC oidmap.o
    CC oidset.o
    CC packfile.o
    CC pack-bitmap.o
    CC pack-bitmap-write.o
    CC pack-check.o
    CC pack-objects.o
    CC pack-revindex.o
    CC pack-write.o
    CC pager.o
    CC parse-options.o
    CC parse-options-cb.o
    CC patch-delta.o
    CC patch-ids.o
    CC path.o
    CC pathspec.o
    CC pkt-line.o
    CC preload-index.o
    CC pretty.o
    CC prio-queue.o
    CC progress.o
    CC promisor-remote.o
    CC prompt.o
    CC protocol.o
    CC quote.o
    CC range-diff.o
    CC reachable.o
    CC read-cache.o
    CC rebase.o
    CC rebase-interactive.o
    CC reflog-walk.o
    CC refs.o
    CC refs/files-backend.o
    CC refs/iterator.o
    CC refs/packed-backend.o
    CC refs/ref-cache.o
    CC refspec.o
    CC ref-filter.o
    CC remote.o
    CC replace-object.o
    CC repo-settings.o
    CC repository.o
    CC rerere.o
    CC resolve-undo.o
    CC revision.o
    CC run-command.o
    CC send-pack.o
    CC sequencer.o
    CC serve.o
    CC server-info.o
    CC setup.o
    CC sha1-array.o
    CC sha1-lookup.o
    CC sha1-file.o
    CC sha1-name.o
    CC shallow.o
    CC sideband.o
    CC sigchain.o
    CC split-index.o
    CC stable-qsort.o
    CC strbuf.o
    CC streaming.o
    CC string-list.o
    CC submodule.o
    CC submodule-config.o
    CC sub-process.o
    CC symlinks.o
    CC tag.o
    CC tempfile.o
    CC thread-utils.o
    CC tmp-objdir.o
    CC trace.o
    CC trace2.o
    CC trace2/tr2_cfg.o
    CC trace2/tr2_cmd_name.o
    CC trace2/tr2_dst.o
    CC trace2/tr2_sid.o
    CC trace2/tr2_sysenv.o
    CC trace2/tr2_tbuf.o
    CC trace2/tr2_tgt_event.o
    CC trace2/tr2_tgt_normal.o
    CC trace2/tr2_tgt_perf.o
    CC trace2/tr2_tls.o
    CC trailer.o
    CC transport.o
    CC transport-helper.o
    CC tree-diff.o
    CC tree.o
    CC tree-walk.o
    CC unpack-trees.o
    CC upload-pack.o
    CC url.o
    CC urlmatch.o
    CC usage.o
    CC userdiff.o
    CC utf8.o
    CC varint.o
    CC version.o
    CC versioncmp.o
    CC walker.o
    CC wildmatch.o
    CC worktree.o
    CC wrapper.o
    CC write-or-die.o
    CC ws.o
    CC wt-status.o
    CC xdiff-interface.o
    CC zlib.o
    CC unix-socket.o
    CC sha1dc_git.o
    CC sha1dc/sha1.o
    CC sha1dc/ubc_check.o
    CC sha256/block/sha256.o
    CC compat/fopen.o
    CC compat/strlcpy.o
    CC compat/qsort_s.o
    AR libgit.a
    CC xdiff/xdiffi.o
    CC xdiff/xprepare.o
    CC xdiff/xutils.o
    CC xdiff/xemit.o
    CC xdiff/xmerge.o
    CC xdiff/xpatience.o
    CC xdiff/xhistogram.o
    AR xdiff/lib.a
    LINK git-credential-store
    CC daemon.o
    LINK git-daemon
    CC fast-import.o
    LINK git-fast-import
    CC http-backend.o
    LINK git-http-backend
    CC imap-send.o
    CC http.o
    LINK git-imap-send
    CC sh-i18n--envsubst.o
    LINK git-sh-i18n--envsubst
    CC shell.o
    LINK git-shell
    CC remote-testsvn.o
    CC vcs-svn/line_buffer.o
    CC vcs-svn/sliding_window.o
    CC vcs-svn/fast_export.o
    CC vcs-svn/svndiff.o
    CC vcs-svn/svndump.o
    AR vcs-svn/lib.a
    LINK git-remote-testsvn
    CC http-walker.o
    CC http-fetch.o
    LINK git-http-fetch
    CC http-push.o
    LINK git-http-push
    CC credential-cache.o
    LINK git-credential-cache
    CC credential-cache--daemon.o
    LINK git-credential-cache--daemon
    CC remote-curl.o
    LINK git-remote-http
    LN/CP git-remote-https
    LN/CP git-remote-ftp
    LN/CP git-remote-ftps
    * new script parameters
    GEN git-bisect
    GEN git-difftool--helper
    GEN git-filter-branch
    GEN git-merge-octopus
    GEN git-merge-one-file
    GEN git-merge-resolve
    GEN git-mergetool
    GEN git-quiltimport
    GEN git-legacy-stash
    GEN git-request-pull
    GEN git-submodule
    GEN git-web--browse
    * new perl-specific parameters
    GEN GIT-PERL-HEADER
    GEN git-add--interactive
    GEN git-archimport
    GEN git-cvsexportcommit
    GEN git-cvsimport
    GEN git-cvsserver
    GEN git-send-email
    GEN git-svn
    * new Python interpreter location
    GEN git-p4
    GEN git-instaweb
    GEN git-mergetool--lib
    GEN git-parse-remote
    GEN git-rebase--preserve-merges
    GEN git-sh-setup
    GEN git-sh-i18n
    CC git.o
    CC builtin/add.o
    CC builtin/am.o
    CC builtin/annotate.o
    CC builtin/apply.o
    CC builtin/archive.o
    CC builtin/bisect--helper.o
    CC builtin/blame.o
    CC builtin/branch.o
    CC builtin/bundle.o
    CC builtin/cat-file.o
    CC builtin/check-attr.o
    CC builtin/check-ignore.o
    CC builtin/check-mailmap.o
    CC builtin/check-ref-format.o
    CC builtin/checkout-index.o
    CC builtin/checkout.o
    CC builtin/clean.o
    CC builtin/clone.o
    CC builtin/column.o
    CC builtin/commit-tree.o
    CC builtin/commit.o
    CC builtin/commit-graph.o
    CC builtin/config.o
    CC builtin/count-objects.o
    CC builtin/credential.o
    CC builtin/describe.o
    CC builtin/diff-files.o
    CC builtin/diff-index.o
    CC builtin/diff-tree.o
    CC builtin/diff.o
    CC builtin/difftool.o
    CC builtin/env--helper.o
    CC builtin/fast-export.o
    CC builtin/fetch-pack.o
    CC builtin/fetch.o
    CC builtin/fmt-merge-msg.o
    CC builtin/for-each-ref.o
    CC builtin/fsck.o
    CC builtin/gc.o
    CC builtin/get-tar-commit-id.o
    CC builtin/grep.o
    CC builtin/hash-object.o
    CC builtin/help.o
    CC builtin/index-pack.o
    CC builtin/init-db.o
    CC builtin/interpret-trailers.o
    CC builtin/log.o
    CC builtin/ls-files.o
    CC builtin/ls-remote.o
    CC builtin/ls-tree.o
    CC builtin/mailinfo.o
    CC builtin/mailsplit.o
    CC builtin/merge.o
    CC builtin/merge-base.o
    CC builtin/merge-file.o
    CC builtin/merge-index.o
    CC builtin/merge-ours.o
    CC builtin/merge-recursive.o
    CC builtin/merge-tree.o
    CC builtin/mktag.o
    CC builtin/mktree.o
    CC builtin/multi-pack-index.o
    CC builtin/mv.o
    CC builtin/name-rev.o
    CC builtin/notes.o
    CC builtin/pack-objects.o
    CC builtin/pack-redundant.o
    CC builtin/pack-refs.o
    CC builtin/patch-id.o
    CC builtin/prune-packed.o
    CC builtin/prune.o
    CC builtin/pull.o
    CC builtin/push.o
    CC builtin/range-diff.o
    CC builtin/read-tree.o
    CC builtin/rebase.o
    CC builtin/receive-pack.o
    CC builtin/reflog.o
    CC builtin/remote.o
    CC builtin/remote-ext.o
    CC builtin/remote-fd.o
    CC builtin/repack.o
    CC builtin/replace.o
    CC builtin/rerere.o
    CC builtin/reset.o
    CC builtin/rev-list.o
    CC builtin/rev-parse.o
    CC builtin/revert.o
    CC builtin/rm.o
    CC builtin/send-pack.o
    CC builtin/shortlog.o
    CC builtin/show-branch.o
    CC builtin/show-index.o
    CC builtin/show-ref.o
    CC builtin/sparse-checkout.o
    CC builtin/stash.o
    CC builtin/stripspace.o
    CC builtin/submodule--helper.o
    CC builtin/symbolic-ref.o
    CC builtin/tag.o
    CC builtin/unpack-file.o
    CC builtin/unpack-objects.o
    CC builtin/update-index.o
    CC builtin/update-ref.o
    CC builtin/update-server-info.o
    CC builtin/upload-archive.o
    CC builtin/upload-pack.o
    CC builtin/var.o
    CC builtin/verify-commit.o
    CC builtin/verify-pack.o
    CC builtin/verify-tag.o
    CC builtin/worktree.o
    CC builtin/write-tree.o
    LINK git
    BUILTIN git-add
    BUILTIN git-am
    BUILTIN git-annotate
    BUILTIN git-apply
    BUILTIN git-archive
    BUILTIN git-bisect--helper
    BUILTIN git-blame
    BUILTIN git-branch
    BUILTIN git-bundle
    BUILTIN git-cat-file
    BUILTIN git-check-attr
    BUILTIN git-check-ignore
    BUILTIN git-check-mailmap
    BUILTIN git-check-ref-format
    BUILTIN git-checkout-index
    BUILTIN git-checkout
    BUILTIN git-clean
    BUILTIN git-clone
    BUILTIN git-column
    BUILTIN git-commit-tree
    BUILTIN git-commit
    BUILTIN git-commit-graph
    BUILTIN git-config
    BUILTIN git-count-objects
    BUILTIN git-credential
    BUILTIN git-describe
    BUILTIN git-diff-files
    BUILTIN git-diff-index
    BUILTIN git-diff-tree
    BUILTIN git-diff
    BUILTIN git-difftool
    BUILTIN git-env--helper
    BUILTIN git-fast-export
    BUILTIN git-fetch-pack
    BUILTIN git-fetch
    BUILTIN git-fmt-merge-msg
    BUILTIN git-for-each-ref
    BUILTIN git-fsck
    BUILTIN git-gc
    BUILTIN git-get-tar-commit-id
    BUILTIN git-grep
    BUILTIN git-hash-object
    BUILTIN git-help
    BUILTIN git-index-pack
    BUILTIN git-init-db
    BUILTIN git-interpret-trailers
    BUILTIN git-log
    BUILTIN git-ls-files
    BUILTIN git-ls-remote
    BUILTIN git-ls-tree
    BUILTIN git-mailinfo
    BUILTIN git-mailsplit
    BUILTIN git-merge
    BUILTIN git-merge-base
    BUILTIN git-merge-file
    BUILTIN git-merge-index
    BUILTIN git-merge-ours
    BUILTIN git-merge-recursive
    BUILTIN git-merge-tree
    BUILTIN git-mktag
    BUILTIN git-mktree
    BUILTIN git-multi-pack-index
    BUILTIN git-mv
    BUILTIN git-name-rev
    BUILTIN git-notes
    BUILTIN git-pack-objects
    BUILTIN git-pack-redundant
    BUILTIN git-pack-refs
    BUILTIN git-patch-id
    BUILTIN git-prune-packed
    BUILTIN git-prune
    BUILTIN git-pull
    BUILTIN git-push
    BUILTIN git-range-diff
    BUILTIN git-read-tree
    BUILTIN git-rebase
    BUILTIN git-receive-pack
    BUILTIN git-reflog
    BUILTIN git-remote
    BUILTIN git-remote-ext
    BUILTIN git-remote-fd
    BUILTIN git-repack
    BUILTIN git-replace
    BUILTIN git-rerere
    BUILTIN git-reset
    BUILTIN git-rev-list
    BUILTIN git-rev-parse
    BUILTIN git-revert
    BUILTIN git-rm
    BUILTIN git-send-pack
    BUILTIN git-shortlog
    BUILTIN git-show-branch
    BUILTIN git-show-index
    BUILTIN git-show-ref
    BUILTIN git-sparse-checkout
    BUILTIN git-stash
    BUILTIN git-stripspace
    BUILTIN git-submodule--helper
    BUILTIN git-symbolic-ref
    BUILTIN git-tag
    BUILTIN git-unpack-file
    BUILTIN git-unpack-objects
    BUILTIN git-update-index
    BUILTIN git-update-ref
    BUILTIN git-update-server-info
    BUILTIN git-upload-archive
    BUILTIN git-upload-pack
    BUILTIN git-var
    BUILTIN git-verify-commit
    BUILTIN git-verify-pack
    BUILTIN git-verify-tag
    BUILTIN git-worktree
    BUILTIN git-write-tree
    BUILTIN git-cherry
    BUILTIN git-cherry-pick
    BUILTIN git-format-patch
    BUILTIN git-fsck-objects
    BUILTIN git-init
    BUILTIN git-merge-subtree
    BUILTIN git-restore
    BUILTIN git-show
    BUILTIN git-stage
    BUILTIN git-status
    BUILTIN git-switch
    BUILTIN git-whatchanged
    SUBDIR git-gui
GITGUI_VERSION = 0.21.GITGUI
    * new locations or Tcl/Tk interpreter
    GEN git-gui
    INDEX lib/
    * tclsh failed; using unoptimized loading
    MSGFMT    po/de.msg 579 translated.
    MSGFMT po/pt_pt.msg 550 translated.
    MSGFMT    po/vi.msg 543 translated.
    MSGFMT    po/hu.msg 514 translated.
    MSGFMT    po/nb.msg 474 translated, 39 untranslated.
    MSGFMT    po/sv.msg 547 translated.
    MSGFMT    po/fr.msg 520 translated.
    MSGFMT    po/it.msg 519 translated, 1 untranslated.
    MSGFMT    po/ja.msg 546 translated, 1 untranslated.
    MSGFMT po/pt_br.msg 520 translated.
    MSGFMT    po/bg.msg 565 translated.
    MSGFMT    po/ru.msg 520 translated.
    MSGFMT po/zh_cn.msg 366 translated, 7 fuzzy, 17 untranslated.
    MSGFMT    po/el.msg 381 translated, 4 fuzzy, 6 untranslated.
    SUBDIR gitk-git
    * new Tcl/Tk interpreter location
    GEN gitk-wish
Generating catalog po/de.msg
msgfmt --statistics --tcl po/de.po -l de -d po/
307 translated messages.
Generating catalog po/pt_pt.msg
msgfmt --statistics --tcl po/pt_pt.po -l pt_pt -d po/
311 translated messages.
Generating catalog po/vi.msg
msgfmt --statistics --tcl po/vi.po -l vi -d po/
307 translated messages.
Generating catalog po/ca.msg
msgfmt --statistics --tcl po/ca.po -l ca -d po/
307 translated messages.
Generating catalog po/hu.msg
msgfmt --statistics --tcl po/hu.po -l hu -d po/
277 translated messages, 18 fuzzy translations, 12 untranslated messages.
Generating catalog po/es.msg
msgfmt --statistics --tcl po/es.po -l es -d po/
184 translated messages, 46 fuzzy translations, 77 untranslated messages.
Generating catalog po/sv.msg
msgfmt --statistics --tcl po/sv.po -l sv -d po/
311 translated messages.
Generating catalog po/fr.msg
msgfmt --statistics --tcl po/fr.po -l fr -d po/
311 translated messages.
Generating catalog po/it.msg
msgfmt --statistics --tcl po/it.po -l it -d po/
274 translated messages, 17 fuzzy translations, 16 untranslated messages.
Generating catalog po/ja.msg
msgfmt --statistics --tcl po/ja.po -l ja -d po/
311 translated messages.
Generating catalog po/pt_br.msg
msgfmt --statistics --tcl po/pt_br.po -l pt_br -d po/
279 translated messages, 16 fuzzy translations, 12 untranslated messages.
Generating catalog po/bg.msg
msgfmt --statistics --tcl po/bg.po -l bg -d po/
317 translated messages.
Generating catalog po/ru.msg
msgfmt --statistics --tcl po/ru.po -l ru -d po/
317 translated messages.
Generating catalog po/zh_cn.msg
msgfmt --statistics --tcl po/zh_cn.po -l zh_cn -d po/
317 translated messages.
    SUBDIR templates
    MSGFMT po/build/locale/pt_PT/LC_MESSAGES/git.mo
3198 translated messages.
    MSGFMT po/build/locale/de/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/tr/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/vi/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/is/LC_MESSAGES/git.mo
14 translated messages.
    MSGFMT po/build/locale/ca/LC_MESSAGES/git.mo
4024 translated messages, 392 fuzzy translations, 384 untranslated messages.
    MSGFMT po/build/locale/ko/LC_MESSAGES/git.mo
3608 translated messages.
    MSGFMT po/build/locale/es/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/sv/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/fr/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/it/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/bg/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/ru/LC_MESSAGES/git.mo
3521 translated messages, 1060 untranslated messages.
    MSGFMT po/build/locale/zh_CN/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/zh_TW/LC_MESSAGES/git.mo
4839 translated messages.
    MSGFMT po/build/locale/el/LC_MESSAGES/git.mo
1038 translated messages, 3325 untranslated messages.
    GEN perl/build/lib/Git.pm
    GEN perl/build/lib/Git/IndexInfo.pm
    GEN perl/build/lib/Git/Packet.pm
    GEN perl/build/lib/Git/SVN.pm
    GEN perl/build/lib/Git/I18N.pm
    GEN perl/build/lib/Git/LoadCPAN.pm
    GEN perl/build/lib/Git/LoadCPAN/Error.pm
    GEN perl/build/lib/Git/SVN/GlobSpec.pm
    GEN perl/build/lib/Git/SVN/Fetcher.pm
    GEN perl/build/lib/Git/SVN/Utils.pm
    GEN perl/build/lib/Git/SVN/Prompt.pm
    GEN perl/build/lib/Git/SVN/Ra.pm
    GEN perl/build/lib/Git/SVN/Editor.pm
    GEN perl/build/lib/Git/SVN/Migration.pm
    GEN perl/build/lib/Git/SVN/Log.pm
    GEN perl/build/lib/Git/LoadCPAN/Mail/Address.pm
    GEN perl/build/lib/Git/SVN/Memoize/YAML.pm
    GEN perl/build/lib/FromCPAN/Error.pm
    GEN perl/build/lib/FromCPAN/Mail/Address.pm
    CC t/helper/test-fake-ssh.o
    LINK t/helper/test-fake-ssh
    CC t/helper/test-line-buffer.o
    LINK t/helper/test-line-buffer
    CC t/helper/test-svn-fe.o
    LINK t/helper/test-svn-fe
    CC t/helper/test-tool.o
    CC t/helper/test-chmtime.o
    CC t/helper/test-config.o
    CC t/helper/test-ctype.o
    CC t/helper/test-date.o
    CC t/helper/test-delta.o
    CC t/helper/test-dir-iterator.o
    CC t/helper/test-drop-caches.o
    CC t/helper/test-dump-cache-tree.o
    CC t/helper/test-dump-fsmonitor.o
    CC t/helper/test-dump-split-index.o
    CC t/helper/test-dump-untracked-cache.o
    CC t/helper/test-example-decorate.o
    CC t/helper/test-genrandom.o
    CC t/helper/test-genzeros.o
    CC t/helper/test-hash.o
    CC t/helper/test-hashmap.o
    CC t/helper/test-hash-speed.o
    CC t/helper/test-index-version.o
    CC t/helper/test-json-writer.o
    CC t/helper/test-lazy-init-name-hash.o
    CC t/helper/test-match-trees.o
    CC t/helper/test-mergesort.o
    CC t/helper/test-mktemp.o
    CC t/helper/test-oidmap.o
    CC t/helper/test-online-cpus.o
    CC t/helper/test-parse-options.o
    CC t/helper/test-parse-pathspec-file.o
    CC t/helper/test-path-utils.o
    CC t/helper/test-pkt-line.o
    CC t/helper/test-prio-queue.o
    CC t/helper/test-progress.o
    CC t/helper/test-reach.o
    CC t/helper/test-read-cache.o
    CC t/helper/test-read-graph.o
    CC t/helper/test-read-midx.o
    CC t/helper/test-ref-store.o
    CC t/helper/test-regex.o
    CC t/helper/test-repository.o
    CC t/helper/test-revision-walking.o
    CC t/helper/test-run-command.o
    CC t/helper/test-scrap-cache-tree.o
    CC t/helper/test-serve-v2.o
    CC t/helper/test-sha1.o
    CC t/helper/test-sha1-array.o
    CC t/helper/test-sha256.o
    CC t/helper/test-sigchain.o
    CC t/helper/test-strcmp-offset.o
    CC t/helper/test-string-list.o
    CC t/helper/test-submodule-config.o
    CC t/helper/test-submodule-nested-repo-config.o
    CC t/helper/test-subprocess.o
    CC t/helper/test-trace2.o
    CC t/helper/test-urlmatch-normalization.o
    CC t/helper/test-xml-encode.o
    CC t/helper/test-wildmatch.o
    CC t/helper/test-windows-named-pipe.o
    CC t/helper/test-write-cache.o
    LINK t/helper/test-tool
    GEN bin-wrappers/git
    GEN bin-wrappers/git-upload-pack
    GEN bin-wrappers/git-receive-pack
    GEN bin-wrappers/git-upload-archive
    GEN bin-wrappers/git-shell
    GEN bin-wrappers/git-cvsserver
    GEN bin-wrappers/test-fake-ssh
    GEN bin-wrappers/test-line-buffer
    GEN bin-wrappers/test-svn-fe
    GEN bin-wrappers/test-tool
Removing intermediate container 9dd068247bc4
 ---> b2313f465484
Step 29/32 : RUN make install
 ---> Running in c7cbeb81d818
    SUBDIR git-gui
    SUBDIR gitk-git
    SUBDIR templates
install -d -m 755 '/usr/local/bin'
install -d -m 755 '/usr/local/libexec/git-core'
install   git-credential-store git-daemon git-fast-import git-http-backend git-imap-send git-sh-i18n--envsubst git-shell git-remote-testsvn git-http-fetch git-http-push git-credential-cache git-credential-cache--daemon git-remote-http git-remote-https git-remote-ftp git-remote-ftps git-bisect git-difftool--helper git-filter-branch git-merge-octopus git-merge-one-file git-merge-resolve git-mergetool git-quiltimport git-legacy-stash git-request-pull git-submodule git-web--browse git-add--interactive git-archimport git-cvsexportcommit git-cvsimport git-cvsserver git-send-email git-svn git-p4 git-instaweb '/usr/local/libexec/git-core'
install -m 644  git-mergetool--lib git-parse-remote git-rebase--preserve-merges git-sh-setup git-sh-i18n '/usr/local/libexec/git-core'
install git git-upload-pack git-receive-pack git-upload-archive git-shell git-cvsserver '/usr/local/bin'
make -C templates DESTDIR='' install
make[1]: Entering directory '/usr/src/git-2.26.2/templates'
install -d -m 755 '/usr/local/share/git-core/templates'
(cd blt && gtar cf - .) | \
(cd '/usr/local/share/git-core/templates' && umask 022 && gtar xof -)
make[1]: Leaving directory '/usr/src/git-2.26.2/templates'
install -d -m 755 '/usr/local/libexec/git-core/mergetools'
install -m 644 mergetools/* '/usr/local/libexec/git-core/mergetools'
install -d -m 755 '/usr/local/share/locale'
(cd po/build/locale && gtar cf - .) | \
(cd '/usr/local/share/locale' && umask 022 && gtar xof -)
install -d -m 755 '/usr/local/share/perl5'
(cd perl/build/lib && gtar cf - .) | \
(cd '/usr/local/share/perl5' && umask 022 && gtar xof -)
make -C gitweb install
make[1]: Entering directory '/usr/src/git-2.26.2/gitweb'
make[2]: Entering directory '/usr/src/git-2.26.2'
make[2]: 'GIT-VERSION-FILE' is up to date.
make[2]: Leaving directory '/usr/src/git-2.26.2'
    GEN gitweb.cgi
    GEN static/gitweb.js
install -d -m 755 '/usr/local/share/gitweb'
install -m 755 gitweb.cgi '/usr/local/share/gitweb'
install -d -m 755 '/usr/local/share/gitweb/static'
install -m 644 static/gitweb.js static/gitweb.css static/git-logo.png static/git-favicon.png '/usr/local/share/gitweb/static'
make[1]: Leaving directory '/usr/src/git-2.26.2/gitweb'
make -C gitk-git install
make[1]: Entering directory '/usr/src/git-2.26.2/gitk-git'
install -d -m 755 '/usr/local/bin'
install -m 755 gitk-wish '/usr/local/bin'/gitk
install -d -m 755 '/usr/local/share/gitk/lib/msgs'
install -m 644 po/zh_cn.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/de.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/sv.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/it.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/hu.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/ja.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/pt_pt.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/fr.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/pt_br.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/ru.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/ca.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/vi.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/bg.msg '/usr/local/share/gitk/lib/msgs' &&  install -m 644 po/es.msg '/usr/local/share/gitk/lib/msgs' && true
make[1]: Leaving directory '/usr/src/git-2.26.2/gitk-git'
make -C git-gui gitexecdir='/usr/local/libexec/git-core' install
make[1]: Entering directory '/usr/src/git-2.26.2/git-gui'
  DEST /usr/local/libexec/git-core
    INSTALL 755 git-gui
    INSTALL 755 git-gui--askpass
    LINK        git-citool -> git-gui
  DEST /usr/local/share/git-gui/lib
    INSTALL 644 tclIndex
    INSTALL 644 themed.tcl
    INSTALL 644 tools.tcl
    INSTALL 644 line.tcl
    INSTALL 644 checkout_op.tcl
    INSTALL 644 transport.tcl
    INSTALL 644 console.tcl
    INSTALL 644 remote_add.tcl
    INSTALL 644 browser.tcl
    INSTALL 644 branch_checkout.tcl
    INSTALL 644 spellcheck.tcl
    INSTALL 644 status_bar.tcl
    INSTALL 644 diff.tcl
    INSTALL 644 remote.tcl
    INSTALL 644 choose_font.tcl
    INSTALL 644 option.tcl
    INSTALL 644 mergetool.tcl
    INSTALL 644 tools_dlg.tcl
    INSTALL 644 search.tcl
    INSTALL 644 shortcut.tcl
    INSTALL 644 branch_rename.tcl
    INSTALL 644 class.tcl
    INSTALL 644 remote_branch_delete.tcl
    INSTALL 644 choose_repository.tcl
    INSTALL 644 about.tcl
    INSTALL 644 blame.tcl
    INSTALL 644 branch.tcl
    INSTALL 644 win32.tcl
    INSTALL 644 sshkey.tcl
    INSTALL 644 branch_create.tcl
    INSTALL 644 chord.tcl
    INSTALL 644 commit.tcl
    INSTALL 644 branch_delete.tcl
    INSTALL 644 index.tcl
    INSTALL 644 encoding.tcl
    INSTALL 644 logo.tcl
    INSTALL 644 date.tcl
    INSTALL 644 choose_rev.tcl
    INSTALL 644 database.tcl
    INSTALL 644 error.tcl
    INSTALL 644 merge.tcl
    INSTALL 644 git-gui.ico
    INSTALL 644 win32_shortcut.js
  DEST /usr/local/share/git-gui/lib/msgs
    INSTALL 644 pt_br.msg
    INSTALL 644 hu.msg
    INSTALL 644 zh_cn.msg
    INSTALL 644 de.msg
    INSTALL 644 sv.msg
    INSTALL 644 it.msg
    INSTALL 644 ja.msg
    INSTALL 644 pt_pt.msg
    INSTALL 644 fr.msg
    INSTALL 644 ru.msg
    INSTALL 644 el.msg
    INSTALL 644 vi.msg
    INSTALL 644 bg.msg
    INSTALL 644 nb.msg
make[1]: Leaving directory '/usr/src/git-2.26.2/git-gui'
bindir=$(cd '/usr/local/bin' && pwd) && \
execdir=$(cd '/usr/local/libexec/git-core' && pwd) && \
destdir_from_execdir_SQ=$(echo 'libexec/git-core' | sed -e 's|[^/][^/]*|..|g') && \
{ test "$bindir/" = "$execdir/" || \
  for p in git git-shell git-cvsserver; do \
	rm -f "$execdir/$p" && \
	test -n "" && \
	ln -s "$destdir_from_execdir_SQ/bin/$p" "$execdir/$p" || \
	{ test -z "" && \
	  ln "$bindir/$p" "$execdir/$p" 2>/dev/null || \
	  cp "$bindir/$p" "$execdir/$p" || exit; } \
  done; \
} && \
for p in git-receive-pack git-upload-archive git-upload-pack; do \
	rm -f "$bindir/$p" && \
	test -n "" && \
	ln -s "git" "$bindir/$p" || \
	{ test -z "" && \
	  ln "$bindir/git" "$bindir/$p" 2>/dev/null || \
	  ln -s "git" "$bindir/$p" 2>/dev/null || \
	  cp "$bindir/git" "$bindir/$p" || exit; } \
done && \
for p in  git-add git-am git-annotate git-apply git-archive git-bisect--helper git-blame git-branch git-bundle git-cat-file git-check-attr git-check-ignore git-check-mailmap git-check-ref-format git-checkout-index git-checkout git-clean git-clone git-column git-commit-tree git-commit git-commit-graph git-config git-count-objects git-credential git-describe git-diff-files git-diff-index git-diff-tree git-diff git-difftool git-env--helper git-fast-export git-fetch-pack git-fetch git-fmt-merge-msg git-for-each-ref git-fsck git-gc git-get-tar-commit-id git-grep git-hash-object git-help git-index-pack git-init-db git-interpret-trailers git-log git-ls-files git-ls-remote git-ls-tree git-mailinfo git-mailsplit git-merge git-merge-base git-merge-file git-merge-index git-merge-ours git-merge-recursive git-merge-tree git-mktag git-mktree git-multi-pack-index git-mv git-name-rev git-notes git-pack-objects git-pack-redundant git-pack-refs git-patch-id git-prune-packed git-prune git-pull git-push git-range-diff git-read-tree git-rebase git-receive-pack git-reflog git-remote git-remote-ext git-remote-fd git-repack git-replace git-rerere git-reset git-rev-list git-rev-parse git-revert git-rm git-send-pack git-shortlog git-show-branch git-show-index git-show-ref git-sparse-checkout git-stash git-stripspace git-submodule--helper git-symbolic-ref git-tag git-unpack-file git-unpack-objects git-update-index git-update-ref git-update-server-info git-upload-archive git-upload-pack git-var git-verify-commit git-verify-pack git-verify-tag git-worktree git-write-tree git-cherry git-cherry-pick git-format-patch git-fsck-objects git-init git-merge-subtree git-restore git-show git-stage git-status git-switch git-whatchanged; do \
	rm -f "$execdir/$p" && \
	test -n "" && \
	ln -s "$destdir_from_execdir_SQ/bin/git" "$execdir/$p" || \
	{ test -z "" && \
	  ln "$execdir/git" "$execdir/$p" 2>/dev/null || \
	  ln -s "git" "$execdir/$p" 2>/dev/null || \
	  cp "$execdir/git" "$execdir/$p" || exit; } \
done && \
remote_curl_aliases="git-remote-https git-remote-ftp git-remote-ftps" && \
for p in $remote_curl_aliases; do \
	rm -f "$execdir/$p" && \
	test -n "" && \
	ln -s "git-remote-http" "$execdir/$p" || \
	{ test -z "" && \
	  ln "$execdir/git-remote-http" "$execdir/$p" 2>/dev/null || \
	  ln -s "git-remote-http" "$execdir/$p" 2>/dev/null || \
	  cp "$execdir/git-remote-http" "$execdir/$p" || exit; } \
done && \
./check_bindir "z$bindir" "z$execdir" "$bindir/git-add"
Removing intermediate container c7cbeb81d818
 ---> 5cbc9473fbf0
Step 30/32 : USER jenkins
 ---> Running in 1baa9d49dfd3
Removing intermediate container 1baa9d49dfd3
 ---> afb391fa8f02
Step 31/32 : RUN mkdir -m 700 $HOME/.ssh
 ---> Running in e7763dda51ae
Removing intermediate container e7763dda51ae
 ---> fd75a0b91f30
Step 32/32 : RUN ssh-keygen -t rsa -N "" -f $HOME/.ssh/id_rsa
 ---> Running in c156ffff0d4b
Generating public/private rsa key pair.
Created directory '/var/jenkins_home/.ssh'.
Your identification has been saved in /var/jenkins_home/.ssh/id_rsa.
Your public key has been saved in /var/jenkins_home/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:iMoD1Y6bkZjp+GLTiCizKJLH95FXQNkiZrDWzRnnH4U jenkins@c156ffff0d4b
The key's randomart image is:
+---[RSA 3072]----+
|    ..  oo.  ..  |
|   . o++o=. E.   |
|  . +o..=.. .    |
| = = . . . . .   |
|= + o . S . .    |
|oo =   . .       |
|++O   o .        |
|@=oo.  o         |
|B=o. ..          |
+----[SHA256]-----+
Removing intermediate container c156ffff0d4b
 ---> f1173f57b147
Successfully built f1173f57b147
Successfully tagged jenkins:1.0
```

</details>

Jenkinsイメージビルド後、  
以下の通り、REPOSITORY: `jenkins`、TAG: `1.0`が出力されていれば、成功

```bash
# docker images
REPOSITORY                 TAG                 IMAGE ID            CREATED              SIZE
jenkins                    1.0                 0dbecb2375e7        About a minute ago   1.41GB
jenkins/jenkins            centos              bf1c9963816a        2 days ago           782MB
```

## zipの展開

```bash
# unzip jenkins_data.zip
```

## Jenkinsコンテナ起動

```bash
docker-compose up -d
```

なお、起動時のログは以下のコマンドで確認出来る。

```bash
# docker logs -f jenkins
```

## Jenkinsアップデート

```bash
# docker cp jenkins.war jenkins:/usr/share/jenkins/.
# docker restart jenkins
```

参考情報として、`root`で`jenkins`コンテナにログインする場合は、  
以下のコマンドを実行する。

```bash
# docker exec -it --user root jenkins /bin/bash
```

## Jenkinsプラグイン一覧

`Jenkins管理` → `スクリプトコンソール`

以下を入力し、`実行`するとプラグイン一覧が取得出来る。

```groovy
def pluginList = new ArrayList(Jenkins.instance.pluginManager.plugins)
pluginList.sort { it.getShortName() }.each{
  plugin ->
    println ("${plugin.getShortName()}:${plugin.getVersion()}")
}
```

<details>
<summary>Jenkinsプラグイン一覧の詳細</summary>

```bash
ace-editor:1.1
analysis-model-api:8.0.1
ansible:1.0
ansible-tower:0.14.1
ant:1.11
antisamy-markup-formatter:2.0
apache-httpcomponents-client-4-api:4.5.10-2.0
authentication-tokens:1.3
badge:1.8
blueocean:1.23.0
blueocean-autofavorite:1.2.4
blueocean-bitbucket-pipeline:1.23.0
blueocean-commons:1.23.0
blueocean-config:1.23.0
blueocean-core-js:1.23.0
blueocean-dashboard:1.23.0
blueocean-display-url:2.3.1
blueocean-events:1.23.0
blueocean-executor-info:1.23.0
blueocean-github-pipeline:1.23.0
blueocean-git-pipeline:1.23.0
blueocean-i18n:1.23.0
blueocean-jira:1.23.0
blueocean-jwt:1.23.0
blueocean-personalization:1.23.0
blueocean-pipeline-api-impl:1.23.0
blueocean-pipeline-editor:1.23.0
blueocean-pipeline-scm-api:1.23.0
blueocean-rest:1.23.0
blueocean-rest-impl:1.23.0
blueocean-web:1.23.0
bootstrap4-api:4.4.1-12
bouncycastle-api:2.18
branch-api:2.5.6
build-environment:1.7
build-timeout:1.19.1
cloudbees-bitbucket-branch-source:2.7.0
cloudbees-folder:6.12
command-launcher:1.4
config-file-provider:3.6.3
coverity:1.11.4
credentials:2.3.7
credentials-binding:1.22
dashboard-view:2.12
data-tables-api:1.10.20-14
display-url-api:2.3.2
docker-build-step:2.4
docker-commons:1.16
docker-compose-build-step:1.0
docker-java-api:3.1.5.2
docker-plugin:1.2.0
docker-slaves:1.0.7
docker-swarm:1.9
docker-workflow:1.23
durable-task:1.34
echarts-api:4.6.0-9
email-ext:2.69
favorite:2.3.2
font-awesome-api:5.13.0-1
forensics-api:0.7.0
generic-webhook-trigger:1.67
git:4.2.2
git-changelog:2.23
git-client:3.2.1
github:1.29.5
github-api:1.111
github-branch-source:2.7.1
gitlab-api:1.0.6
gitlab-branch-source:1.4.6
gitlab-oauth:1.5
gitlab-plugin:1.5.13
git-parameter:0.9.12
git-server:1.9
google-oauth-plugin:1.0.0
gradle:1.36
groovy:2.2
groovy-postbuild:2.5
h2-api:1.4.199
handlebars:1.1.1
handy-uri-templates-2-api:2.1.8-1.0
htmlpublisher:1.22
image-tag-parameter:1.2
jackson2-api:2.10.3
javadoc:1.5
jdk-tool:1.4
jenkins-design-language:1.23.0
jira:3.0.15
jquery:1.12.4-1
jquery3-api:3.4.1-10
jquery-detached:1.2.1
jsch:0.1.55.2
junit:1.28
kubernetes:1.25.3
kubernetes-client-api:4.9.1-1
kubernetes-credentials:0.6.2
ldap:1.24
list-git-branches-parameter:0.0.8
lockable-resources:2.7
mailer:1.32
mapdb-api:1.0.9.0
matrix-auth:2.5
matrix-project:1.14
maven-plugin:3.6
mercurial:2.10
momentjs:1.1.1
next-executions:1.0.15
oauth-credentials:0.4
pam-auth:1.6
pipeline-build-step:2.12
pipeline-github-lib:1.0
pipeline-graph-analysis:1.10
pipeline-input-step:2.11
pipeline-maven:3.8.2
pipeline-milestone-step:1.3.1
pipeline-model-api:1.6.0
pipeline-model-declarative-agent:1.1.1
pipeline-model-definition:1.6.0
pipeline-model-extensions:1.6.0
pipeline-rest-api:2.13
pipeline-stage-step:2.3
pipeline-stage-tags-metadata:1.6.0
pipeline-stage-view:2.13
plain-credentials:1.7
plugin-util-api:1.2.0
popper-api:1.16.0-6
pubsub-light:1.13
rebuild:1.31
resource-disposer:0.14
scm-api:2.6.3
script-security:1.71
sse-gateway:1.23
ssh-agent:1.19
ssh-credentials:1.18.1
ssh-slaves:1.31.2
structs:1.20
subversion:2.13.1
synopsys-coverity:2.3.1
timestamper:1.11.3
token-macro:2.12
trilead-api:1.0.6
variant:1.3
view-job-filters:2.2
violation-comments-to-gitlab:2.39
warnings-ng:8.1.0
workflow-aggregator:2.6
workflow-api:2.40
workflow-basic-steps:2.20
workflow-cps:2.80
workflow-cps-global-lib:2.16
workflow-durable-task-step:2.35
workflow-job:2.38
workflow-multibranch:2.21
workflow-scm-step:2.11
workflow-step-api:2.22
workflow-support:3.4
ws-cleanup:0.38
```

</details>

もしくは以下のコマンドでプラグイン一覧を取得する。

```bash
curl -sSL "http://admin:adminadmin@192.168.33.10:8080/jenkins/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'| sort | sed 's/ /:/' > plugin.txt
```

## SSH公開鍵設定

クライアント側の公開鍵、秘密鍵を作成する。
以下の操作は`Dockerfile`で実施済み。

```bash
bash-4.4# whoami
jenkins
bash-4.4# pwd
/
bash-4.4# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/var/jenkins_home/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /var/jenkins_home/.ssh/id_rsa.
Your public key has been saved in /var/jenkins_home/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:5F74PqoXvbmxZDfUqWRbwLp4LBC/T0voiT9C4cwki60 jenkins@228a3f4b0b56
The key's randomart image is:
+---[RSA 3072]----+
|                 |
|           .     |
|      . .   o    |
|    . o= . . o . |
|   o B..S.o + +  |
|  . o =o.B.= +   |
|   . .  *.@o=    |
|  E   .oo@+= .   |
|      o==o*o     |
+----[SHA256]-----+
bash-4.4# ls -rlta /var/jenkins_home/.ssh/
total 12
drwxrwxrwx. 1 jenkins jenkins 2496  4月 24 20:26 ..
-rw-r--r--. 1 jenkins jenkins  183  4月 24 20:26 known_hosts
-rw-r--r--. 1 jenkins jenkins  574  4月 24 21:40 id_rsa.pub
-rw-------. 1 jenkins jenkins 2610  4月 24 21:40 id_rsa
drwxr-xr-x. 1 jenkins jenkins  160  4月 24 21:40 .
bash-4.4# exit
exit
```

サンプルとして、`user01`に接続する。

```bash
# ll /home/
total 0
drwx------. 4 vagrant vagrant 111 Feb 20 16:38 vagrant/
drwx------. 4 user01  user01  104 Apr 24 09:57 user01/
```

以下のコマンドでdockerコンテナから公開鍵をコピーする。

```bash
# docker cp jenkins:/var/jenkins_home/.ssh/id_rsa.pub .
# ll
total 720972
-rw-r--r--. 1 vagrant vagrant     31718 Apr 24 11:10 README.md
-rwxr-xr-x. 1 vagrant vagrant       582 Apr 24 11:10 Dockerfile*
drwxr-xr-x. 1 vagrant vagrant       128 Apr 24 11:10 asset/
-rw-r--r--. 1 vagrant vagrant 735703913 Apr 24 11:10 jenkins_data.zip
-rw-r--r--. 1 vagrant vagrant       518 Apr 24 11:23 docker-compose.yml
drwxrwxrwx. 1 vagrant vagrant      2496 Apr 24 11:26 jenkins_data/
-rw-r--r--. 1 vagrant vagrant       574 Apr 24 12:40 id_rsa.pub
```

ホスト(`192.168.33.10`)側のユーザ`user01`になり、  
以下の手続きでクライアントで作成した公開鍵を設定する。

```bash
[12:43:22 root@dev /vagrant/docker-jenkins-prototype]
# su - user01
[12:48:11 user01@dev ~]
# mkdir ~/.ssh
[12:48:27 user01@dev ~]
# ls -rlta
total 12
-rw-r--r--. 1 user01 user01 193 Sep  6  2017 .bash_profile
-rw-r--r--. 1 user01 user01  18 Sep  6  2017 .bash_logout
drwxr-xr-x. 4 root   root    35 Apr 24 09:33 ../
-rw-r--r--. 1 user01 user01 346 Apr 24 09:33 .bashrc
drwxr-xr-x. 2 user01 user01  40 Apr 24 09:57 .oracle_jre_usage/
drwxr-xr-x. 3 user01 user01  49 Apr 24 09:58 .gfclient/
drwxrwxr-x. 2 user01 user01   6 Apr 24 12:48 .ssh/
drwx------. 5 user01 user01 116 Apr 24 12:48 ./
[12:48:35 user01@dev ~]
# chmod 700 .ssh
[12:49:23 user01@dev ~]
# ls -rlta | grep .ssh
drwx------. 2 user01 user01   6 Apr 24 12:48 .ssh/
```

```bash
[12:50:36 root@dev ~]
# cat /vagrant/docker-jenkins-prototype/id_rsa.pub >> /home/user01/.ssh/authorized_keys
[12:51:20 root@dev ~]
# ll /home/user01/.ssh/authorized_keys 
-rw-r--r--. 1 root root 574 Apr 24 12:51 /home/user01/.ssh/authorized_keys
[12:51:30 root@dev ~]
# chown user01:user01 /home/user01/.ssh/authorized_keys
[12:51:47 root@dev ~]
# chmod 600 /home/user01/.ssh/authorized_keys
[12:51:55 root@dev ~]
# ll /home/user01/.ssh/authorized_keys 
-rw-------. 1 user01 user01 574 Apr 24 12:51 /home/user01/.ssh/authorized_keys
```

dockerコンテナからホスト(`192.168.33.10`)に接続確認

```bash
# docker exec -it jenkins /bin/bash
bash-4.4# ssh user01@192.168.33.10
Last login: Fri Apr 24 12:48:11 2020
[12:53:59 user01@dev ~]
# exit
logout
Connection to 192.168.33.10 closed.
bash-4.4$
```

## Jenkinsの設定

### 接続先ユーザの登録

`jenkins`ユーザで作成した`user01`に関する秘密鍵を登録する。

![add_credentials](images/add_credentials.png)

認証情報の登録

![create_node](images/create_node.png)

jenkinsからssh接続後の`user01`配下のフォルダには、  
以下のように自動でフォルダが作成される。

```bash
[13:13:09 user01@dev ~]
# cd jenkins_workspace/
[13:13:11 user01@dev ~/jenkins_workspace]
# ls -rlt
total 1488
-rw-rw-r--. 1 user01 user01 1522833  4月 24 13:12 remoting.jar
drwxrwxr-x. 4 user01 user01      34  4月 24 13:12 remoting/
```

### ツール設定

`Jenkinsの管理` → `Global Tool Configuration`で以下の設定をする。

- JDKのパス
- Gitのパス
- Mavenのパス

![setting_jdk_and_git](images/setting_jdk_and_git.png)

![setting_maven](images/setting_maven.png)

### その他

- `root`ユーザ以外で`docker`コマンドを実行させる場合
  
```bash
[13:57:32 root@dev ~]
# gpasswd -a user01 docker
Adding user user01 to group docker
[13:57:43 root@dev ~]
# cat /etc/group | grep docker
docker:x:993:user01
[13:57:52 root@dev ~]
# systemctl restart docker
```

`user01`で`docker`コマンドが問題なく使えることを確認した。

```bash
[13:59:36 user01@dev ~]
# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                              NAMES
fd2ab25f117f        jenkins:1.0         "/sbin/tini -- /usr/…"   3 hours ago         Up About a minute   0.0.0.0:8080->8080/tcp, 0.0.0.0:50000->50000/tcp   jenkins
[13:59:39 user01@dev ~]
# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
jenkins             1.0                 f1173f57b147        9 hours ago         2.9GB
oracle/database     12.1.0.2-se2        c0d031137aff        2 days ago          4.9GB
jenkins/jenkins     centos              bf1c9963816a        6 days ago          782MB
oraclelinux         7-slim              f23503228fa1        2 weeks ago         120MB
```

### イメージのsaveとload

```bash
# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               1.0                 014c7907f337        5 hours ago         127MB
jenkins             1.0                 a2991f6e9850        2 days ago          2.9GB
jenkins/jenkins     2.234-centos        7f53630e24ed        7 days ago          783MB
oracle/database     12.2.0.1-ee         1250a1edfaca        7 days ago          5.98GB
nginx               latest              602e111c06b6        11 days ago         127MB
oraclelinux         7-slim              f23503228fa1        3 weeks ago         120MB
```

`jenkins:1.0`と`nginx:1.0`のイメージをバックアップ取得する。

```bash
# docker save nginx:1.0 -o nginx.tar
# docker save jenkins:1.0 -o jenkins.tar
```

イメージをloadする場合は以下のようにする。

```bash
# docker load -i nginx.tar 
c2adabaecedb: Loading layer [==================================================>]  72.49MB/72.49MB
216cf33c0a28: Loading layer [==================================================>]  58.11MB/58.11MB
b3003aac411c: Loading layer [==================================================>]  3.584kB/3.584kB
5f08e8b0fe2b: Loading layer [==================================================>]  3.584kB/3.584kB
Loaded image: nginx:1.0
```

```bash
# docker load -i jenkins.tar 
0683de282177: Loading layer [==================================================>]  244.9MB/244.9MB
a172a916af1e: Loading layer [==================================================>]  486.9MB/486.9MB
b7929c594495: Loading layer [==================================================>]  309.8kB/309.8kB
bafa503659b3: Loading layer [==================================================>]  3.584kB/3.584kB
78374bb0d910: Loading layer [==================================================>]  9.728kB/9.728kB
9772f06ea992: Loading layer [==================================================>]  869.4kB/869.4kB
23888e98abdc: Loading layer [==================================================>]  66.29MB/66.29MB
30e8f77496fc: Loading layer [==================================================>]  3.584kB/3.584kB
7192be1035e0: Loading layer [==================================================>]  9.728kB/9.728kB
52074d3d4988: Loading layer [==================================================>]   5.12kB/5.12kB
5618fcd45ecf: Loading layer [==================================================>]  3.584kB/3.584kB
bb27fc1e4cd3: Loading layer [==================================================>]  7.168kB/7.168kB
02b44d17e7b6: Loading layer [==================================================>]  13.82kB/13.82kB
6a5f9574adfe: Loading layer [==================================================>]  1.815MB/1.815MB
957128654d6a: Loading layer [==================================================>]  492.4MB/492.4MB
4d6cd26bc1a1: Loading layer [==================================================>]  49.29MB/49.29MB
cb8f1ef2c88a: Loading layer [==================================================>]  3.584kB/3.584kB
8d48d96bceb3: Loading layer [==================================================>]  184.8MB/184.8MB
a1c9904248f7: Loading layer [==================================================>]  30.95MB/30.95MB
b3ab368153dc: Loading layer [==================================================>]  5.632kB/5.632kB
f908381ba266: Loading layer [==================================================>]  367.1MB/367.1MB
c402c11fe9db: Loading layer [==================================================>]  11.33MB/11.33MB
d9dea319a81a: Loading layer [==================================================>]  2.048kB/2.048kB
0bba45c5f844: Loading layer [==================================================>]  4.608kB/4.608kB
f6f9c969ee4b: Loading layer [==================================================>]  7.168kB/7.168kB
6ec45190e196: Loading layer [==================================================>]  349.9MB/349.9MB
7d28da6b5453: Loading layer [==================================================>]  30.99MB/30.99MB
c71b5940fac3: Loading layer [==================================================>]  76.38MB/76.38MB
041a011afbc2: Loading layer [==================================================>]  38.73MB/38.73MB
0b26d3414274: Loading layer [==================================================>]  541.7kB/541.7kB
cd64e0aa2a16: Loading layer [==================================================>]  83.97kB/83.97kB
a99263827564: Loading layer [==================================================>]  310.6MB/310.6MB
0354edf5cbf2: Loading layer [==================================================>]  187.2MB/187.2MB
Loaded image: jenkins:1.0
```


### 参考

- https://www.masatom.in/pukiwiki/Docker/Jenkins/
- https://eng-entrance.com/linux-ssh-key
- https://qiita.com/mountcedar/items/43157ff1225c56500655