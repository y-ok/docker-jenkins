FROM jenkins/jenkins:2.235.3-lts-centos

LABEL y-ok <fairsky1985@gmail.com>

USER root

RUN dnf clean all
RUN dnf -y groupinstall "Development Tools"
RUN dnf -y install langpacks-ja
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

ENV LANG="ja_JP.UTF-8"
ENV LANGUAGE="ja_JP:ja"
ENV LC_ALL="ja_JP.UTF-8"

ADD asset /asset

RUN dnf -y remove java-1.8.0-openjdk
RUN rm -rf /usr/lib/jvm/*
RUN rpm -Uvh --force /asset/jdk-8u101-linux-x64.rpm
ENV JAVA_HOME=/usr/java/jdk1.8.0_101
ENV PATH $PATH:$JAVA_HOME/bin

RUN tar xvzf /asset/apache-maven-3.6.3-bin.tar.gz -C /opt/
RUN ln -s /opt/apache-maven-3.6.3 /opt/apache-maven
RUN echo 'export PATH=$PATH:/opt/apache-maven/bin' | tee -a /etc/profile

COPY /asset/plugin.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt

RUN dnf -y remove git
RUN dnf -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-ExtUtils-MakeMaker
ADD /asset/git-2.28.0.tar.gz /usr/src/
WORKDIR /usr/src/git-2.28.0
RUN make configure
RUN ./configure --prefix=/usr/local
RUN make all
RUN make install

USER jenkins

WORKDIR /var/jenkins_home